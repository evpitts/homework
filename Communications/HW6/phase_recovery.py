import math
import numpy as np
from numpy.random import rand,seed
import pdb
import matplotlib.pyplot as plt
plt.ion()

import sys
sys.path.append('../HW2')
sys.path.append('../comms')

from convert_bits import convert_bits
from srrc1 import srrc1
from comms import diff_encode, diff_decode, compute_k, get_close_point


plt.close('all')
fig_size = (24,8)

seed(0)

#Required values for pulse and constellation formation
alpha = 1       # excess bandwidth
N = 11          # samples per symbol
Lp = 60         # SRRC truncation length
Ts = 1          # symbol time in seconds
T = Ts/N        # sample time in seconds
Nsym = 100       # number of symbols
omega_c = 2*math.pi*.85  # carrier frequency - must be less than half of the sampling rate, N
M = 4           # number of constellation points - QPSK
A = 1           # amplitude
symSize = math.log(M,2)  # size of each symbol

omega_n = 0

BnT = 0.05
zeta = 1/np.sqrt(2)
k0 = 1
kp = 1
k1,k2 = compute_k(BnT,zeta,k0,kp)

k2_prev = 0

rule = {180 : 0b_00,
        270 : 0b_01,
        0   : 0b_10,
        90  : 0b_11}

symbol_loc = {0b_00:45,
              0b_01:135,
              0b_11:225,
              0b_10:315}

LUT = {3:(1,1),
       1:(-1,1),
       2:(1,-1),
       0:(-1,-1)}

const_list = [(-1,1), (1,-1), (-1,-1), (1,1)]

plot_num = 1    # for plotting purposes

#Create the pulse envelope
p_t,n = srrc1(alpha,N,Lp,Ts)


#Create a random stream of bits for the signal
#bits = (rand(int(Nsym*symSize)) > 0.5).astype(int)     # random bits

#Convert binary to decimal to use as indexer
converted_bits = np.array((rand(Nsym)*M).astype(int))#np.array(convert_bits(bits, M))

encoded_bits = diff_encode(converted_bits,rule,symbol_loc)
Nsym = Nsym + 1

# phase_shift = {0:1,
#                1:3,
#                3:2,
#                2:0}

shifted_bits = encoded_bits #[phase_shift[x] for x in encoded_bits]

#Run the encoded bits through the LUT
a_0,a_1 = zip(*[LUT[x] for x in shifted_bits])

#Upsample
a_0_up = np.zeros(N*len(a_0))              # this will produce a column vector of zeros
a_0_up[0::N] = a_0              # creating the upsampled signal

a_1_up = np.zeros(N*len(a_1))              # this will produce a column vector of zeros
a_1_up[0::N] = a_1              # creating the upsampled signal

#Convolve the upsampled/zero-padded bit stream with the pulse envelope
I = np.convolve(a_0_up, p_t)  # complete the multiplication of the basis function (or the filter)
Q = np.convolve(a_1_up, p_t)

t_trig = np.arange(0,len(I),1)#T*np.arange(len(I)) # Using the relationship between T, Ts, and N, the 

# #QAM has two basis functions, cos() and sin()
#Make the final trasnmitted signal by summing the basis functions convolved/multiplied by the detla trains of binary streams
s_t = I*math.sqrt(2)*np.cos(omega_c*t_trig) - Q*math.sqrt(2)*np.sin(omega_c*t_trig)


############# Reciever ##############
# #Multiply by the rest of the basis functions to get them out of the way
shift = 0     # Induced phase shift in degrees
I_r = s_t*math.sqrt(2)*np.cos(omega_c*t_trig+np.deg2rad(shift))
Q_r = -s_t*math.sqrt(2)*np.sin(omega_c*t_trig+np.deg2rad(shift))

x_t = np.convolve(I_r,p_t)  # convolve again to get rid of the filter
y_t = np.convolve(Q_r,p_t)  # convolve again to get rid of the filter

x_r = x_t[range((2*Lp+1)*N-1, N*Nsym + (2*Lp+1)*N, N)]

plt.figure(plot_num,figsize=fig_size)
plot_num += 1
plt.plot(x_r)

y_r = y_t[range((2*Lp+1)*N-1, N*Nsym + (2*Lp+1)*N, N)]


a_h_0 = []#np.zeros(len(x_r))
a_h_1 = []#np.zeros(len(x_r))
e_vec = []#np.zeros(len(x_r))
v = []#np.zeros(len(x_r))
theta_e_vec = []
theta_hat = 0

print(len(x_r))
for i in range(len(x_r)-1):

    #Calculate the rotation paramters
    C = np.cos(theta_hat);
    S = np.sin(theta_hat);

    #CCW rotation
    x_rotate = C*x_r[i] - S*y_r[i]
    y_rotate = S*x_r[i] + C*y_r[i]

    #Pass the rotated signal through the decision block
    a0,a1 = get_close_point(const_list, x_rotate, y_rotate)
    a_h_0.append(a0)
    a_h_1.append(a1)
    
    #PLL and phase detector
    e = np.angle(a0 + a1*1j) - np.angle(x_rotate + y_rotate*1j)
    e_vec.append(e)
    #y_rotate[i]*a_h_0[i] - x_rotate[i]*a_h_1[i]

    v = k1*e + k2*e + k2_prev
    k2_prev = k2_prev + k2*e

    #DDS
    theta_hat = (v + omega_n + theta_hat)
    theta_e_vec.append(theta_hat)
    

##### Decode the sampled signal symbols #####
const = {-1:{-1:0b_11,
             1:0b_01},
         1:{-1:0b_10,
            1:0b_00}}

received_bits = [const[x][y] for x,y in zip(a_h_0,a_h_1)]
decoded_bits = diff_decode(received_bits, rule, symbol_loc)

#Extract the constellation points
const_points_x, const_points_y = zip(*LUT.values())


x_diff = np.array(a_0) - a_h_0
y_diff = np.array(a_1) - a_h_1

plt.figure(plot_num,figsize=fig_size)
plot_num += 1
plt.stem(x_diff,  markerfmt='go', linefmt='green', use_line_collection='True', label='I(t)')
plt.stem(y_diff,  markerfmt='bo', linefmt='blue', use_line_collection='True', label='Q(t)')
plt.xlabel("Symbol Number")
plt.ylabel("Distance from Origin in Units of A")
plt.title("Symbol Locations in the Constellation Space, Difference Between Transmitted and Sliced Bits")
plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.075),
          fancybox=True, shadow=True, ncol=5)

bit_diff = np.array(converted_bits) - np.array(decoded_bits)#converted_bits - decoded_bits

plt.figure(plot_num,figsize=fig_size)
plot_num += 1
plt.stem(bit_diff,  markerfmt='go', linefmt='purple', use_line_collection='True', label='Bit difference')
plt.xlabel("Symbol Number")
plt.ylabel("Difference between the generated bit and decoded bit")
plt.title("Difference Between Generated Bits and Decoded Bits")
plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.075),
          fancybox=True, shadow=True, ncol=5)

plt.figure(plot_num,figsize=fig_size)
plot_num += 1
plt.plot(theta_e_vec, color='b')
plt.title("theta_e_vec, calculated error in rotation of sampled symbols")

plt.figure(plot_num,figsize=fig_size)
plot_num += 1
plt.plot(e_vec, linestyle='--')
plt.title("e_vec, calculated error between constellation point and sampled symbol")

plt.show()

plt.waitforbuttonpress(0) # this will wait for indefinite time
plt.close('all')
