import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from matplotlib import rc
import math

#Contsellation points will be dictionaries, values as tuples (x,y)

def g_data(theta_e, const, K):

    #For every element in the constellation
    #Calculate g(theta_e)
    #Add it to the previous g(theta_e)
    #Divide by number of constellation points
    num_points = len(const)
    g_theta = 0
    for point in range(num_points):
        a_0,a_1 = const[point]
        g_theta += K*(a_0**2 + a_1**2)*np.sin(theta_e)
    s_curve_point = g_theta/num_points
    return s_curve_point

def g_decn(theta_e, const, K):

    num_points = len(const)
    g_theta = 0
    s_curve_points = 0
    
    for a_0,a_1 in const:
        mag = np.sqrt(a_0**2 + a_1**2)
        curr_theta_e = np.angle(a_0 + 1j*a_1)+theta_e
        ctheta = np.cos(curr_theta_e)*mag
        stheta = np.sin(curr_theta_e)*mag
        a_h_0, a_h_1 = get_close_point(const, ctheta, stheta)
        g_theta += K*((a_0*np.sin(theta_e) + a_1*np.cos(theta_e))*a_h_0 - (a_0*np.cos(theta_e) - a_1*np.sin(theta_e))*a_h_1)
    s_curve_point = g_theta/num_points
    return s_curve_point

def get_close_point(const, point_x, point_y):

    diffs = []
    for symbol in const:
        diff_x = symbol[0] - point_x
        diff_y = symbol[1] - point_y
        diffs.append(diff_x**2 + diff_y**2)
    temp = np.argmin(diffs)
    a_h_0 = const[temp][0]
    a_h_1 = const[temp][1]
    if len(const) == 2:
        a_h_1 = 0
    return a_h_0,a_h_1
    
plt.close('all')
K = 1
# LUT = [(1,1),
#        (-1,1),
#        (1,-1),
#        (-1,-1)]
# sq2 = 1/np.sqrt(2)
# LUT = [(sq2,sq2),
#        (-sq2,sq2),
#        (sq2,-sq2),
#        (-sq2,-sq2),
#        (1,0),
#        (-1,0),
#        (0,1),
#        (0,-1)]
# LUT = [(1,0),
#        (-1,0)]
LUT = [(1,1),
       (-1,1),
       (1,-1),
       (-1,-1),
       (3,1),
       (-3,1),
       (3,-1),
       (-3,-1),
       (1,3),
       (-1,3),
       (1,-3),
       (-1,-3),
       (3,3),
       (-3,3),
       (3,-3),
       (-3,-3)]

theta_e = np.linspace(-np.pi,np.pi,101)
data_points = []
decn_points = []

for angle in theta_e:
    data_points.append(g_data(angle, LUT, K))
    decn_points.append(g_decn(angle, LUT, K))

#rc("text", usetex=True)

ticks = [i/8*np.pi for i in range(-8,9)]
formatting = [r"$-\pi$", r"$-\frac{7}{8}\pi$", r"$-\frac{3}{4}\pi$", r"$-\frac{5}{8}\pi$", r"$-\frac{1}{2}\pi$", r"$-\frac{3}{8}\pi$", r"$-\frac{1}{4}\pi$", r"$-\frac{1}{8}\pi$", r"$0$", r"$\frac{1}{8}\pi$", r"$\frac{1}{4}\pi$", r"$\frac{3}{8}\pi$", r"$\frac{1}{2}\pi$", r"$\frac{5}{8}\pi$", r"$\frac{3}{4}\pi$", r"$\frac{7}{8}\pi$", r"$\pi$"]
fig,ax=plt.subplots(2)
plt.sca(ax[0])
plt.xticks(ticks, formatting)
ax[0].plot(theta_e,data_points)

plt.sca(ax[1])
plt.xticks(ticks, formatting)
ax[1].plot(theta_e,decn_points)

plt.show()
