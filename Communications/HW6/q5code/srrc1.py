#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  8 14:04:08 2021

@author: JP
"""
import math
def srrc1(alpha,N,LP,Ts): #square root raised cosine
    n = list(range(-LP*Ts,LP*Ts,1))
    scaler = 1/math.sqrt(N)
    srrcout = []
    for curn in n:
        if curn != 0:
            a = math.sin(math.pi*(1-alpha)*curn/N)
            b = (4*alpha*curn/N)*math.cos(math.pi*(1+alpha)*curn/N)
            c = (math.pi*curn/N)*(1-(4*alpha*curn/N)**2)

        else:  # use l'hopital's rule to define srrc at n = 0
            a  = math.pi*(1-alpha)/N
            b  = 4*alpha/N
            c  = math.pi/N
        
        srrcout.append(scaler*(a+b)/c)
    return srrcout


