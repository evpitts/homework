#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 17 15:54:02 2021

@author: jason
"""
import numpy as np

def encode(bits):
    
    rule = {
            0 : 180,
            1 : -90,
            2 : 0,
            3 : 90}
    iangle = {
            225 : 0,
            135 : 1,
            315 : 2,
            45  : 3}
    angles = {
            0 : 225,
            1 : 135,
            2 : 315,
            3 : 45}
    
    
    #bits = np.array([3,0,1,2])
    Nsyms = len(bits)
    
    #preallocate encOded array
    encoded = np.zeros(Nsyms+1).astype(int)
    
    #Pick a random phase to start at... not random... always 1
    encoded[0] = 1
    curangle = angles[1]
    
    for i in range(Nsyms):
        curangle = (curangle + rule[bits[i]])%360
        # print(curangle)
        encoded[i+1] = iangle[curangle]
        
    #print(bits)
    #print(encoded)
    
    return encoded


def decode(bits):
    irule = {
            180  : 0,
            -90  : 1,
            0    : 2,
            90   : 3,
            -180 : 0,
            270  : 1}

    angles = {
            0 : 225,
            1 : 135,
            2 : 315,
            3 : 45}
    
    Nsyms = len(bits) # symbols to decode
    decoded = np.zeros(Nsyms-1).astype(int)
    for i in range(1,Nsyms):
        rot = (angles[bits[i]]-angles[bits[i-1]])%360 
        decoded[i-1] = irule[rot]
        
    return(decoded)
        
    
    
#LUT_Corr = np.array([1,2,3,0])
#cur_encoded = LUT_Corr[encoded]
#print(cur_encoded)