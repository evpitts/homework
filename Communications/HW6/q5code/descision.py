#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 22 10:39:03 2021

@author: Jason Powell
"""
import numpy as np

# Assumes I is real and Q is imag
# Also assumes that the LUT value is defined by index
# TODO: use a dictionary instead to avoid assumption 2
def decision(x, LUTX,LUTY):
    if not hasattr(x, "__len__"):
        # breakpoint()
        xd = LUTX - np.real(x)
        yd = LUTY - np.imag(x)
        d = xd**2 + yd**2
        return np.argmin(d)
    else:
        dec = np.zeros(len(x)) # Decided symbols
        for i in range(len(x)):
            xd = LUTX - np.real(x[i])
            yd = LUTY - np.imag(x[i])
            d = xd**2 + yd**2
            dec[i] = np.argmin(d)
        return dec