#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  4 11:55:19 2021

@author: jason
"""

def compute_K(Kp,K0,BnT,zeta):
    
    # C.57
    thetaN = BnT/(zeta + 1/(4*zeta))
    #C.58(A)
    K0KpK1 = (4*zeta*thetaN)/(1+2*zeta*thetaN + thetaN**2)
    #C.58(B)
    K0KpK2 = 4*thetaN**2/(1+2*zeta*thetaN+thetaN**2)
    
    K2 = K0KpK2/(Kp*K0)
    print(K2)
    
    K1 = K0KpK1/(Kp*K0)
    print(K1)
    
    return K1,K2