#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 20 20:37:17 2021

@author: jason
"""
import numpy as np
from numpy.random import rand,seed
from descision import decision
from compute_k import compute_K
import pdb
from srrc1 import srrc1
from Q2 import encode
from Q2 import decode
import math
import matplotlib.pyplot as plt

seed(0)

Nsym = 100
M = 4 # The number of constelation points
N = 11
a = 1
Omega0 = 0

Kp = 1
K0 = 1
BnT = 0.05
zeta = 1/math.sqrt(2)
K1,K2 = compute_K(Kp,K0,BnT,zeta)

# Acquire a LUT (hard code if unique)
LUTX = np.array([-1,-1,1,1])
LUTY = np.array([-1,1,-1,1])
    
    
# Generate random QPSK symbols
s = np.array((rand(Nsym)*M).astype(int))

# ENCODE BITS
s_old = s
s = encode(s)

# increment Nsym by 1 for encoding 
Nsym = Nsym + 1
i = LUTX[s]
q = LUTY[s]

# Upsample by N
i_t = np.zeros((N*Nsym,1))
q_t = np.zeros((N*Nsym,1))
i_t[range(0,N*Nsym,N)] = i.reshape(Nsym,1)
q_t[range(0,N*Nsym,N)] = q.reshape(Nsym,1)

# Pass through the SRRC transmitter pulse shaping filter on I and Q branches
alpha = 1   # excess bandwidth
LP = 60      # SRRC truncation length (samples)
Ts = 1       # Symbol time
T = Ts/N     # Sample time
p_t = srrc1(alpha,N,LP,Ts)       # Sqroot raised cosine pulse
I_t = np.convolve(i_t.reshape((N*Nsym,)),p_t)
Q_t = np.convolve(q_t.reshape((N*Nsym,)),p_t)

# Modulate I by cos and Q by sin.
f0 = 0.85    # Carrier frequency
w0 = f0*2*math.pi
n = np.arange(0,len(I_t),1)
s_t = I_t*math.sqrt(2)*np.cos(w0*n)+Q_t*-1*math.sqrt(2)*np.sin(w0*n) 


# # Receiver processing # #

# Demodulate I by cos and Q by sin
shift = 130
Ir_t = s_t*math.sqrt(2)*np.cos(w0*n+np.deg2rad(shift))
Qr_t = s_t*-1*math.sqrt(2)*np.sin(w0*n+np.deg2rad(shift))

# Pass through the matched filter
x_t = np.convolve(Ir_t,p_t)
y_t = np.convolve(Qr_t,p_t)

# Timining syncronization (Down sample by N)
    # number of symbos to throw away
Nsymtoss = 2*np.ceil(LP/N) 
    # number of points in signal
offset = (2*LP - np.floor(N/2)).astype(int)
# nc = (np.floor((len(x_t) - offset - Nsymtoss*N)/N)).astype(int)

x_down = x_t[range(2*LP,N*Nsym+2*LP,N)]
y_down = y_t[range(2*LP,N*Nsym+2*LP,N)]


# TODO: See about doing this in real time
cnt = N
C = 1 # cos(0)
S = 0 # sin(0)

# Loop Filter
K2gain_prev = 0

# DDS Filter
theta_0 = 0

s_hat = np.zeros(len(x_down)).astype(int)
thetav = np.zeros(len(x_down))
e_vec = np.zeros(len(x_down))
for i in range(len(x_down)):
    # Mix receiver signal with free-running oscillator
    # Matched filter on I/Q branches
    # Downsamplecnt = cnt-1if(cnt == 0):  
        # if this is a symbol timecnt = N
        
    # compute the rotation parameters
    C = np.cos(theta_0)
    S = np.sin(theta_0)
        
    # APPLY ROTATION
    xr = x_down[i]*C - y_down[i]*S
    yr = x_down[i]*S + y_down[i]*C
    # Evaluate the symbol  
    # breakpoint()
    s_hat[i] = decision((xr + 1j*yr),LUTX,LUTY)
    # breakpoint()
    a0_h = LUTX[s_hat[i]]
    a1_h = LUTY[s_hat[i]]
    
    # Evaluate the ML PED
    e = np.angle(a0_h + 1j*a1_h) - np.angle(xr+1j*yr)
    # breakpoint()
    # e = -1*(yr*a0_h - xr*a1_h)
    e_vec[i] = e
    
    # Apply the loop filter
        # LOOP FILTER
    #           K1 Gain         #K2 Gain
    v = e*K1+e*K2+K2gain_prev
    
    K2gain_prev = e*K2+K2gain_prev
    
    # DDS (baseband)
    theta_0 = Omega0+v+theta_0
    thetav[i] = theta_0
    # save the signals you want to look at later
# Plot saved signals

# decode s_hat
s_hat = decode(s_hat)
plt.figure(1);plt.clf()
plt.plot(s_old[:100],marker= 'o',label='Transmitted')
plt.plot(s_hat[:100],marker='x',label='Received')
plt.legend()
pdb.set_trace()

plt.figure(2);plt.clf()
plt.plot(np.rad2deg(thetav))
plt.title('Estimated phase')

plt.figure(3);plt.clf()
plt.plot(e_vec)
plt.title('Error')

plt.figure(4);plt.clf()
plt.plot(I_t[:200],label='I data')
plt.plot(Q_t[:200],label='Q data')
plt.title('Transmitted Signal')
plt.legend()

plt.figure(5);plt.clf()
plt.plot(x_t[:200],label='I data')
plt.plot(y_t[:200],label='Q data')
plt.title('Received Signal')
plt.legend()

plt.show()

