import numpy as np
import matplotlib.pyplot as plt

import sys
sys.path.append('../comms')

import comms

def create_sig(bits, symbol_loc, A):
    
    #Using the magnitude and orientation, I can convert a bit stream to my signal, in two channels
    num_samples = int((len(bits)-2)/2)
    x_sig = []
    y_sig = []
    for symbol in bits:
        x_sig.append(A*np.cos(symbol_loc[symbol]*np.pi/180))
        y_sig.append(A*np.sin(symbol_loc[symbol]*np.pi/180))
    return x_sig, y_sig

if __name__=="__main__":
    #QPSK
    plt.clf
    plt.close('all')
    A = np.sqrt(2)
    
    # answer = 0b_11_00_01_01_00_00_10_11_00_01_00_01
    # encode = 0b_01_11_00_10_11_00_11_11_10_01_00_11_01

    answer = [3, 0, 1, 1, 0, 0, 2, 3, 0, 1, 0, 1]
    encode = [1, 3, 0, 2, 3, 0, 3, 3, 2, 1, 0, 3, 1]
    
    # answer = [1, 1, 1, 1, 1, 3, 3, 3, 2, 0, 3, 0, 3, 0, 2]
    # encode = [1, 0, 2, 3, 1, 0, 1, 3, 2, 2, 0, 1, 2, 0, 3, 3]
    
    rule = {180 : 0b_00,
            270 : 0b_01,
            0   : 0b_10,
            90  : 0b_11
    }
    
    const = {-1:{-1:0b_11,
                 1:0b_01},
             1:{-1:0b_10,
                1:0b_00}
    }

    # reverse_const = {0b_11:(-1,-1),
    #                  0b_01:(-1,1),
    #                  0b_10:(1,-1),
    #                  0b_00:(1,1)}
    
    symbol_loc = {0b_00:45,
                  0b_01:135,
                  0b_11:225,
                  0b_10:315}
    
    #BPSK
    # rule = {0: 0b0,
    #         180 : 0b1}
    
    # const = {-1:{0:0b0},
    #           1:{0:0b1}}
    
    # symbol_loc = {0b0:180,
    #               0b1:0}
    # x_sig = [-1, 1, -1, 1, 1, -1, 1, -1, 1, -1, -1, 1, -1]
    # y_sig = None
    # answer = 0b_1_1_1_0_1_1_1_1_1_0_1_1
    
    coded_message = comms.diff_encode(answer, rule, symbol_loc)
    print(all(map(lambda x,y: x==y, coded_message, encode)))

    x_code,y_code = create_sig(coded_message, symbol_loc, A)

    x_sig,y_sig = create_sig(answer, symbol_loc, A)
    axis = np.arange(len(coded_message))
    fig,ax = plt.subplots(2)
    ax[0].plot(axis[:-1], x_sig, linestyle='--', marker='o', color='b')
    ax[1].plot(axis[:-1], y_sig, linestyle='--', marker='o', color='b')
    plt.title("Original message")
    
    fig,ax = plt.subplots(2)
    ax[0].plot(axis, x_code, linestyle='--', marker='o', color='b')
    ax[1].plot(axis, y_code, linestyle='--', marker='o', color='b')
    plt.title("Encoded message")

    
    phase_shift = {0:1,
                   1:3,
                   3:2,
                   2:0}
    
    shifted_message = [phase_shift[x] for x in coded_message]

    x_shift, y_shift = create_sig(shifted_message, symbol_loc, A)

    fig,ax = plt.subplots(2)
    ax[0].plot(axis, x_shift, linestyle='--', marker='o', color='b')
    ax[1].plot(axis, y_shift, linestyle='--', marker='o', color='b')
    plt.title("Message with a 90$^{\circ}$ induced phase shift")
    
    message = comms.diff_decode(shifted_message,rule,symbol_loc)
    print(all(map(lambda x,y: x==y, coded_message, encode)))

    x_decode, y_decode = create_sig(message, symbol_loc, A)

    fig,ax = plt.subplots(2)
    ax[0].plot(axis[:-1], x_decode, linestyle='--', marker='o', color='b')
    ax[1].plot(axis[:-1], y_decode, linestyle='--', marker='o', color='b')
    plt.title("Decoded message")

    plt.show()
