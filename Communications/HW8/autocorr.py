import matplotlib.pyplot as plt
import pdb

plt.close("all")
plt.rc('text', usetex=True)
plt.rc('font', family='serif')

N = 15
b_i = [0, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1]
c_i = [(-1)**x for x in b_i]

tau = [x for x in range(-N,N+1)]
r_c = []
for t in tau:
    s = 0
    for i in range(N):
        #pdb.set_trace()
        s += 1/N*(c_i[i]*c_i[(i-t)%N])
    #pdb.set_trace()
    r_c.append(s)        
plt.stem(tau,r_c)
plt.title("Autocorrelation of $r_c$")
plt.show()
