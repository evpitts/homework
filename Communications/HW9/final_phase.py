import numpy as np
import numpy.matlib
import math
import matplotlib.pyplot as plt
import matplotlib
import struct   # i
import pdb

import sys
sys.path.append('../HW2')
sys.path.append('../comms')

from srrc1 import srrc1
from comms import diff_encode, diff_decode, compute_k, get_close_point, differentiator_filter, delay_filter, linear_interpolation, cubic_interpolation, convert_uws_l2s, find_uw, find_best_corr, plot_correlation, convert_l2s_complex

plt.close("all")
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plot_num = 1    # for plotting purposes

# Set parameters
f_eps        = 0.0 # Carrier frequency offset percentage (0 = no offset)
Phase_Offset = 0.0 # Carrier phase offset (0 = no offset)
t_eps        = 0.0 # Clock freqency offset percentage (0 = no offset)
T_offset     = 0.0 # Clock phase offset (0 = no offset)
Ts = 1             # Symbol period
N = 4              # Samples per symbol period
Omega0 = math.pi/2*(1+f_eps)
Lp = 12
alpha = 0.7
fname = ['test2021', 'sim1_2021', 'sim2_2021', 'sim3_2021', 'sim4_2021', 'sim5_2021', 'sim6_2021']
L = 11
BnT = 0.2
zeta = 1/np.sqrt(2)
k0 = 1
kp = 1
k1,k2 = compute_k(BnT,zeta,k0,kp)
# Cubic interpolation
# k1 = -0.006#28886
# k2 = -0.0000096289
num_rotations = 4

#Create the pulse envelope
p_t,n = srrc1(alpha,N,Lp,Ts)

##### Use 8 bits per symbol and 256 square QAM #####
B = 8;            # Bits per symbol (B should be even: 8, 6, 4, 2)
bits2index = 2**np.arange(B-1,-1,-1)
M = 2 ** B       # Number of symbols in the constellation
Mroot = math.floor(2**(B/2))
a = np.reshape(np.arange(-Mroot+1,Mroot,2),(2*B,1))
b = np.ones((Mroot,1))
LUT = np.hstack((np.kron(a,b), np.kron(b,a)))

# Scale the constellation to have unit energy
Enorm = np.sum(LUT ** 2) / M;
LUT = LUT/math.sqrt(Enorm);

Eave = 1;
Eb = Eave/B;
EbN0dB = 30; # SNR in dB
N0 = Eb*10**(-EbN0dB/10);
nstd = math.sqrt(N0/2); # Noise standard deviation
# Note nstd is set to 0 below so there is no noise

##### Unique word (randomly generated) #####
uw = np.array([162,29,92,47,16,112,63,234,50,7,15,211,109,124,
                   239,255,243,134,119,40,134,158,182,0,101,
                   62,176,152,228,36])
uw_len = uw.size
uw = uw.reshape(uw_len,)
uwsym = LUT[uw,:]


##### Build the list of four possible UW rotations #####
angles = 2*math.pi*np.arange(0,4)/4
uwrotlocs = np.zeros((uw_len,2,4));

for i in range(angles.size):
  C = math.cos(angles[i])
  S = -math.sin(angles[i])
  G = np.array([[C, -S],[S, C]])
  uwrot = uwsym @ G;  # Rotate the UW symbols
  uwrotlocs[:,:,i] = uwrot; # Save the rotated version

uwrotsyms = convert_uws_l2s(uwrotlocs, LUT)

##### Reciever #####
fid=open(fname[3],'r')
r = np.genfromtxt(fid, delimiter=' ')

#Extract I and Q
n = np.arange(len(r))
I = math.sqrt(2)*np.cos(Omega0*n + Phase_Offset) * r
Q = -math.sqrt(2)*np.sin(Omega0*n + Phase_Offset) * r

#Convolve with pulse envelope
s_t = np.convolve(I,p_t)+1j*np.convolve(Q,p_t)

offset = (Lp*N)
Nsymtoss = 2*np.ceil(Lp)
nc = (np.floor((len(I) - offset - Nsymtoss*N)/N)).astype(int)

output_symbols = s_t[offset:offset + nc*N]

omega_n = 0
BnT = 0.2
zeta = 1/np.sqrt(2)
k0 = 1
kp = 1
k1,k2 = compute_k(BnT,zeta,k0,kp)
# k1 = -0.006#28886
# k2 = -0.0000096289

theta_prev = 0
k2_prev = 0

sig_len = len(output_symbols)

v = np.zeros(sig_len)
e = np.zeros(sig_len)
est_symbols = np.zeros(sig_len, dtype=complex)
est_sym_num = np.zeros(sig_len)
new_symbols = np.zeros(sig_len, dtype=complex)

theta = np.zeros(sig_len)

for idx,n_in in enumerate(output_symbols):
    #Rotate the output symbol by the previous theta
    C = np.cos(theta_prev)
    S = np.sin(theta_prev)
    xn = n_in.real
    yn = n_in.imag
    xr = C*xn - S*yn
    yr = S*xn + C*yn

    new_symbols[idx] = xr + 1j*yr

    a0_hat, a1_hat = get_close_point(LUT, xr, yr)
    est_symbols[idx] = a0_hat + 1j*a1_hat
    est_sym_num[idx] = np.argmin(np.sum((LUT - [a0_hat,a1_hat])**2,axis=1))
    
    #Calculate the difference between the current angle and the closest constellation point
    e[idx] = (yr*a0_hat - xr*a1_hat)

    #Loop filter
    v[idx] = k1*e[idx] + k2*e[idx] + k2_prev
    k2_prev = k2_prev + k2*e[idx]
    
    #error[idx] = arg - DDS_arg_prev
    theta[idx] = v[idx] + omega_n + theta_prev
    theta_prev = theta[idx]
    
#Figures
#pdb.set_trace()
plt.figure(plot_num)
plot_num += 1
n = np.arange(sig_len)
plt.plot(theta)

plt.figure(plot_num)
plot_num += 1
n = np.arange(sig_len)
plt.plot(e)

#Plot the symbols due to TED
C_color = [x/sig_len for x in range(sig_len)]
colormap = 'cividis'
plt.figure(plot_num)
plot_num += 1
plt.scatter(output_symbols.imag, output_symbols.real, cmap = colormap, c = C_color)
cbar = plt.colorbar(matplotlib.cm.ScalarMappable(cmap=colormap),ticks=[0.0, 0.5, 1.0])
cbar.ax.set_yticklabels(['0','500','1000'])
plt.title("Interpolated MF outputs over symbol index")
plt.xlabel("Inphase location of symbol in A")
plt.ylabel("Quadrature location of symbol in A")
cbar.ax.set_ylabel("Symbol Index, n")

# Estimated symbols
plt.figure(plot_num)
plot_num += 1
plt.scatter(est_symbols.imag, est_symbols.real)
plt.title("Interpolated MF outputs over symbol index")
plt.xlabel("Inphase location of symbol in A")
plt.ylabel("Quadrature location of symbol in A")
cbar.ax.set_ylabel("Symbol Index, n")

#New symbols
C_color= [x/sig_len for x in range(sig_len)]
colormap = 'winter'
plt.figure(plot_num)
plot_num += 1
plt.scatter(new_symbols.imag, new_symbols.real, cmap = colormap, c = C_color)
cbar = plt.colorbar(matplotlib.cm.ScalarMappable(cmap=colormap),ticks=[0.0, 0.5, 1.0])
cbar.ax.set_yticklabels(['0','500','1000'])
plt.title("Interpolated MF outputs over symbol index")
plt.xlabel("Inphase location of symbol in A")
plt.ylabel("Quadrature location of symbol in A")
cbar.ax.set_ylabel("Symbol Index, n")


# fig = plt.figure(plot_num)
# plot_num += 1
# ax1 = fig.add_subplot(311)
# ax2 = fig.add_subplot(312)
# ax3 = fig.add_subplot(313)

# plot_n = np.arange(len(output_symbols))
# #Real parts of input and DDS output sinusoids
# ax1.plot(plot_n, np.real(x), label='Re{Input}', linestyle='--', color='k')
# #ax1.plot(n, DDS_cos[:-1], label='Re{DDS}', color='k')
# ax1.plot(plot_n, np.real(DDS[:-1]), label='Re{DDS}', color='k')
# ax1.set_xlabel('Sample Index')
# ax1.set_ylabel('Real Part of Sinusoid')
# ax1.legend()

# #Error
# ax2.plot(plot_n, e, label='error', color='k')
# ax2.set_xlabel('Sample Index')
# ax2.set_ylabel('e(n)')
# ax2.legend()

# #Sinusoidal arguments
# #DDS_args = [x + 2*np.pi for x in DDS_args]
# ax3.plot(plot_n, args, label='Input arg', linestyle='--', color='k')
# ax3.plot(plot_n, DDS_args[:-1], label='DDS arg', color='k')
# ax3.set_xlabel('Sample Index')
# ax3.set_ylabel('Sinusoidal Arguments')
# ax3.legend()
output_sym_nums = convert_l2s_complex(LUT, new_symbols)
correlations = find_uw(uwrotsyms, output_sym_nums)
plot_correlation(correlations,plot_num)
plot_num += 1

#Find the correlation with the highest number of 1.0s
corr_idx,uw_instances = find_best_corr(correlations)
# pdb.set_trace()
periods = np.diff(uw_instances)
mode_period = np.unique(periods)

last_uw = uw_instances[-1]
output_sym_nums = output_sym_nums[:last_uw+uw_len]
num_rows = mode_period[0] #The unique word is included here
leftovers = len(output_sym_nums)%num_rows

output_sym_nums = output_sym_nums[leftovers:]
  
plt.figure(plot_num)
plot_num += 1
image = np.reshape(output_sym_nums,(-1,num_rows)).T # 
plt.imshow(255-image, cmap=plt.get_cmap('Greys'))


plt.show()
