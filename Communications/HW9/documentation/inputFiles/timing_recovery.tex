\section{Timing Recovery Images}
\label{sec:timingrecovery}

This section describes recovery of images with timing or clock offsets. The general maximum likelihood timing error detector (MLTED) is shown in \autoref{fig:block}.

\subsection{Constant Timing Offset}
\label{sec:timingoffset}
\autoref{fig:timingoffset} was recovered from a data stream that had a constant phase offset. This offset was remedied using an MLTED. An MLPED was employed for phase correction, although it was not required. The parameters used for this image can be found in \autoref{tab:timingoffsettab}.

\begin{figure}[H]
    \centering
    \includegraphics[width=3.1in]{images/timingoffset}
    \caption{This image was recovered from a data stream that had a
      fixed timing offset. The image is blurred on the extreme left
      side due to locking time of the MLPED and MLTED. The unique word
      used to recover this image is shown at the bottom.}
    \label{fig:timingoffset}
\end{figure}

Note that the extreme left side of the image is blurred. This is
simply due to the locking time of the MLTED, and perhaps the
MLPED. Also note the unique word at the bottom of the image. The
fractional interval $\mu$ is shown in
\autoref{fig:mutimingoffset}. Recall that the MLTED uses a modulo 1
counter. As a result, as $\mu$ oscillates between $1 - \epsilon$ and
$1 + \epsilon$, the modulo causes $\mu$ to switch instantaneously
between 0 and 1. This is expected in the case of convergence to
unity. Convergence of the fractional interval $\mu$ indicates a fixed
timing offset rather than a carrier frequency offset.

\begin{figure}[H]
    \centering
    \includegraphics[width=3.7in]{images/mutimingoffsetdot}
    \caption{Fractional interval of decrementing modulo counter in MLTED. The plot shows convergence to a value of unity. The modulo operator rolls over at 1, creating the bifurcated plot.}
    \label{fig:mutimingoffset}
\end{figure}

The filtered timing error, shown in \autoref{fig:vTEDtimingoffset}, converges to a small value, indicating good interpolation and convergence to correct sampling times.

\begin{figure}[H]
    \centering
    \includegraphics[width=3.7in]{images/vTEDtimingoffsetdot}
    \caption{Filtered timing error signal, showing convergence to a small value}
    \label{fig:vTEDtimingoffset}
\end{figure}

\autoref{fig:symtimingoffset} shows the received symbols before entering the MLPED. Note that over time, the symbols converge to true constellation points, and the convergence is very quick.

\begin{figure}[H]
    \centering
    \includegraphics[width=3.7in]{images/symtimingoffset}
    \caption{Received constellation symbols over time, showing quick convergence. Blue corresponds to early time, with gray and yellow respectively occurring later in the transmission after lock has occurred.}
    \label{fig:symtimingoffset}
\end{figure}


\subsection{Carrier Frequency Offset}
\label{sec:clockoffset}

\autoref{fig:clockoffset} was recovered from a data stream that had a carrier frequency timing offset. This means the carrier frequency in the transmitter changed over time. This carrier frequency timing offset was remedied using an MLTED. An MLPED was employed for phase correction, although it was not required. The parameters used for this image can be found in \autoref{tab:carrieroffsettab}.

\begin{figure}[H]
    \centering
    \includegraphics[width=3.1in]{images/clockoffset}
    \caption{This image was recovered from a data stream that had a
      carrier frequency offset. The image is blurred on the extreme left
      side due to locking time of the MLPED and MLTED. The unique word
      used to recover this image is shown at the bottom.}
    \label{fig:clockoffset}
\end{figure}

Note the unique word at the bottom of the image. The fractional interval $\mu$ is shown in \autoref{fig:muclockoffsetdot}. Note that because the timing offset varies in time, $\mu$ never converges or settles. This is expected, because a frequency offset will necessarily cause $\mu$ to continuously adapt to differences in frequency between receiver and transmitter.

\begin{figure}[H]
    \centering
    \includegraphics[width=5.7in]{images/muclockoffsetdot}
    \caption{Fractional interval of decrementing modulo counter in MLTED. The plot shows no convergence, which is expected because of the continuous change in frequency}
    \label{fig:muclockoffsetdot}
\end{figure}

The filtered timing error, shown in \autoref{fig:vTEDclockoffsetdot}, converges to a small value, indicating the MLTED is interpolating and sampling correctly.

\begin{figure}[H]
    \centering
    \includegraphics[width=3.7in]{images/vTEDclockoffsetdot}
    \caption{Filtered timing error signal, showing convergence to a small value}
    \label{fig:vTEDclockoffsetdot}
\end{figure}

\autoref{fig:symclockoffset} shows the received symbols after interpolation in the MLTED. Note that the symbols are still quickly converging to correct constellation locations, so the MLTED is working very well.

\begin{figure}[H]
    \centering
    \includegraphics[width=3.7in]{images/symclockoffset}
    \caption{Received constellation symbols over time, no rotation applied}
    \label{fig:symclockoffset}
\end{figure}
