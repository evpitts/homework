\section{Phase Recovery Images}
\label{sec:phaserecovery}

This section describes recovery of images with phase or frequency offsets. The MLPED is shown in \autoref{fig:block}.

\subsection{Constant Phase Offset}
\label{sec:phaseoffset}

\autoref{fig:phaseoffset} was recovered from a data stream that had a constant phase offset. This offset was remedied using an MLPED. An MLTED was employed for timing, although it was not required. The parameters used to recover this image can be found in \autoref{tab:phaseoffsettab}.

\begin{figure}[H]
    \centering
    \includegraphics[width=3.1in]{images/phaseoffset}
    \caption{This image was recovered from a data stream that had a
      fixed phase offset. The image is blurred on the extreme left
      side due to locking time of the MLPED and MLTED. The unique word
      used to recover this image is shown at the bottom.}
    \label{fig:phaseoffset}
\end{figure}

Note that the extreme left side of the image is blurred. This is simply due to the locking time of the MLPED, and perhaps the locking time of the MLTED. Also note the unique word at the bottom of the image. The phase estimate $\hat{\theta}$ is shown as a line plot and in polar form in \autoref{fig:thetaphaseoffset}.

\begin{figure}[H]
    \centering
    \includegraphics[width=3.7in]{images/thetaphaseoffset}
    \caption{MLPED phase estimate for recovery of image in
      \autoref{fig:nooffset}. A fixed phase offset of
      approximately 41$^\circ$ was present. The graph on the left
      shows a line plot of phase error over time as a function of
      sample index. The graph on the right represents the same data
      shown in polar form. Note the settling time is longer
      than the settling time shown in \autoref{fig:thetanooffset}.}
    \label{fig:thetaphaseoffset}
\end{figure}

Note that $\hat{\theta}$ converges to approximately $ -41^{\circ}$,
indicating that the MLPED has detected a fixed constellation rotation, and is recommending a rotation of approximately $ 41^{\circ}$ to
correct this. The filtered phase error, shown in
\autoref{fig:vPEDphaseoffset}, converges to a small value, indicating
the MLPED has truly locked. There is some error, but it is negligible.

\begin{figure}[H]
    \centering
    \includegraphics[width=3.6in]{images/vPEDphaseoffset}
    \caption{Filtered phase error signal for the recovery of the image in \autoref{fig:phaseoffset}. Note convergence to a small value.}
    \label{fig:vPEDphaseoffset}
\end{figure}

\autoref{fig:symphaseoffset} shows the received symbols before entering the MLPED. Note that the constellation is rotated but stationary, meaning there is a constant phase offset from the transmitted data.

\begin{figure}[H]
  \centering
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \includegraphics[width=\linewidth]{images/symphaseoffset}
    \caption{}
    \label{fig:symphaseoffset}
  \end{subfigure}
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \includegraphics[width=\linewidth]{images/rotsymphaseoffset}
    \caption{}
    \label{fig:rotsymphaseoffset}
  \end{subfigure}
  \caption{Constellation symbols after MLPED. (a) Received constellation symbols over time, before MLPED. (b) Rotated constellation symbols over time, after MLPED. Blue corresponds to early time, with gray and yellow respectively occurring later in the transmission.}
\end{figure}

After the constellation has locked, the unique word is used to find the correct constellation rotation. The MLPED only ensures a lock on one of four constellation rotations; the unique word is used to find which of them is correct, allowing correct rotation of the locked symbols. \autoref{fig:correlationsphaseoffset} shows the correlations of each rotation of the unique word and the locked symbols. Note that the $270^{\circ}$ rotation shows the highest correlation, meaning each locked symbol needs to be rotated by $270^{\circ}$ to match with the correct constellation.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.91\linewidth]{images/correlationsphaseoffset}
    \caption{Correlation of each rotation of the unique word with the MLPED output symbols. A correlation value of 1 is expected each time the unique word is found in the MLPED output. Note that this correlation value is achieved regularly in the lower-right plot (red). It is possible to find multiple rotations of the unique word in the data, but only the correct rotation will show up regularly. This never occurred in the other rotations shown above.}
    \label{fig:correlationsphaseoffset}
\end{figure}



\subsection{Carrier Frequency Offset}
\label{sec:carrieroffset}
\autoref{fig:carrieroffset} was recovered from a data stream that had a carrier frequency offset. This means the rotation of the constellation changed over time. This type of error is remedied using an MLPED. A MLTED was employed for timing, although it was not required. The parameters used for this image can be found in \autoref{tab:carrieroffsettab}.

\begin{figure}[H]
    \centering
    \includegraphics[width=3.1in]{images/carrieroffset}
    \caption{This image was recovered from a data stream that had a
      carrier frequency offset. The image is blurred on the extreme left
      side due to locking time of the MLPED and MLTED. Note that the locking time is longer than in \autoref{fig:phaseoffset}. The unique word is shown at the bottom.}
    \label{fig:carrieroffset}
\end{figure}

Note that the extreme left side of the image is blurred, and that the blur is not contiguous. This is due to the locking time of the MLPED and MLTED, and the fact that the MLPED can occasionally become unlocked. Also note the unique word at the bottom of the image. The phase estimate $\hat{\theta}$ is shown as a line plot and in polar form in \autoref{fig:thetacarrieroffset}.

\begin{figure}[H]
    \centering
    \includegraphics[width=3.6in]{images/thetacarrieroffset}
    \caption{MLPED phase estimate for recovery of image in
      \autoref{fig:nooffset}. The graph on the left shows a line plot
      of phase error over time as a function of sample index. Note
      that $\theta$ never settles, because of the carrier frequency
      offset. The graph on the right represents the same data shown in
      polar form. The distance from origin, $\rho$, increases linearly
      with time. This allows us to see that $\theta$ undergoes a
      continuous linear increase with time indefinitely. Compare with \autoref{fig:thetanooffset} and \autoref{fig:thetaphaseoffset}.}
    \label{fig:thetacarrieroffset}
\end{figure}

Note that $\hat{\theta}$ never converges, indicating that the image has a carrier frequency offset, rather than a constant phase offset. This can be seen in the line plot and in the polar plot, where the distance from origin, $\rho$, is a linear function of time. As the receiver continues in time, $\theta$ continues to sweep through cycles of $2\pi$. The filtered phase error, shown in \autoref{fig:vPEDcarrieroffset}, converges to a small value, indicating the MLPED is appropriately identifying the needed rotation at every time step.

\begin{figure}[H]
    \centering
    \includegraphics[width=3.6in]{images/vPEDcarrieroffset}
    \caption{Filtered phase error signal for the recovery of the image in \autoref{fig:carrieroffset}. Note convergence to a small value.}
    \label{fig:vPEDcarrieroffset}
\end{figure}

\autoref{fig:symcarrieroffset} shows the received symbols before entering the MLPED. Note that the constellation is rotating in time, meaning the carrier frequency offset is a function of time, and the rotation from the transmitted data is continuously changing in a linear fashion. \autoref{fig:rotsymcarrieroffset} shows a stationary, correctly locked constellation. Because of the continuous change in rotation, the MLPED takes a longer time to lock, but still does a very good job of detecting the constellation rotation.

\begin{figure}[H]
  \centering
  \begin{subfigure}[b]{0.49\textwidth}
    \includegraphics[width=\linewidth]{images/symcarrieroffset}
    \caption{}
    \label{fig:symcarrieroffset}
  \end{subfigure}
  \begin{subfigure}[b]{0.49\textwidth}
    \centering
    \includegraphics[width=\linewidth]{images/rotsymcarrieroffset}
    \caption{}
    \label{fig:rotsymcarrieroffset}
  \end{subfigure}
  \caption{Constellation symbols after MLPED. (a) Received constellation symbols over time, before MLPED. (b) Rotated constellation symbols over time, after MLPED. Blue corresponds to early time, with gray and yellow respectively occurring later in the transmission after lock has occurred.}
\end{figure}

After the constellation has locked, the unique word is used to find the correct constellation rotation. The MLPED only ensures a lock on one of four constellation rotations; the unique word is used to find which of them is correct, allowing rotation of the locked symbols by the correct amount. \autoref{fig:correlationscarrieroffset} shows the correlations of each rotation of the unique word and the locked symbols. Note that the $90^{\circ}$ rotation shows the highest correlation, meaning each locked symbol needs to be rotated by $90^{\circ}$ to match with the correct constellation.

\begin{figure}[H]
    \centering
    \includegraphics[width=\linewidth]{images/correlationscarrieroffset}
    \caption{Correlation of each rotation of the unique word with the MLPED output symbols. A correlation value of 1 is expected each time the unique word is found in the MLPED output. Note that this correlation value is achieved regularly in the upper-right plot (orange). It is possible to find multiple rotations of the unique word in the MLPED output, but only the correct rotation will show up regularly. In the lower-left plot (green), a single correlation of almost 1 can be seen. This demonstrates that multiple rotations of the unique word can be found, although not regularly}
    \label{fig:correlationscarrieroffset}
\end{figure}
