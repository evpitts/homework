\contentsline {section}{\numberline {1}Introduction}{2}{section.1}% 
\contentsline {section}{\numberline {2}Basic Image Recovery}{3}{section.2}% 
\contentsline {section}{\numberline {3}Phase Recovery Images}{6}{section.3}% 
\contentsline {subsection}{\numberline {3.1}Constant Phase Offset}{6}{subsection.3.1}% 
\contentsline {subsection}{\numberline {3.2}Carrier Frequency Offset}{9}{subsection.3.2}% 
\contentsline {section}{\numberline {4}Timing Recovery Images}{12}{section.4}% 
\contentsline {subsection}{\numberline {4.1}Constant Timing Offset}{12}{subsection.4.1}% 
\contentsline {subsection}{\numberline {4.2}Carrier Frequency Offset}{14}{subsection.4.2}% 
\contentsline {section}{\numberline {5}Final Recovery Image}{16}{section.5}% 
\contentsline {section}{\numberline {A}Parameters}{21}{appendix.A}% 
\contentsline {subsection}{\numberline {A.1}Initial Parameters}{21}{subsection.A.1}% 
\contentsline {subsection}{\numberline {A.2}Phase Offset Parameters}{22}{subsection.A.2}% 
\contentsline {subsection}{\numberline {A.3}Carrier Frequency Offset Parameters}{23}{subsection.A.3}% 
\contentsline {subsection}{\numberline {A.4}Timing Offset Parameters}{24}{subsection.A.4}% 
\contentsline {subsection}{\numberline {A.5}Clock Frequency Offset Parameters}{25}{subsection.A.5}% 
\contentsline {subsection}{\numberline {A.6}Final Offset Parameters}{26}{subsection.A.6}% 
\contentsline {section}{\numberline {B}Python Code}{27}{appendix.B}% 
\contentsline {subsection}{\numberline {B.1}final.py}{27}{subsection.B.1}% 
\contentsline {subsection}{\numberline {B.2}srrc1}{33}{subsection.B.2}% 
\contentsline {subsection}{\numberline {B.3}compute\_k}{34}{subsection.B.3}% 
\contentsline {subsection}{\numberline {B.4}get\_close\_point}{34}{subsection.B.4}% 
\contentsline {subsection}{\numberline {B.5}differentiator\_filter}{34}{subsection.B.5}% 
\contentsline {subsection}{\numberline {B.6}delay\_filter}{35}{subsection.B.6}% 
\contentsline {subsection}{\numberline {B.7}cubic\_interpolation}{35}{subsection.B.7}% 
\contentsline {subsection}{\numberline {B.8}convert\_uws\_l2s}{35}{subsection.B.8}% 
\contentsline {subsection}{\numberline {B.9}find\_uw}{36}{subsection.B.9}% 
\contentsline {subsection}{\numberline {B.10}find\_best\_corr}{36}{subsection.B.10}% 
\contentsline {subsection}{\numberline {B.11}plot\_correlation}{37}{subsection.B.11}% 
\contentsline {subsection}{\numberline {B.12}convert\_l2s}{37}{subsection.B.12}% 
\contentsline {section}{\numberline {C}List of Figures and Tables}{38}{appendix.C}% 
