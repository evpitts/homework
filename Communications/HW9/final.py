import numpy as np
import numpy.matlib
import math
import matplotlib.pyplot as plt
import matplotlib
import struct
import pdb
from scipy.stats import mode

import sys
sys.path.append('../HW2')
sys.path.append('../comms')

from srrc1 import srrc1
from comms import compute_k
from comms import get_close_point
from comms import differentiator_filter
from comms import delay_filter
from comms import cubic_interpolation
from comms import convert_uws_l2s
from comms import find_uw
from comms import find_best_corr
from comms import plot_correlation
from comms import convert_l2s

plt.close("all")
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plot_num = 1    # for plotting purposes

# Set parameters
Ts = 1             # Symbol period
N = 4              # Samples per symbol period
Omega0 = math.pi/2*(1)
Lp = 12
alpha = 0.7
L = 11
BnT = 0.12
zeta = 1/np.sqrt(2)
k0 = 1
kp = 1
k1PED,k2PED = compute_k(BnT,zeta,k0,kp)
print(k1PED,k2PED)
# Cubic interpolation
k1 = -0.006
k2 = -0.0000096289
num_rotations = 4

#Create the pulse envelope
p_t,n = srrc1(alpha,N,Lp,Ts)

##### Use 8 bits per symbol and 256 square QAM #####
B = 8;
bits2index = 2**np.arange(B-1,-1,-1)
M = 2 ** B
Mroot = math.floor(2**(B/2))
a = np.reshape(np.arange(-Mroot+1,Mroot,2),(2*B,1))
b = np.ones((Mroot,1))
LUT = np.hstack((np.kron(a,b), np.kron(b,a)))

# Scale the constellation to have unit energy
Enorm = np.sum(LUT ** 2) / M;
LUT = LUT/math.sqrt(Enorm);


##### Unique word (randomly generated) #####
uw = np.array([162,29,92,47,16,112,63,234,50,7,15,211,109,124,
               239,255,243,134,119,40,134,158,182,0,101,
               62,176,152,228,36])
uw_len = uw.size
uw = uw.reshape(uw_len,)
uwsym = LUT[uw,:]

##### Build the list of four possible UW rotations #####
angles = 2*math.pi*np.arange(0,4)/4
uwrotlocs = np.zeros((uw_len,2,4));

for i in range(angles.size):
  C = math.cos(angles[i])
  S = -math.sin(angles[i])
  G = np.array([[C, -S],[S, C]])
  uwrot = uwsym @ G;  # Rotate the UW symbols
  uwrotlocs[:,:,i] = uwrot; # Save the rotated version
  
uwrotsyms = convert_uws_l2s(uwrotlocs, LUT)

##### Reciever #####
fname = ['test2021',
         'sim1_2021',
         'sim2_2021',
         'sim3_2021',
         'sim4_2021',
         'sim5_2021',
         'sim6_2021']
fid=open(fname[6],'r')
r = np.genfromtxt(fid, delimiter=' ')

#Extract I and Q
n = np.arange(len(r))
I = math.sqrt(2)*np.cos(Omega0*n) * r
Q = -math.sqrt(2)*np.sin(Omega0*n) * r

#Convolve with pulse envelope
s_t = np.convolve(I,p_t)+1j*np.convolve(Q,p_t)

#### TED ####
delay = delay_filter(L)
diffr = differentiator_filter(N,L)

s_t_delay = np.convolve(delay,s_t)
s_t_diffr = np.convolve(diffr,s_t)

mu = 0

# Initialization
k = 0
theta_prev = 0
k2_prev = 0
theta = []
V21 = 0
NCO1 = 0
W1 = 1
MU1 = 0
axout = []
ayout = []
axout_rot = []
ayout_rot = []
el = []
el_TED = []
el_PED = []
mul = []
v_PED = []
v_TED = []
e_TED = 0
mu = NCO1/W1

const_list = list(zip(LUT.T[0],LUT.T[1]))

# sweep over the whole sequence (not sample-by-sample)
for n in range(len(s_t_delay)-4):
   # Compute NCO, strobe, and mu
   temp = NCO1 - W1
   if(temp < 0):
      #print("strobe on")
      strobe = 1
      mu = NCO1/W1
      mul.append(mu)
      nco = 1 + temp
   else:
      strobe = 0
      mu = MU1
      nco = temp
      
   if strobe == 0:
      e_TED = 0
   else:
     #Get rotation
     C = np.cos(theta_prev)
     S = -np.sin(theta_prev)
     
     # Compute interpolant xi and dxi
     xi_delay = cubic_interpolation(np.real(s_t_delay[n:n+4:1]),mu)
     yi_delay = cubic_interpolation(np.imag(s_t_delay[n:n+4:1]),mu)
     # Output of decision block
     axout.append(xi_delay)
     ayout.append(yi_delay)

     #Rotate the interpolants
     xr_delay = C*xi_delay - S*yi_delay
     yr_delay = S*xi_delay + C*yi_delay

     axout_rot.append(xr_delay)
     ayout_rot.append(yr_delay)
     
     # Get the estimated symbol
     a_0, a_1 = get_close_point(const_list, xr_delay, yr_delay)

     # Compute the interpolant of the derivative filter outputs
     xi_deriv = cubic_interpolation(np.real(s_t_diffr[n:n+4:1]),mu)
     yi_deriv = cubic_interpolation(np.imag(s_t_diffr[n:n+4:1]),mu)

     #Rotate the interpolants
     xr_deriv = C*xi_deriv - S*yi_deriv
     yr_deriv = S*yi_deriv + C*yi_deriv
     
     # Compute the error signal for TED
     e_TED = a_0*xr_deriv + a_1*yr_deriv
     el_TED.append(e_TED)
     
     # Compute the error signal for PED
     e_PED = a_0*yr_delay - a_1*xr_delay
     el_PED.append(e_PED)
     
     #Loop filter - PED
     vpp = k1PED*e_PED
     k2_prev = k2_prev + k2PED*e_PED
     v_PED.append(vpp+k2_prev)
     
     #Calculate theta_prev
     theta.append(v_PED[k] + theta_prev)
     theta_prev = theta[k]
     
     # Increment index
     k = k+1
     
   # Compute loop filter output  vt - TED
   v1 = k1*e_TED
   v2 = V21 + k2*e_TED   # V21 is last value of v2
   v_TED.append(v1 + v2)
   w = v_TED[n]+ 1/N
   
   # Compute states for next time.
   V21 = v2
   NCO1 = nco
   MU1 = mu
   W1 = w


#Plots
#Theta plot for phase images
fig = plt.figure(plot_num)
plot_num += 1
fig.suptitle("Phase Estimate $\\hat{\\theta}$")
ax0 = fig.add_subplot(121)
ax0.plot(range(len(theta)),theta)
ax0.set_xlabel("k")
ax0.set_ylabel("$\\hat{\\theta}$")
ax1 = fig.add_subplot(122,projection='polar')
theta_rad = np.pi/180*np.array(theta)
rho = np.arange(0,1,1/len(theta_rad))
ax1.plot(theta,rho)

#PED loop filter output
plt.figure(plot_num)
plot_num += 1
plt.plot(v_PED)
plt.title("Filtered Phase Error Signal")
plt.xlabel("k")
plt.ylabel("v")
plt.ylim([-0.5,0.5])

n = np.arange(len(mul))
#Mu plot for phase images
plt.figure(plot_num)
plot_num += 1
plt.scatter(n,mul)
plt.title("Fractional Interval $\\mu$")
plt.xlabel("$\mu$")
plt.ylabel("")

m = np.arange(len(v_TED))
#TED loop filter output
plt.figure(plot_num)
plot_num += 1
plt.scatter(m,v_TED)
plt.title("Filtered Timing Error Signal")
plt.xlabel("n")
plt.ylabel("v")
plt.ylim([-0.5,0.5])

C = [x/len(ayout) for x in range(len(ayout))]
colormap = 'cividis'
plt.figure(plot_num)
plot_num += 1
plt.scatter(ayout, axout, cmap = colormap, c = C)
cbar = plt.colorbar(
  matplotlib.cm.ScalarMappable(cmap=colormap),ticks=[0.0, 0.5, 1.0])
cbar.ax.set_yticklabels(['0','500','1000'])
plt.title("Interpolated MF Outputs Over Symbol Index")
plt.xlabel("Inphase location of symbol in A")
plt.ylabel("Quadrature location of symbol in A")
cbar.ax.set_ylabel("Symbol Index, n")

#Plot the symbols due to TED
plt.figure(plot_num)
plot_num += 1
plt.scatter(ayout_rot, axout_rot, cmap = colormap, c = C)
cbar = plt.colorbar(
  matplotlib.cm.ScalarMappable(cmap=colormap),ticks=[0.0, 0.5, 1.0])
cbar.ax.set_yticklabels(['0','500','1000'])
plt.title("Interpolated/phase-aligned MF Outputs Over Symbol Index")
plt.xlabel("Inphase location of symbol in A")
plt.ylabel("Quadrature location of symbol in A")
cbar.ax.set_ylabel("Symbol Index, n")

#Find the closest symbols
a_x_hat = []
a_y_hat = []
output_symbols = []
LUT_list = [[round(item[0],6),round(item[1],6)] for item in LUT]
for idx in range(len(axout)):
  a0, a1 = get_close_point(const_list,axout_rot[idx],ayout_rot[idx])
  a_x_hat.append(round(a0,6))
  a_y_hat.append(round(a1,6))
  output_symbols.append(LUT_list.index([round(a0,6),round(a1,6)]))

correlations = find_uw(uwrotsyms, output_symbols)
plot_correlation(correlations,plt,plot_num)
plot_num += 1


#Find the correlation with the highest number of 1.0s
corr_idx,uw_instances = find_best_corr(correlations)
periods = np.diff(uw_instances)
mode_period = np.unique(periods)

# Rotate the symbols
output_symbol_locs = LUT[output_symbols]
theta_uw = -angles[corr_idx]
G = [[np.cos(theta_uw), -np.sin(theta_uw)],
     [np.sin(theta_uw), np.cos(theta_uw)]]
output_symbols_rot = (G @ output_symbol_locs.T).T

output_symbols = convert_l2s(LUT_list,output_symbols_rot)

last_uw = uw_instances[-1]
#There are extra symbols on (I think) both ends, so I cut off anything
#that comes after the last instance of the uw. My reasoning is that by
#the end of the signal, my timing/phse error correction should be
#locked up, so the end of my signal is a more reliable barometer for
#when the last uw is.
output_symbols = output_symbols[:last_uw+uw_len]

#To get rid of the symbols on the beginning of the signal, I need to
#use the period of the unique word and its length to determine how
#many rows are in the image, and mod the signal by the number of rows
#to determine how many columns there should be. Working from the new
#end of the signal, I can leave any symbols that wouldn't fill an
#entire row
num_rows = mode_period[0] #The unique word is included here
leftovers = len(output_symbols)%num_rows

output_symbols = output_symbols[leftovers:]
  
plt.figure(plot_num)
plot_num += 1
#Reshape the image and show it
image = np.reshape(output_symbols,(-1,num_rows)).T
plt.imshow(255-image, cmap=plt.get_cmap('Greys'))

plt.show()
