from compute_k1_2 import compute_k
import numpy as np
import matplotlib.pyplot as plt
import pdb

plt.close('all')

num_samples = 80
n = np.arange(0,num_samples)
omega_n = 2*np.pi/20
theta = np.pi*np.heaviside(n,1)/18
BnT = 0.05
zeta = 1/np.sqrt(2)
k0 = 1
kp = 1
k1,k2 = compute_k(BnT,zeta,k0,kp)

DDS_arg_prev = -np.pi
k2_prev = 0

x = np.zeros(num_samples, dtype=complex)
f = np.zeros(num_samples)
v = np.zeros(num_samples)
args = omega_n*n+theta
error = np.zeros(num_samples)

DDS = [-1]#np.exp(1j*(omega_n*0+theta[0]))]
DDS_args = [-1]
DDS_cos = [0]

for n_in in range(0,num_samples):
    # #pdb.set_trace()
    arg = omega_n*n_in+theta[n_in]
    x[n_in] = np.exp(1j*(arg))
    #C.2.4,5
    #f[n_in] = np.angle(x[n_in]*np.conj(DDS[n_in]))
    #C.2.6,7
    f[n_in] = np.imag(x[n_in]*np.conj(DDS[n_in]))

    #Loop filter
    v[n_in] = k1*f[n_in] + k2*f[n_in] + k2_prev
    k2_prev = k2_prev + k2*f[n_in]
    
    #DDS
    DDS.append(np.exp(1j*DDS_arg_prev))
    DDS_cos.append(np.real(np.exp(1j*DDS_arg_prev)))
    DDS_args.append(DDS_arg_prev)
    
    error[n_in] = arg - DDS_arg_prev
    DDS_arg_prev = v[n_in] + omega_n + DDS_arg_prev

    # # #C.2.8,9
    # arg = omega_n*n_in+theta[n_in]
    # x[n_in] = np.real(np.exp(1j*(arg)))
    # f[n_in] = x[n_in]*DDS[n_in]

    # #Loop filter
    # v[n_in] = k1*f[n_in] + k2*f[n_in] + k2_prev
    # k2_prev = k2_prev + k2*f[n_in]
    
    # #DDS

    # DDS.append(-np.imag(np.exp(1j*DDS_arg_prev)))
    # DDS_cos.append(np.real(np.exp(1j*DDS_arg_prev)))
    # DDS_args.append(DDS_arg_prev)
    
    # error[n_in] = (arg - DDS_arg_prev)
    # DDS_arg_prev = v[n_in] + omega_n + DDS_arg_prev

    
#Figures
fig = plt.figure(1)
ax1 = fig.add_subplot(311)
ax2 = fig.add_subplot(312)
ax3 = fig.add_subplot(313)

#Real parts of input and DDS output sinusoids
ax1.plot(n, np.real(x), label='Re{Input}', linestyle='--', color='k')
#ax1.plot(n, DDS_cos[:-1], label='Re{DDS}', color='k')
ax1.plot(n, np.real(DDS[:-1]), label='Re{DDS}', color='k')
ax1.set_xlabel('Sample Index')
ax1.set_ylabel('Real Part of Sinusoid')
ax1.legend()

#Error
ax2.plot(n, error, label='error', color='k')
ax2.set_xlabel('Sample Index')
ax2.set_ylabel('e(n)')
ax2.legend()

#Sinusoidal arguments
#DDS_args = [x + 2*np.pi for x in DDS_args]
ax3.plot(n, args, label='Input arg', linestyle='--', color='k')
ax3.plot(n, DDS_args[:-1], label='DDS arg', color='k')
ax3.set_xlabel('Sample Index')
ax3.set_ylabel('Sinusoidal Arguments')
ax3.legend()

plt.show()
