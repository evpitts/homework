import numpy as np

def compute_k(BnT,zeta,k0,kp):
    theta_n= BnT/(zeta + 1/(4*zeta))
    k1 = 4*zeta*theta_n / (1 + 2*zeta*theta_n + theta_n**2)*1/(k0*kp)
    k2 = 4*theta_n**2 / (1 + 2*zeta*theta_n + theta_n**2)*1/(k0*kp)
    return k1,k2
