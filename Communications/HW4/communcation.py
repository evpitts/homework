import numpy as np
import math
import pdb
import matplotlib.pyplot as plt
from time import time
from datetime import timedelta
from scipy.stats import norm
from concurrent.futures import ThreadPoolExecutor, as_completed
from tqdm import tqdm

import sys
sys.path.insert(1, '../HW2/')

plt.close('all')
#Need to figure out the theoretical error rates (Q function stuff he was talking about in class)
#Need to calculate Eb for all 4 constellations, not always 1
Eb = 1       # average energy per bit for BPSK and QPSK
A = 1
M = 8
bps = int(math.log(M,2))

#Table for BPSK (assuming A is 1)
# LUT = [np.array([-1,1])*A,
#       np.array([0,0])*A]

# #Table for QPSK (Assuming A is 1)
# Eavg = A
# Eb = Eavg/bps
# LUT = [np.array([-1, 1, -1, 1]),
#        np.array([1, 1, -1, -1])]

# #Table for 8-PSK (Assuming A is 1)
# Eavg = np.sqrt(A)
# Eb = Eavg/bps
# LUT = [np.array([np.cos(0), np.cos(2*np.pi/8), np.cos(4*np.pi/8), np.cos(6*np.pi/8), np.cos(8*np.pi/8), np.cos(10*np.pi/8), np.cos(12*np.pi/8), np.cos(14*np.pi/8)]),
#        np.array([np.sin(0), np.sin(2*np.pi/8), np.sin(4*np.pi/8), np.sin(6*np.pi/8), np.sin(8*np.pi/8), np.sin(10*np.pi/8), np.sin(12*np.pi/8), np.sin(14*np.pi/8)])]

# #Table for CCITT 8 (Asusming A is 1)
Eavg = (4*(2*A**2)+4*(9*A**2))/8
Eb = Eavg/bps
LUT = [np.array([0, -1, 1, -3, 3, -1, 1, 0]),
       np.array([3, 1, 1, 0, 0, -1, -1, -3])]

# Choose a signal constellation LUT and determine its Es in terms of Eb
maxsymbolerrorcount = 50

#Create a random generator
rng = np.random.default_rng()

def process_symbol(sigma):
    Nsyms = 0    # number of symbols
    symbolerrorcount = 0
    biterrorcount = 0
    while(symbolerrorcount==0):
        Nsyms += 1
        #Generate a random integer
        symbol = np.random.randint(0,M)
        
        # lookup symbol s in constellation using this integer (this is the transmitted symbol)
        s_x = LUT[0][symbol]
        s_y = LUT[1][symbol]
        
        # channel
        n_x = sigma*rng.normal(0,1,1)
        n_y = sigma*rng.normal(0,1,1)
        # generate noise n with variance sigmaˆ2 in each coordinate direction
        # create r = s + n
        r_x = s_x + n_x
        r_y = s_y + n_y
        
        # receiver
        # slice the received value into the nearest symbol, call it s_hat
        x_dist = LUT[0] - r_x
        y_dist = LUT[1] - r_y
        
        Euc_dist = x_dist**2 + y_dist**2
        s_hat = np.argmin(Euc_dist)
        
        #Bitwise compare the original integer with the received integer
        diff = s_hat^symbol
        if diff > 0:
            symbolerrorcount += 1
            while diff > 0:
                biterrorcount += (diff & 1)
                diff >>= 1
    
    return Nsyms, biterrorcount

startSNRdB = 0
endSNRdB = 21
SNRdBstep = 1

Ps_all = []
Pb_all = []
theo_all = []
runtime_all = []

start_time = time()
with ThreadPoolExecutor() as executor:
    for SNRdb in range(startSNRdB,endSNRdB,SNRdBstep):
        loop_start_time = time()
        #compute SNR from SNRdB
        SNR = 10**(SNRdb/10)
        N0 = Eb/SNR         #SNR per bit
        sigma_2 = N0/2
        sigma = np.sqrt(sigma_2)

        it = []
        desc_width = len(str(endSNRdB-1))
        desc = ('SNR {:>' + str(desc_width) + '}').format(SNRdb)
        n_width = len(str(maxsymbolerrorcount))
        bar_format = '{l_bar}{bar}| {n_fmt:>' + str(n_width) + '}/{total_fmt} [{elapsed}]'
        with tqdm(total=maxsymbolerrorcount, position=0, desc=desc, bar_format=bar_format) as pbar:
            futures = [executor.submit(process_symbol, sigma)
                for _ in range(maxsymbolerrorcount)]
            for future in as_completed(futures):
                it.append(future.result())
                pbar.update(1)
                pbar.refresh()
                
        Nsyms, biterrorcount = list(map(sum, zip(*it)))
        

        numbits = Nsyms*bps
        Psymerror = maxsymbolerrorcount/Nsyms
        Pbiterror = biterrorcount/numbits

        Ps_all.append(Psymerror)
        Pb_all.append(Pbiterror)
        
        #For BPSK
        #theoretical_Ps = 1-norm.cdf(np.sqrt(2*Eb/N0))
        
        #For QPSK
        #theoretical_Ps = 2*(1 - norm.cdf(1/sigma)) - (1-norm.cdf(1/sigma))**2
        
        #For 8PSK - using the union bound, no reduced terms
        # theoretical_Ps = 2*(1-norm.cdf(np.sqrt(2*Eb*bps/N0*np.sin(np.pi/8)**2)) + 1-norm.cdf(np.sqrt(2*Eb*bps/N0*np.sin(np.pi/4)**2)) + 1-norm.cdf(np.sqrt(2*Eb*bps/N0*np.sin(3*np.pi/8)**2))) + 1-norm.cdf(np.sqrt(2*Eb*bps/N0))
        #For CCITT
        theoretical_Ps = 8/(8*3)*(1-norm.cdf(np.sqrt(4*3*Eb/(2*5.5*N0)))) + 16/(8*3)*(1-norm.cdf(np.sqrt(5*3*Eb/(2*5.5*N0)))) + 4/(8*3)*(1-norm.cdf(np.sqrt(8*3*Eb/(2*5.5*N0)))) + 16/(8*3)*(1-norm.cdf(np.sqrt(17*3*Eb/(2*5.5*N0)))) + 8/(8*3)*(1-norm.cdf(np.sqrt(18*3*Eb/(2*5.5*N0)))) + 4/(8*3)*(1-norm.cdf(np.sqrt(36*3*Eb/(2*5.5*N0))))

        
        theo_all.append(theoretical_Ps)
        
        run_time = time() - loop_start_time
        runtime_all.append(run_time)
        
end_time = time()
total_run_time = end_time - start_time

print() # new line
for i, run_time in enumerate(runtime_all):
    print("Time to run SNR loop", i, ":", timedelta(seconds=run_time))
print("Total runtime in seconds: ", timedelta(seconds=total_run_time))

plt.figure(1)
plt.semilogy(np.arange(startSNRdB,endSNRdB,SNRdBstep), Pb_all, marker="s", label="Probability of bit error")
plt.semilogy(np.arange(startSNRdB,endSNRdB,SNRdBstep), Ps_all, marker="X", label="Probability of symbol error")
plt.semilogy(np.arange(startSNRdB,endSNRdB,SNRdBstep), theo_all, marker="X", label="Theoretical probability of symbol error")
plt.xlabel("Eb/N0 in dB")
plt.ylabel("Probability of bit error")
plt.title("Probabilities of error at " + str(maxsymbolerrorcount) + " maximum symbol error")
plt.legend()

plt.figure(2)
plt.plot(np.array(runtime_all)/60, marker="s", label="Run times")
plt.xlabel("Eb/N0 in dB")
plt.ylabel("Run times in minutes")
plt.title("Run times for each SNR value")

plt.show()
