\documentclass[answers,addpoints,12pt]{exam}

%For references
\usepackage{hyperref}

%For nomenclature
\usepackage{nomencl}

%For images
\usepackage{graphicx}
%\usepackage{subcaption}
\usepackage{float}
\graphicspath{ {images/} }

%For colors
\usepackage{color, colortbl}

%For align environment
\usepackage{amsmath}

%New commands - todo
\newcommand{\todo}[1]{{\color{red}[TODO: #1]}}
\renewcommand{\solutiontitle}{}

\author{Emily Pitts}
\title{\LARGE \bf
  MSK and DVB Standards
}

\begin{document}
\maketitle
\unframedsolutions

\section{MSK questions}
\label{secMSKQuestions}

\begin{questions}
  \question
  From the MSK document
  \begin{parts}
    \part
    What is frequency shift keying? What is binary frequency shift keying?
    \begin{solution}
      Frequency shift keying is a simple method of transmission in which the frequency of a carrier signal is changed. There are many variations on FSK, including BFS, or Binary Frequency Shift Keying. Binary shift keying is the ``simplest form of FSK'' in which the bits 0 and 1 correspond to the frequencies $F_0$ and $F_1$, which are distinct carrier frequencies. The bits are converted to symbols in the following relationship
      \begin{align}
        \label{eq:BitRelations}
        \begin{split}
          0 & \rightarrow -1 \\
          1 & \rightarrow +1
        \end{split}
      \end{align}
     which means the frequencies can be expressed as
      \begin{equation}
        \label{eq:Freq}
        F_i = F_c + (-1)^{i+1}\Delta_F = F_c \pm \Delta_F,
      \end{equation}
      where $i \in \{0,1\}$, $F_C$ is the nominal carrier frequency, and $\Delta_F$ is the peak frequency deviation from the carrier frequency.
      The signal $s(t)$ can then be expressed as
      \begin{equation}
        \label{eq:s_t}
        \begin{aligned}
          s_i(t) & = A\cos(2\pi F_it) \\
          & = A*cos(2\pi\{F_c\pm\Delta_F\}t),
        \end{aligned}
      \end{equation}
      where $0 \leq t \leq T_B$. \\
      In \autoref{fig:BFSKExample}, an example of BFSK with a random bit stream is shown. $F_0$ changes the carrier frquency by a greater amount than $F_1$. Note that the bit period and the symbol period are the same for BFSK, which is why they align in the figure. Also note that the bit transitions are, in general, discontinuous, which is seen in the misalignment of the signal at the edges of the bit/symbol period. MSK can actually achieve continuity of phase, which will be shown later.
    \end{solution}

    %Figures need to go outside the solution box or they get lost
    \ifprintanswers
    \begin{figure}[H]
        \centering
        \includegraphics[width=0.4\linewidth]{BFSKExample.png}
        \caption{Example of BFSK}
        \label{fig:BFSKExample}
    \end{figure}
    \fi

    \part
    What is the minimum frequency seperation $\Delta_F$ which gives orthogonal signals? Work through the derivation on pp. 2-3 to explain why
    \begin{solution}
      Orthogonality between two functions $f(t)$ and $g(t)$ over the range {a,b} is defined as
      \begin{equation}
        \label{eq:GeneralOrth}
        \int_a^{b}\bar{f(t)}g(t) dt = 0
      \end{equation}
      Using the distinct frequencies $F_0$ and $F_1$, the fact that cos is a real signal, and the fact that these frequencies only need to be orthogonal on the range \{0,$T_b$\}, the equation that needs to be satisfied is,
      \begin{equation}
        \label{eq:BFSKOrth}
        \int_0^{T_b}s_1(t)s_0(t) dt = 0,
      \end{equation}
      or
      \begin{equation}
        \label{eq:BFSKOrthFull}
        \int_0^{T_b}\cos(2\pi F_1t)\cos(2\pi F_0t) dt = 0.
      \end{equation}
      When this integral is rewritten using $\cos \left( {\theta _1 \pm \theta _2 } \right) = \cos \theta _1 \cos \theta _2 \mp \sin \theta _1 \sin \theta _2$ twice (once with $+$ and once with $-$) and evaluated, the result is
      \begin{equation}
        \frac{\sin(2\pi (F_1 + F_0)t}{2\pi (F_1 + F_0)} + \frac{\sin(2\pi (F_1 - F_0)t}{2\pi (F_1 - F_0)} = 0 
      \end{equation}
      Due to the fact that $F_1 + F_0 \gg 1$, the first term goes to zero, and this reduces to,
      \begin{equation}
        \sin(2\pi (F_1 - F_0)T_b = 0.
      \end{equation}
      Taking the inverse sin of both sides, and remembering that $\sin = k\pi$ for integer $k$
      \begin{equation}
        2\pi(F_1 - F_0)T_b = 0,
      \end{equation}
      which reduces to
      \begin{equation}
        F_1 - F_0 = \frac{k}{2T_b}
      \end{equation}
      which is then the minimum frequency separation.
    \end{solution}
    \part
    Explain carefully why it is desirable to have continuous phase
    \begin{solution}
      Sharp transitions between 0s and 1s can cause signals to have sidebands that extend out quite a ways from the carrier, which can cause interference. Achieving phase continuity can prevent this and reduce the needed bandwidth. Additionally, continuous phase gives good power efficiency.
    \end{solution}
    \part
    Explain carefully how MSK achieves continuity of phase
    \begin{solution}
      Phase remains continuous by virute of the offset behavior of MSK. MSK has a frequency difference that is always equal to half of the data rate. Because sinusoidal signals are being used, a frequency difference equal to half of the data rate means that a multiple of half of the sinusoidal is contained in the symbol time, so at transitions, the sinusoidal signal is always at a zero crossing.
    \end{solution}
    \part
    Work through the derivation of the formula $\theta_n = \theta_{n-1} + a_{n-1}\pi/2$ in Eq(3).
    \begin{solution}
      \begin{equation}
        \left. 2\pi\frac{a_nR_b}{4}(t-nT_b)+\theta_n \right\rvert_{t=(n+1)T_b} = \left. 2\pi\frac{a_{n+1}R_b}{4}(t-(n+1)T_b)+\theta_{n+1} \right\rvert_{t=(n+1)T_b}
      \end{equation}
      Doing the evaluation,
      \begin{equation}
        2\pi\frac{a_nR_b}{4}((n+1)T_b-nT_b)+\theta_n = 2\pi\frac{a_{n+1}R_b}{4}((n+1)T_b-(n+1)T_b)+\theta_{n+1} 
      \end{equation}
      \begin{equation}
        \pi\frac{a_nR_b}{2}(T_b)+\theta_n = \theta_{n+1}
      \end{equation}
      Recall that
      \begin{equation}
        R_b = \frac{k}{2T_b}
      \end{equation}
      and simplify to
      \begin{equation}
        \pi\frac{a_n}{2}+\theta_n = \theta_{n+1}
      \end{equation}
    \end{solution}
    \part
    Work through the steps to derive Eq (8)
    \begin{solution}
      Starting with Eq(5),
      \begin{equation}
        s(t) = \cos \left[2\pi F_ct+2\pi\frac{a_nR_b}{4}t-na_n\frac{\pi}{2}+\theta_n \right]
      \end{equation}
      we again use $\cos \left( {\theta _1 \pm \theta _2 } \right) = \cos \theta _1 \cos \theta _2 \mp \sin \theta _1 \sin \theta _2$ to get
      \begin{equation}
        s(t) = \cos2\pi F_ct\cdot\cos\left(2\pi\frac{a_nR_b}{4}t +
        \Theta_n\right) - \sin2\pi F_ct\cdot\sin\left(2\pi\frac{a_nR_b}{4}t + \Theta_n\right)
      \end{equation}
      Expanding the second $cos$ of the first term we get ,
      \begin{equation}
        \cos\left(2\pi\frac{a_nR_b}{4}t + \Theta_n\right) = d_{I,n}\cos2\pi\frac{R_b}{4}t
      \end{equation}
      because $a_n \in \{-1,+1\}$ and $d_{I,n} = \cos Theta_n$
      Expanding the second $sin$ of the second term we get ,
      \begin{equation}
        \sin\left(2\pi\frac{a_nR_b}{4}t + \Theta_n\right) = d_{Q,n}\sin2\pi\frac{R_b}{4}t
      \end{equation}
      because $d_{Q,n} = a_n\cdot\cos Theta_n = a_n\cdot d_{I,n}$
    \end{solution}
    Plugging back in to $s(t)$ and using the identity $\cos\alpha=\sin(\alpha+\pi/2)$,
    \begin{equation}
      s(t) = d_{I,n}\sin(2\pi\frac{R_b}{4}t+\frac{\pi}{2})\cdot\cos2\pi F_ct - d_{Q,n}\sin(2\pi\frac{R_b}{4}t+\frac{\pi}{2})\cdot\sin2\pi F_ct
    \end{equation}
    \begin{equation}
      s(t) = d_{I,n}p(t + \frac{1}{2}2T_b)\cdot\cos2\pi F_ct - d_{Q,n}p(t)\cdot\sin(2\pi F_ct)
    \end{equation}
    where $p(t)$ is a half-sinusoidal pulse shape with a period of $4T_b$
    \part
    How is MSK both linear and nonlinear modulation?
    \begin{solution}
      MSK is actually non-linear, but it can be cast as a linear modulation scheme by treating it as offset QPSK, which is a linear modulation scheme.
    \end{solution}
  \end{parts}
\end{questions}

\section{Summary of  DVB-S2 and Its Operational Blocks}
\label{sec:Summary}
Digital Video Broadcasting V1.2.1 (2009-08), or DVB-S2. This standard specifies QPSK modulation, concatenated convolutional and Reed-Solomon channel coding. It was introduced as a standard in 1994. In addition to the DVB-S standard, DVB-DSNG was introduced in 1997, and specifies the use of 8PSK and 16QAM modulation. This document presents a second generation modulation and channel coding system, or DVB-S2, necessitated by the evolution of digital satellite technology since the introduction of the standard. DVB-S2 is characterized by
\begin{itemize}
\item A flexible input stream adapter
\item An FEC system based on LDPC codes concatenated with BCH codes
\item a wide range of code rates, and 4 constellations
\item three spectrume rates
\item Adaptive Coding and Modulation (ACM) functionality
\end{itemize}

\section{General DVB-S2 system}
\label{sec:DVB}
A functional block diagram for DVB-S is shown in \autoref{fig:DVBBlock}. Any block colored yellow is required for the minimal system to run. Any block with a dotted outline are relevant only for the multiple input stream broadcasting system. A brief overview of the blocks for the minimal system follows.
    \begin{figure}[H]
        \centering
        \includegraphics[width=0.75\linewidth]{DVB-S-BlockDiagram.png}
        \caption{Block Diagram of the DVB-S2 System, taken from the standard}
        \label{fig:DVBBlock}
    \end{figure}
    \subsection{Input Interface}
    \label{sec:II}
    The input interface system maps the incoming signal from electrical format to internal logical-bit format. The first bit that is received is designated as the most significant bit.
    \subsection{CRC-8 Encoder}
    \label{sec:CRC}
    The purpose of the CRC-8 Encoder block is to detect accidental changes to raw data. Data that enters this system have an attached value based on the polynomial division of their contents, sometimes known as the sync bit. This calculation can be repeated inside the decoder block and if the calculations do not match, changed data has been detected and can then be corrected.

    If UPL (User Packet Length) is $0_D$, this sub-system passes through the stream without modification. Otherwise, the sync bit precedes the input stream sequence of User Packets (UP). Then following generator function can then be used to encode the UP
    \begin{equation}
      \label{eq:GenFunc}
      g(X) = (X^5+X^4+X^3+X^2+1)(X^2+X+1)(X+1) = X^8+X^7+X^6+X^4+X^2+1
    \end{equation}
    The output of the CRC encoder is then computed as
    \begin{equation}
      \label{eq:CRC}
      CRC = remainder [X^8 u(X) : g(X)]
    \end{equation}
    where $u(X)$ is the input sequence that is to be encoded.
    \subsection{Merger Slicer}
    \label{sec:Merger}
    In the single stream system, this block reduces to a slicer block. The slicer reads from its input a Data Field, od a certain number of bits. When a merger is employed, the merger then merges that data field through concatentation to the other data fields into a single output.
    \subsection{BB Scrambler}
    \label{sec:BBScrambler}
    The BB Scrambler scrambles an input signal. This may seem counterintuitive as clean capture and interperatition of signals is difficult enough, but one purpose of scrambling is to reduce the length of consecutive 0s or 1s, which could cause synchronization problems.
    \subsection{BCH Encoder}
    \label{sec:BCH}
    BCH codes are error-correcting codes constructed using polynomials over a finite field. BCH codes are desirable due to their precise control over the number of correctable symbol errors and their decoding ease, which simplifies the hardware of the decoder.

    The BCH encoder takes an input stream and converts it to a BCH code. Tables containing the polynomials used in the BHC encoding are provided in the standard.
    \subsection{LDPC Encoder}
    \label{sec:LDPC}
    Low-density parity-check codes (LDPC codes) are linear error correcting codes, a method of correcting transmission errors and transmitting a message over a noisy channel. When a signal is transmitted, sometimes a bit is so corrupted that the receiver cannot tell if it was meant to be a 0 or 1. This is called bit erasure. To counter erasure, one could send a copy of the signal, so the receiver can look up what each bit is. However, the copy can also be corrupted. This is where parity bits come in handy. A parity bit is used to reconstruct how many 1s a signal is supposed to have. A parity check set is an arrangement of parity bits corresponding to overlapping sets of signal bits. Then, when a signal bit is erased, the receiver can check the parity bits of every parity check set that contains this bit, and utilize the overlapping information about sets of bits to determine what the bit was meant to be. When bit erasures occur in the signal, well coded parity check sets can recover those erased bits. For long signals, low-density parity-check sets need to be used to simplify the problem of decoding.
    LDPC paramters $n_{ldpc}$, $k_{ldpc}$ are given in tables in the standard.
    \subsection{Bit Interleaver}
    \label{sec:Interleaver}
    The point of the bit interleaver is to distribute grouped bit errors more evenly across a block of data.
    \subsection{Bit Mapper Into Constellations}
    \label{sec:BitMapper}
    The bit mapper uses a Look Up Table (LUT) to map bits to signal amplitudes based on the signal constellation.
    Uses serial-to-parallel conversion, and generates (I,Q) sequence of variable length. QPSK employs Gray-coded modulation with absolute mapping. The mapping follows \autoref{fig:QPSKGrayCoding}, where the average energy per symbol is $\rho^2=1$.
    \begin{figure}[H]
        \centering
        \includegraphics[width=0.5\linewidth]{QPSKGrayCoding.png}
        \caption{The constellation and coding for QPSK in DVB-S2}
        \label{fig:QPSKGrayCoding}
    \end{figure}
    \subsection{PL Scrambler}
    \label{sec:PLScrambler}
    Physical layering scrambling is done prior to modulation. This is done by multiplying the (I+jQ) samples by a complex randomization sequence. The sequences are constructed by combining two real m-sequences (which are generate from two generator polynomials of degree 18) into a complex sequence.
    \subsection{BB Filter and Quadrature Modulation}
    \label{sec:QuadMod}
    DVB-S2 uses the discrete time square root raised cosine (SRRC) filter. The roll-off factor is $\alpha=0.35,0.25,0.20$. Quadrature modulation is performed by multiplying the inphase and quadrature signals by $\sin(2\pi f_0t)$ and $\cos(2\pi f_0t)$ respectively, where $f_0$ is the carrier frequency, and then adding then together to obtain the final output.

\end{document}
