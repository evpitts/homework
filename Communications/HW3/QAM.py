import math
import numpy as np
from numpy.random import rand
import matplotlib.pyplot as plt
import pdb
import itertools

import sys
sys.path.insert(1, '../HW2/')

from convert_bits import convert_bits
from MaryConst import MaryConst
from srrc1 import srrc1


def freqd(ns):
    if np.remainder(ns,2) == 1:
        temp = np.arange((ns-1)/2+1)
        fn = np.concatenate((-temp[::-1][:-1],temp))/ns
    else:
        fn = np.arange(-ns/2,ns/2)/ns
    return fn

plt.close('all')
fig_size = (24,8)

#Required values for pulse and constellation formation
alpha = 0.5     # excess bandwidth
N = 201         # samples per symbol
Lp = 60         # SRRC truncation length
Ts = 1          # symbol time in seconds
T = Ts/N        # sample time in seconds
Nsym = 1000      # number of symbols
omega_c = 2*math.pi*50  # carrier frequency - must be less than half of the sampling rate, N
#M = 4           # number of constellation points - QPSK
M = 8           # number of constellation points - 8PSK
A = 1           # amplitude
symSize = math.log(M,2)  # size of each symbol
const_type = "8"

plot_num = 1    # for plotting purposes

#Create the pulse envelope
p_t, n = srrc1(alpha,N,Lp,Ts)
# plt.figure(plot_num,figsize=fig_size)
# plot_num += 1
# plt.plot(n,p_t)
# plt.xlabel("Ts/N")
# plt.ylabel("Amplitude, 1/sqrt(N)")
# plt.title("Pulse Envelope, p_t")

#Create the LUT (for QAM/QPSK)
Eavg,LUT = MaryConst(M,A,const_type)             # LUT and average energy

#Create a random stream of bits for the signal
bits = (rand(int(Nsym*symSize)) > 0.5).astype(int)     # random bits
#bits = [1,1]

#Convert binary to decimal to use as indexer
converted_bits = np.array(convert_bits(bits, M))

#Run the bits through the LUT and plot for check
# phi_0 = [int(x%math.sqrt(M)) for x in converted_bits]  # get the x coordinates (coeff. of A)
# phi_1 = [int(x/math.sqrt(M)) for x in converted_bits]  # get the y coordinates (coeff. of A)

# a_0 = np.array([LUT[0][x] for x in phi_0])    # run through the appropriate LUT
# a_1 = np.array([LUT[1][x] for x in phi_1])    # run through the appropriate LUT

#FOR 8PSK
a_0 = np.array([LUT[0][x] for x in converted_bits])
a_1 = np.array([LUT[1][x] for x in converted_bits])

plt.figure(plot_num,figsize=fig_size)
plot_num += 1
plt.stem(a_0, linefmt='green', markerfmt='go',  use_line_collection='True', label='I(t)')
plt.stem(a_1, linefmt='blue', markerfmt='bo', use_line_collection='True', label='Q(t)')
plt.xlabel("Symbol Number")
plt.ylabel("Distance from Origin in Units of A")
plt.title("Symbol Locations in the Constellation Space, Transmitter")
plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.075),
          fancybox=True, shadow=True, ncol=5)

#Upsample
a_0_up = np.zeros(N*Nsym)              # this will produce a column vector of zeros
a_0_up[0::N] = a_0              # creating the upsampled signal

a_1_up = np.zeros(N*Nsym)              # this will produce a column vector of zeros
a_1_up[0::N] = a_1              # creating the upsampled signal

#Convolve the upsampled/zero-padded bit stream with the pulse envelope
I = np.convolve(a_0_up, p_t)  # complete the multiplication of the basis function (or the filter)
Q = np.convolve(a_1_up, p_t)

# plt.figure(plot_num,figsize=fig_size)
# plot_num += 1
# plt.plot(I, label='I(t)')
# plt.plot(Q, label='Q(t)')
# plt.xlabel("Ts/N")
# plt.ylabel("Amplitude")
# plt.title('I(t) and Q(t) convolved with p_t')
# plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.075),
#           fancybox=True, shadow=True, ncol=5)

#Compute the autocorrelation function
r_t_I = np.correlate(I,I,'full')#r_t = np.convolve(p_t,p_t)
t_r_I = np.linspace(-len(r_t_I)/2, len(r_t_I)/2, len(r_t_I))

# #Calculate the shifted fft of the autocorrelation function in dBs
auto_fft_I = 10*np.log10(np.abs(np.fft.fftshift(np.fft.fft(r_t_I)))**2)

plt.figure(plot_num,figsize=fig_size)
plot_num += 1
plt.plot(N/Ts*freqd(len(auto_fft_I)),auto_fft_I)
plt.show()
plt.xlabel("Frequency in Hz")
plt.ylabel("Amplitude in dBs")
plt.title("Power Spectral Density of I(t)")

#For eye diagrams, start a half symbol early
offset = (Lp*N)          #Offset due to convolution in samples

Nsymtoss = np.ceil(Lp)   #Number of symbols at the end of the signal to throw away

nc = (np.floor((len(I) - offset - Nsymtoss*N)/N)).astype(int) #number of symbols that will be extracted from the signal

#Reshape the signals so they are easier to plot
Ireshape = I[offset:offset + nc*N].reshape(nc,N)
Qreshape = Q[offset:offset + nc*N].reshape(nc,N)

#Plot the phase trajectory
#plot I(t) on x axis, Q(t) on y axis
plt.figure(plot_num,figsize=fig_size)
plot_num += 1
plt.plot(Ireshape.T, Qreshape.T, color='b', linewidth=0.25)
plt.xlabel("I(t) transmitter")
plt.ylabel("Q(t) transmitter")
plt.title("Phase Trajectory of Transmitted Signals")

t_trig = Ts/N*np.arange(len(I)) #Using the relationship between T, Ts, and N, the 

# #QAM has two basis functions, cos() and sin()
#Make the final trasnmitted signal by summing the basis functions convolved/multiplied by the detla trains of binary streams
s_t = I*math.sqrt(2)*np.cos(omega_c*t_trig) - Q*math.sqrt(2)*np.sin(omega_c*t_trig)

#Compute the autocorrelation function
r_t_s = np.correlate(s_t,s_t,'full')#r_t = np.convolve(p_t,p_t)
t_r_s = np.linspace(-len(r_t_s)/2, len(r_t_s)/2, len(r_t_s))

# #Calculate the shifted fft of the autocorrelation function in dBs
auto_fft_s = 10*np.log10(np.abs(np.fft.fftshift(np.fft.fft(r_t_s)))**2)

plt.figure(plot_num,figsize=fig_size)
plot_num += 1
plt.plot(N/Ts*freqd(len(auto_fft_s)),auto_fft_s)
plt.show()
plt.xlabel("Frequency in Hz")
plt.ylabel("Amplitude in dBs")
plt.title("Power Spectral Density of s(t)")

# plt.figure(plot_num,figsize=fig_size)
# plot_num += 1
# plt.plot(s_t)
# plt.title('s(t)')
# plt.xlabel("Amplitude")
# plt.ylabel("Ts/N")

# #Multiply by the rest of the basis functions to get them out of the way
I_r = s_t*math.sqrt(2)*np.cos(omega_c*t_trig)
Q_r = -s_t*math.sqrt(2)*np.sin(omega_c*t_trig)

x_t = np.convolve(I_r,p_t)  # convolve again to get rid of the filter
y_t = np.convolve(Q_r,p_t)  # convolve again to get rid of the filter

# #plt.figure(plot_num,figsize=fig_size)
# fig, axs = plt.subplots(2)
# plot_num += 1
# axs[0].plot(x_t)
# axs[1].plot(y_t)
# plt.title('Matched filter output')

offset_recv = (2*Lp*N + np.floor(N/2)).astype(int)

Nsymtoss_recv = 2*np.ceil(Lp)+1   # throw away symbols at the end

nc_recv = (np.floor((len(x_t) - offset_recv - Nsymtoss_recv*N)/N)).astype(int) # number of points of signal (in samples)

xreshape = x_t[offset_recv:offset_recv + nc_recv*N].reshape(nc_recv,N)
yreshape = y_t[offset_recv:offset_recv + nc_recv*N].reshape(nc_recv,N)

fig, axs = plt.subplots(2)
plot_num += 1
axs[0].plot(np.arange(-np.floor(N/2), np.floor(N/2)+1), xreshape.T,color='b', linewidth=0.25, label="I(t)")
axs[1].plot(np.arange(-np.floor(N/2), np.floor(N/2)+1), yreshape.T,color='g', linewidth=0.25, label="Q(t)")
#plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.075),
          #fancybox=True, shadow=True, ncol=5)
plt.title("Eye Diagrams for the Receiver")
# #TODO: Add labels to these subplots

# # back_end_x = np.roll(xreshape[:,-1],1)
# # front_end_x = np.roll(xreshape[:,0],-1)
# # new_reshape_x = np.c_[back_end_x, np.c_[xreshape,front_end_x]]
# # final_reshape_x = new_reshape_x[1:-1,:]

# # back_end_y = np.roll(yreshape[:,-1],1)
# # front_end_y = np.roll(yreshape[:,0],-1)
# # new_reshape_y = np.c_[back_end_y, np.c_[yreshape,front_end_y]]
# # final_reshape_y = new_reshape_y[1:-1,:]

# #Phase trajectories
# #plot I(t) on x axis, Q(t) on y axis for the reciever
plt.figure(plot_num,figsize=fig_size); plt.clf()
plot_num += 1
plt.plot(xreshape.T, yreshape.T, color='b', linewidth=0.25)
plt.xlabel("I(t) receiver")
plt.ylabel("Q(t) receiver")
plt.title("Phase Trajectory of Received Signals")
# #plt.plot(final_reshape_x.T, final_reshape_y.T, color='b', linewidth=0.25)

x_r = np.zeros(Nsym)
y_r = np.zeros(Nsym)
x_r_corrected = np.zeros(Nsym)
y_r_corrected = np.zeros(Nsym)
for i in range(int(len(bits)/symSize)):
    x_r[i] = x_t[(2*Lp+1)*N + i*N]
    y_r[i] = y_t[(2*Lp+1)*N + i*N]
    if x_r[i] < 0:
        x_r_corrected[i] = -1
    else:
        x_r_corrected[i] = 1
    if y_r[i] < 0:
        y_r_corrected[i] = -1
    else:
        y_r_corrected[i] = 1
plt.figure(plot_num,figsize=fig_size)
plot_num += 1
plt.stem(x_r,  markerfmt='go', linefmt='green', use_line_collection='True', label='I(t)')
plt.stem(y_r,  markerfmt='bo', linefmt='blue', use_line_collection='True', label='Q(t)')
plt.xlabel("Symbol Number")
plt.ylabel("Distance from Origin in Units of A")
plt.title("Symbol Locations in the Constellation Space")
plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.075),
          fancybox=True, shadow=True, ncol=5)

plt.figure(plot_num,figsize=fig_size)
plot_num += 1
plt.stem(x_r_corrected,  markerfmt='go', linefmt='green', use_line_collection='True', label='I(t)')
plt.stem(y_r_corrected,  markerfmt='bo', linefmt='blue', use_line_collection='True', label='Q(t)')
plt.xlabel("Symbol Number")
plt.ylabel("Distance from Origin in Units of A")
plt.title("Symbol Locations in the Constellation Space, After the Decision Box")
plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.075),
          fancybox=True, shadow=True, ncol=5)

#Extract the constellation points
if const_type == "8":
    const_points = LUT
    temp = range(8)
else:
    const_points = list(zip(*list(itertools.product(*LUT))))

#Plot the constellation points
plt.figure(plot_num,figsize=fig_size)
plot_num += 1
plt.scatter(const_points[0],const_points[1])
# for i, txt in enumerate(temp):
#     plt.annotate(txt, (const_points[0][i], const_points[1][i]))
plt.xlabel("I(t) in terms of A")
plt.ylabel("Q(t) in terms of A")    
plt.title("Constellation Points")

#TODO:Do the Euclidean distance to finish the decision block
# #closest_x = LUT[0][:] - x_t[0]

#Compute the autocorrelation function
r_t = np.correlate(s_t,s_t,'full')#r_t = np.convolve(p_t,p_t)
t_r = np.linspace(-len(r_t)/2, len(r_t)/2, len(r_t))

# #Calculate the shifted fft of the autocorrelation function in dBs
auto_fft = 10*np.log10(np.abs(np.fft.fftshift(np.fft.fft(r_t)))**2)
 
plt.figure(plot_num,figsize=fig_size)
plot_num += 1
plt.plot(N/Ts*freqd(len(auto_fft)),auto_fft)
plt.xlabel("Frequency in Hz")
plt.ylabel("Amplitude in dBs")
plt.title("Power Spectral Density")

x_diff = a_0 - x_r_corrected
y_diff = a_1 - y_r_corrected
plt.figure(plot_num,figsize=fig_size)
plot_num += 1
plt.stem(x_diff,  markerfmt='go', linefmt='green', use_line_collection='True', label='I(t)')
plt.stem(y_diff,  markerfmt='bo', linefmt='blue', use_line_collection='True', label='Q(t)')
plt.xlabel("Symbol Number")
plt.ylabel("Distance from Origin in Units of A")
plt.title("Symbol Locations in the Constellation Space, Difference Between Transmitted and Sliced Bits")
plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.075),
          fancybox=True, shadow=True, ncol=5)
plt.show()
