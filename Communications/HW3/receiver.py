def receiver(r_t,omega_x,t,p_t,N):
    
    #Multiply by the basis functions again
    I_r = r_t*math.sqrt(2)*np.cos(omega_c*t)
    Q_r = -r_t*math.sqrt(2)*np.sin(omega_c*t)

    #Convolve with the pulse envelope
    x_nT = np.convolve(I_r,p_t)
    y_nT = np.convolve(Q_r,p_t)

    #Determine when to sample based on the envelope length, the upsample rate, and number of bad symbols at the end
    # first peak at 2*Lp, then every N samples after that
    offset = ((len(p_t)-1) - np.floor(N/2)).astype(int)
    
    Nsymtoss = 2*np.ceil(Lp/N)   # throw away length of pulse envelope (b/c convolution will leave a length of non-overlapping )
    
    nc = (np.floor((len(x_t) - offset - Nsymtoss*N)/N)).astype(int) # number of points in signal constellation

    #For eye diagrams
    # x_kTs = x_nT[offset:offset + nc*N].reshape(nc,N)
    # y_kTs = y_nT[offset:offset + nc*N].reshape(nc,N)

    #Sample, starting at 2*Lp (size of the pulse envelope)
    x_kTs = np.zeros(Nsym)
    y_kTs = np.zeros(Nsym)
    for i in range(int(len(bits)/symSize)):
        x_kTs[i] = x_t[2*Lp + i*N]
        y_kTs[i] = y_t[2*Lp + i*N]

    bits = 
