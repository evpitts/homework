def transmitter(bits,alpha, N, Lp, Ts, Nsym, omega_c, M, A):

    #Recieve bits
    #S/P
    #Convert to decimal (I use the decimal representation as LUT indices)
    converted_bits = np.array(convert_bits(bits, M))
    
    #Split into I and Q branches
    phi_0 = [int(x%math.sqrt(M)) for x in converted_bits]
    phi_1 = [int(x/math.sqrt(M)) for x in converted_bits]
    
    #LUT each branch
    a_0 = np.array([LUT[0][x] for x in phi_0])
    a_1 = np.array([LUT[1][x] for x in phi_1])
    
    #Upsample each branch
    i_nT= np.zeros(N*Nsym)
    i_nT[0::N] = mappedbits_0

    q_nT = np.zeros(N*Nsym)
    q_nT[0::N] = mappedbits_1

    #Convolve with pulse envelope
    p_t, t = srrc1(alpha,N,Lp,Ts)
    
    I_nT  = np.convolve(i_nT, p_t)
    Q_nT  = np.convolve(q_nT, p_t)
    
    #Multiply by the rest of the basis functions (~ orthogonal functions)
    #Sum
    ##the time variable that is used in the pulse envelope formation and the signal formation must be different, because the length of the upsampled and convolved signals are, of course, greater than the length of the pulse envelope
    s_t = I*math.sqrt(2)*np.cos(omega_c*t_trig) - Q*math.sqrt(2)*np.sin(omega_c*t_trig)

    #Transmit
    return s_t
##TODO##
#Decide what plots to make and if they go in this file
##If the plots don't go in this file, everything needed to make the plots must be returned
#Determine the correct method of create time arrays
