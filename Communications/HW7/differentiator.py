#Full-bandwidth differentiator
import numpy as np
import matplotlib.pyplot as plt

plt.close('all')
plt.rc('text', usetex=True)
plt.rc('font', family='serif')

def differentiator_filter(f_s,L):

    T = 1/f_s
    
    n = np.array([x for x in range(-L,L+1)])
    
    hd = [0]*(2*L+1)
    #WcT = np.pi  #Cutoff frequency - pi is full-bandwidth
    for idx, val in enumerate(n):
        if val == 0:
            hd[idx] = 0
        else:
            hd[idx] = 1/T*(-1.0)**val/val
            
    #np.sqrt(sum(abs(np.array(hd))**2))
    #Shifts the filter so that the time delay zero is in the first index
    #Takes the fft
    #Shifts the DC to the center
    hd = hd*np.blackman(2*L+1)
    H = np.fft.fftshift(np.fft.fft(np.fft.ifftshift(hd)))
    #Put the x-axis in frequency
    W_s = T*len(hd)     #Sampling window
    freq_space = 1/W_s  #Spacing in the frequency domain
    #label = n*freq_space
    label = np.linspace(-f_s/2,f_s/2,len(n))/f_s
    plt.figure(1)
    plt.plot(label,np.abs(H), label="Magnitude")
    plt.plot(label,np.angle(H), label="Phase")
    plt.xlabel("Frequency (cycles/sample)")
    plt.ylabel("$H_{d,FIR}(e^{j\Omega})$")
    plt.grid('on')
    plt.title("Magnitude Response of Differentiator Filter, L=7")
    plt.axvline(x=-0.3, ymin=0.35, ls='--', c='r', label='$|f| = 0.3$')
    plt.axvline(x=0.3, ymin=0.35, ls='--', c='r')
    plt.legend()
    
    plt.figure(2)
    plt.stem(n,hd)
    plt.title("Impulse Response of Differentiator Filter")
    plt.xlabel("Sample Index")

    return hd

def delay_filter(f_s,L):

    hd = np.zeros(2*L+1)
    hd[L] = 1

    temp = hd*np.blackman(2*L+1)
    H = np.fft.fftshift(np.fft.fft(np.fft.ifftshift(temp)))
    label = np.linspace(-f_s/2,f_s/2,2*L+1)/f_s
    plt.figure(3)
    plt.plot(label,np.abs(H), label="Magnitude")
    plt.plot(label,np.angle(H), label="Phase")
    plt.xlabel("Frequency (cycles/sample)")
    plt.ylabel("$H_{d,FIR}(e^{j\Omega})$")
    plt.grid('on')
    plt.title("Magnitude Response of Delay Filter")
    plt.axvline(x=-0.5, ymin=0.05, ymax=0.95)
    plt.axvline(x=0.5, ymin=0.05, ymax=0.95)
    plt.legend(loc = (0.9,-0.12))

    t_delay = np.arange(2*L+1)
    plt.figure(4)
    plt.stem(t_delay,hd)
    plt.title("Impulse Response of Delay Filter")

    return hd



f = 0.3  #half the sampling frequency - active region
f_s = 2*f
L = 7        #half number of filter taps

hd = differentiator_filter(f_s, L)
h_delay = delay_filter(f_s,L)
t = np.arange(100)*(1/f_s)
w = 2*np.pi*0.05
s = np.sin(w*t)

#Filtered signal - computed derivative
s_dot = np.convolve(hd, s)#, mode="same")

s_pad = np.pad(s, (L,L), 'constant', constant_values=(0,0))
#Calculate derivative
#ds = w*np.cos(w*t)
#ds = np.pad(w*np.cos(w*t), (L,L), 'constant', constant_values=(0,0))
ds = np.convolve(h_delay,w*np.cos(w*t))
#Plot with respect to t
t_delay = np.arange(100+2*L)*(1/f_s)
plt.figure(5)
plt.plot(t_delay, s_pad, color='orange', label="Sinusoid")
plt.plot(t_delay, ds, color='blue', label="Calculated derivative")
plt.plot(t_delay, s_dot, color='green', label="Computed derivative")
plt.legend()

delay_calc = np.pad(s, (L,L), 'constant', constant_values=(0,0))
delay_comp = np.convolve(h_delay,s)
plt.figure(6)
plt.plot(t_delay, s_pad, color='orange', label="Sinusoid")
plt.plot(t_delay, delay_calc, color='blue', label="Calculated delay")
plt.plot(t_delay, delay_comp, color='green', label="Computed delay")
plt.legend()
plt.show()


