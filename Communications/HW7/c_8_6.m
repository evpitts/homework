syms m1 m2 m3 m4
syms x1 x2 x3 x4
syms c1 c2 c3 c0
X = [x1; x2; x3; x4]
M = [m1^3 m1^2 m1 1; ...
     m2^3 m2^2 m2 1; ...
     m3^3 m3^2 m3 1; ...
     m4^3 m4^2 m4 1]
C = [c3; c2; c1; c0]

A = linsolve(M,X)