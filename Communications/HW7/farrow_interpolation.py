#Linear and cubic interpolation using the farrow filter structure
import numpy as np
import matplotlib.pyplot as plt


# def generate_data(num_samples, func):
#     """
#     num_samples: integer
#        num_samples is the number of samples that should be taken from func

#     func: array-like
#        func is the function that is being interpolated. It contains 
#     """
#     #Generate num_samples samples of func
#     x = []
#     for ind in range(num_samples)
    
#     return final_data

def linear_interpolation(x,mu):
    """
    Requires 2 samples: x_nT, x_(n-1)T
    The linear coefficients are as follows:
    | i|b1_i|b0_i|
    |-1| 1  | 0  |
    | 0| -1 | 1  |
    """
    v_0 = x[0]
    v_1 = x[1] - x[0]

    x_mk_muk = v_1*mu + v_0

    return x_mk_muk
    
def cubic_interpolation(x,mu):
    """
DOUBLE CHECK THIS DOCUMENTATION (THE FRACTIONAL IDEX FORMATTING)
    x: array-like
       contains the 4 samples required to do cubic interpolation, arranged from most recent (x((n+2)T) or x(m)) to least recent (x((n-1)T) or x(m-2))
    num_points: integer
       Number of points to be interpolated. The fractional interval mu, used the calculate the interpolated points, is num_points evenly space points between the middle points (x((n+1)T) or x(m) and )
    Requires 4 samples: x_nT, x_(n-1)T, x_(n-2)T, x_(n-3)T
    The cubic coefficients are as follows:
    | i|b3_i|b2_i|b1_i|b0_i|
    |-2| 1/6|   0|-1/6|   0|
    |-1|-1/2| 1/2|   1|   0|
    | 0| 1/2|  -1|-1/2|   1|
    | 1|-1/6| 1/2|-1/3|   0|
    """
    v_0 = x[1]
    v_1 = (-1/6)*x[3] + (1)*x[2] + (-1/2)*x[1] + (-1/3)*x[0]
    v_2 = (0)*x[3] + (1/2)*x[2] + (-1)*x[1] + (1/2)*x[0]
    v_3 = (1/6)*x[3] + (-1/2)*x[2] + (1/2)*x[1] + (-1/6)*x[0]
    
    x_mk_muk = ((v_3*mu + v_2)*mu + v_1)*mu + v_0

    return x_mk_muk

#Generate sin(2πF0t) from 0 <= t <= 3T
#F0*T = 0.15 for 11 points on the interval T <= t <= 2T
#Desired samples at times t = 0, T, 2T, 3T

plt.close('all')

F0 = 1
T = 0.15/F0
t = np.linspace(0,1/F0,1000)

samples = np.array([0, 1, 2, 3])*T

s = np.sin(2*np.pi*t)

mu_vals = np.linspace(0,1,11)
cube_x = []
line_x = []
for mu in mu_vals:
    cube_point = cubic_interpolation(np.sin(2*np.pi*F0*samples),mu)
    line_point = linear_interpolation(np.sin(2*np.pi*F0*samples[1:3]),mu)
    cube_x.append(cube_point)
    line_x.append(line_point)

plt.figure(1)
plt.plot(t,s)
plt.stem(T*mu_vals+T, cube_x, linefmt='g', markerfmt='gd')
plt.stem(samples, np.sin(2*np.pi*F0*samples))#, markerfmt='o')
plt.title("Cubic Interpolation")
plt.figure(2)
plt.plot(t,s)
plt.stem(T*mu_vals+T, line_x, linefmt='g', markerfmt='gd')
plt.stem(samples, np.sin(2*np.pi*F0*samples))#, markerfmt='o')
plt.title("Linear Interpolation")
plt.show()
