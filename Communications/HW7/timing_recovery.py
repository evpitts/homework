import numpy as np
from numpy.random import rand,seed
import matplotlib.pyplot as plt
import matplotlib
import pdb

import sys
sys.path.append('../HW2')
sys.path.append('../comms')

from comms import compute_k
from srrc1 import srrc1
from comms import diff_encode, diff_decode, compute_k, get_close_point, differentiator_filter, delay_filter, linear_interpolation, cubic_interpolation

plt.close("all")
plt.rc('text', usetex=True)
plt.rc('font', family='serif')

#Generate the signal
#Required values for pulse and constellation formation
alpha = 1       # excess bandwidth
N = 16           # samples per symbol
Lp = 60         # SRRC truncation length
Ts = 1          # symbol time in seconds
T = Ts/N        # sample time in seconds
Nsym = 1000      # number of symbols
omega_c = 2*np.pi*(T+1)  # carrier frequency - must be less than half of the sampling rate, N
M = 4           # number of constellation points - QPSK
A = 1           # amplitude
symSize = np.log2(M)  # size of each symbol
L = 11

omega_n = 0

BnT = 0.05
zeta = 1/np.sqrt(2)
k0 = -1
kp = 1
k1,k2 = compute_k(BnT,zeta,k0,kp)

# Cubic interpolation
k1 = -0.006#28886
k2 = -0.0000096289

# Linear interpolation
# k1 = 0.005
# k2 = 0.00005

k2_prev = 0

LUT = {3:(1,1),
       1:(-1,1),
       2:(1,-1),
       0:(-1,-1)}

const_list = [(-1,1), (1,-1), (-1,-1), (1,1)]

plot_num = 1    # for plotting purposes

#Create the pulse envelope
p_t,n = srrc1(alpha,N,Lp,Ts)

#Convert binary to decimal to use as indexer
converted_bits = np.array((rand(Nsym)*M).astype(int))#np.array(convert_bits(bits, M))

#Run the encoded bits through the LUT
a_0,a_1 = zip(*[LUT[x] for x in converted_bits])

#Upsample
a_0_up = np.zeros(N*len(a_0))              # this will produce a column vector of zeros
a_0_up[0::N] = a_0              # creating the upsampled signal

a_1_up = np.zeros(N*len(a_1))              # this will produce a column vector of zeros
a_1_up[0::N] = a_1              # creating the upsampled signal

#Convolve the upsampled/zero-padded bit stream with the pulse envelope
I = np.convolve(a_0_up, p_t)  # complete the multiplication of the basis function (or the filter)
Q = np.convolve(a_1_up, p_t)

t_trig = np.arange(0,len(I),1)#T*np.arange(len(I)) # Using the relationship between T, Ts, and N, the 

# #QAM has two basis functions, cos() and sin()
#Make the final trasnmitted signal by summing the basis functions convolved/multiplied by the detla trains of binary streams
s_t = I*np.sqrt(2)*np.cos(omega_c*t_trig) - Q*np.sqrt(2)*np.sin(omega_c*t_trig)


############# Reciever ##############
# #Multiply by the rest of the basis functions to get them out of the way
shift = 0
I_r = s_t*np.sqrt(2)*np.cos(omega_c*t_trig+np.deg2rad(shift))
Q_r = -s_t*np.sqrt(2)*np.sin(omega_c*t_trig+np.deg2rad(shift))

# ##### Introducing a timing offset #####
# Cubic interpolation
I_r = [cubic_interpolation(I_r[x:x+4:1],0.7) for x in range(len(I_r)-3)]
Q_r = [cubic_interpolation(Q_r[x:x+4:1],0.7) for x in range(len(Q_r)-3)]

# #Linear interpolation
# I_r = [linear_interpolation(I_r[x:x+2:1],0.5) for x in range(len(I_r)-1)]
# Q_r = [linear_interpolation(Q_r[x:x+2:1],0.5) for x in range(len(Q_r)-1)]

delay = delay_filter(L)
diffr = differentiator_filter(N,L)
hd = diffr*np.blackman(2*L+1)
H = np.fft.fftshift(np.fft.fft(np.fft.ifftshift(hd)))
label = np.linspace(-N/2,N/2,2*L+1)/N
plt.figure(plot_num)
plot_num += 1
plt.plot(label,np.abs(H), label="Magnitude")
plt.plot(label,np.angle(H), label="Phase")
plt.xlabel("Frequency (cycles/sample)")
plt.ylabel("$H_{d,FIR}(e^{j\Omega})$")
plt.grid('on')
plt.title("Magnitude Response of Differentiator Filter, L="+str(L))
plt.legend()

n = np.array([x for x in range(-L,L+1)])
plt.figure(plot_num)
plot_num += 1
plt.stem(n,hd)
plt.title("Impulse Response of Differentiator Filter")
plt.xlabel("Sample Index")

MF = np.convolve(delay,p_t)
MDF = np.convolve(diffr,p_t)

#### Delay and Derivative  Matched Filters ####
x_delay = np.convolve(I_r,MF,'valid')
y_delay = np.convolve(Q_r,MF,'valid')

x_deriv = np.convolve(I_r,MDF,'valid')
y_deriv = np.convolve(Q_r,MDF,'valid')


#### TED ####
mu = 0

# Initialization
k = 1
V21 = 0
NCO1 = 0
W1 = 1
MU1 = 0
axout = []
ayout = []
el = []
mul = []
e = 0
mu = NCO1/W1

for n in range(len(x_delay)-3): # sweep over the whole sequence (not sample-by-sample)
   # Compute NCO, strobe, and mu
   temp = NCO1 - W1
   if(temp < 0):
      #print("strobe on")
      strobe = 1
      mu = NCO1/W1
      mul.append(mu)
      nco = 1 + temp
   else:
      strobe = 0
      mu = MU1
      nco = temp
      
   if strobe == 0:
      e = 0
   else:
      # Compute interpolant xi and dxi
      xi_delay = linear_interpolation(x_delay[n:n+4:1],mu)
      yi_delay = linear_interpolation(y_delay[n:n+4:1],mu)
      
      # Get the estimated symbol
      a_0, a_1 = get_close_point(const_list, xi_delay, yi_delay)
      
      xi_deriv = linear_interpolation(x_deriv[n:n+4:1],mu)
      yi_deriv = linear_interpolation(y_deriv[n:n+4:1],mu)
      
      # Compute the error signal
      e = a_0*xi_deriv + a_1*yi_deriv
      el.append(e)
     
      # Output
      axout.append(xi_delay)
      ayout.append(yi_delay)
      k = k+1
      
   # Compute loop filter output  vt
   v1 = k1*e
   v2 = V21 + k2*e   # V21 is last value of v2
   v = v1 + v2
   w = v + 1/N
   
   # Compute states for next time.
   V21 = v2
   NCO1 = nco
   MU1 = mu
   W1 = w

plt.figure(plot_num)
plot_num += 1
plt.plot(el)
plt.title("Timing Error Signal")
plt.xlabel("$t/T_s(s)$")
plt.ylabel("Timing error e(t)")

plt.figure(plot_num)
plot_num += 1
plt.plot(mul)
plt.title("Fractional Interpolation Interval")
plt.xlabel("Symbol index")
plt.ylabel("Fractional interval $\mu$ (k)")

#Create a gradient colormap for scatter plot points
# C = np.array([[0, 0, x] if x < 256 else [0, x-255, 255-(x-255)] if x < 511 else [x-510, 255-(x-510), 0] if x < 766 else [255-(x-765), 0, x-765] for x in range(len(ayout))])
# C = C/255.0
C = [x/len(ayout) for x in range(len(ayout))]
colormap = 'cividis'
plt.figure(plot_num)
plot_num += 1
plt.scatter(ayout, axout, cmap = colormap, c = C)
cbar = plt.colorbar(matplotlib.cm.ScalarMappable(cmap=colormap),ticks=[0.0, 0.5, 1.0])
cbar.ax.set_yticklabels(['0','500','1000'])
plt.title("Interpolated MF outputs over symbol index")
plt.xlabel("Inphase location of symbol in A")
plt.ylabel("Quadrature location of symbol in A")
cbar.ax.set_ylabel("Symbol Index, n")

plt.show()
