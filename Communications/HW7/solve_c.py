import sympy as sym

# h1 = x((m(k) − 1)T)  
# h2 = x(m(k)T)        
# h3 = x((m(k) + 1)T)  
# h4 = x((m(k) + 2)T)

h1 = sym.Symbol('h1')
h2 = sym.Symbol('h2')
h3 = sym.Symbol('h3')
h4 = sym.Symbol('h4')

h = sym.Matrix([h1,h2,h3,h4])

T  = sym.Symbol('T')
mk = sym.Symbol('mk')
uk = sym.Symbol('uk')

M = sym.Matrix([[((mk - 1)*T)**3, ((mk - 1)*T)**2,  (mk - 1)*T,  1],
                [(mk*T)**3, (mk*T)**2 ,  mk*T,  1],
                [((mk + 1)*T)**3, ((mk + 1)*T)**2,  (mk + 1)*T, 1],
                [((mk + 2)*T)**3, ((mk + 2)*T)**2,  (mk + 2)*T ,1]])

MC1 = M.copy()
MC1[:,0] = h
print(MC1)
c3 = sym.expand(sym.det(MC1)/sym.det(M))
print(c3)

MC2 = M.copy()
MC2[:,1] = h
c2 = sym.expand(sym.det(MC2)/sym.det(M))

MC3 = M.copy()
MC3[:,2] = h
c1 = sym.expand(sym.det(MC3)/sym.det(M))

MC4 = M.copy()
MC4[:,3] = h
c0 = sym.expand(sym.det(MC4)/sym.det(M))


value = sym.expand(c3*((mk + uk)*T)**3 + c2*((mk + uk)*T)**2 + c1*((mk + uk)*T) + c0)
print(value)

h1_coeff = value.coeff(h1)
h2_coeff = value.coeff(h2)
h3_coeff = value.coeff(h3)
h4_coeff = value.coeff(h4)

print('x((m(k) + 2)T)  : ',h4_coeff)
print('x((m(k) + 1)T)  : ',h3_coeff)
print('x(m(k)T)        : ',h2_coeff)
print('x((m(k) − 1)T)  : ',h1_coeff)
print('\n\n')
full_value = h4_coeff*h4 + h3_coeff*h3 + h2_coeff*h2 + h1_coeff*h1
print(full_value)
