import numpy as np
import matplotlib.pyplot as plt
plt.ion()
from matplotlib.ticker import MaxNLocator


def modulate_symbol(s,LUT,Ts,N=100,b=2):
    '''Encode symbols'''
    [a0,a1] = LUT[s]
    w_0 = b*2*np.pi/Ts
    #N=number of samples per period
    t = np.linspace(0,Ts,N+1)
    #When concatenating the symbols together, the overlapping point Ts needs to not be counted twice, so we drop the last point
    t = t[:-1]

    #HS pulse shape
    p = lambda t : np.sqrt(2/Ts)*np.sin(np.pi*t/Ts)
    
    #Basis functions
    phi_0 = np.sqrt(2)*p(t)*np.cos(w_0*t)
    phi_1 = -np.sqrt(2)*p(t)*np.sin(w_0*t)

    #Linear combination of basis functions
    s_hat = a0*phi_0 + a1*phi_1

    return s_hat

def decode_symbol(s_hat,Ts,N,b=2):
    w_0 = b*2*np.pi/Ts
    dt=Ts/N
    t = np.arange(len(s_hat))*dt
    
    #HS pulse shape
    p = lambda t : abs(np.sqrt(2/Ts)*np.sin(np.pi*t/Ts))
    
    #Basis functions
    phi_0 = np.sqrt(2)*p(t)*np.cos(w_0*t)
    phi_1 = -np.sqrt(2)*p(t)*np.sin(w_0*t)

    I = phi_0*s_hat
    Q = phi_1*s_hat

    n = int(np.rint(len(s_hat)/N))
    symbols = []
    for index in range(n):
        a0 = sum(I[index*N:(index+1)*N])*dt
        a1 = sum(Q[index*N:(index+1)*N])*dt
        symbols.append((a0,a1))

    return symbols
        
LUT = {"0000" : (0, 1.5),
       "0001" : (-0.9, 0.9),
       "0011" : (0.9, 0.9),
       "0010" : (0, 0.9),
       "0110" : (-1.5, 0),
       "0100" : (-0.3, 0.3),
       "0101" : (0.3, 0.3),
       "0111" : (-0.9, 0),
       "1111" : (0, -1.5),
       "1110" : (0.9, -0.9),
       "1100" : (0, -0.9),
       "1101" : (-0.9,-0.9),
       "1001" : (1.5, 0),
       "1000" : (0.9, 0),
       "1010" : (-0.3, -0.3),
       "1011" : (0.3, -0.3)}

s = "1101001110100100"
#s = "0100"
n=4
N = 100
out = [(s[i:i+n]) for i in range(0, len(s), n)]
s_hat = np.concatenate(list(map(lambda x : modulate_symbol(x,LUT,1,N=N),out)))

t = np.arange(len(s_hat))/N


#Plotting
#plt.close("all")
fg = plt.figure(figsize=(5,3))
ax = fg.gca()
ax.plot(t,s_hat)
ax.xaxis.set_major_locator(MaxNLocator(integer=True))
plt.grid("on")
plt.title("Plot for 5.47: 16-ary QAM")
plt.xlabel("$t/T_s$")
plt.ylabel("$\hat{s}(t)$")
plt.show()
