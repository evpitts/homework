import math
import numpy as np

#M-ary PAM returns the average energy and LUT for the desired PAM constellation
def MaryConst(M=2,A=1,type="P"):
    if type == "P":
        #Average energy for M-ary PAM
        Eavg = (M**2 - 1)/3 * A**2
        #-(M-1)A, -(M-3)A, ..., -A, +A, ..., +(M-3)A, +(M-1)A
        LUT = sorted([sign*(M-x)*A for sign in [-1,1] for x in range(M) if x % 2 == 1])
        LUT = np.array(LUT)
    elif type == "Q":
        #Do average enery
        Eavg = (2*(M-1))/3 * A**2
        points_per_axis = int(math.sqrt(M))
        #print("Number of calculated points on each constellation axis:, ", points_per_axis)
        #Right now bits are assigned left to right, top to bottom, column format
        #Should rework this to use grey encoding
        LUT = [sorted([sign*(points_per_axis-x)*A for sign in [-1,1] for x in range(points_per_axis) if x % 2 == 1]) for x in range(2)]
    elif type == "B":
        #Do average energy for BPSK
        Eavg = math.sqrt(A)
        points_per_axis = int(math.log(M,2))
        #BPSK is every point arrange in a circle - APSK is concentric circles
        #Just evenly space the points around the circle
        #LUT = np.array([[math.cos(3*math.pi/M * x) for x in range(points_per_axis)],
        #                [math.sin(3*math.pi/M * x) for x in range(points_per_axis)]])
        #LUT = np.array([[-1,1],[-1,1]])
        LUT = np.array([-1, 1])
        # LUT = [[np.cos(index*2*np.pi/M + np.pi/M) for index in range(M)],
        #        [np.sin(index*2*np.pi/M + np.pi/M) for index in range(M)]]
        # LUT = LUT*sqrt(2)*A
    elif type == "8":
        Eavg = 2*A**2
        points_per_axis = M
        # LUT = [[np.cos(index*(np.pi*2/7)+(np.pi-2*np.pi/7))*A for index in range(points_per_axis)],
        #        [np.sin(index*(np.pi*2/7)+(np.pi-2*np.pi/7))*A for index in range(points_per_axis)]]
        # LUT[0][0] = 0
        # LUT[1][0] = 0
        LUT = [[0, np.cos(np.pi), np.cos(np.pi - 2*np.pi/7), np.cos(np.pi - 4*np.pi/7), np.cos(np.pi - 6*np.pi/7), np.cos(np.pi - 8*np.pi/7), np.cos(np.pi - 10*np.pi/7), np.cos(np.pi - 12*np.pi/7)],
               [0, np.sin(np.pi), np.sin(np.pi - 2*np.pi/7), np.sin(np.pi - 4*np.pi/7), np.sin(np.pi - 6*np.pi/7), np.sin(np.pi - 8*np.pi/7), np.sin(np.pi - 10*np.pi/7), np.sin(np.pi - 12*np.pi/7)]]
    return Eavg, LUT
