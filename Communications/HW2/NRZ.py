import numpy as np
import math

def NRZ(N=11,Ts=1):
    
    t = np.linspace(0,2*N-1,2*N)
    print(t)
    p_s = np.zeros(2*N)
    p_s[0:N] = math.sqrt(1/Ts)
    return p_s,t
