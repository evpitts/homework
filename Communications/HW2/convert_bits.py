import numpy as np
from math import log

#Convert a strings of bits to decimal based on the constellation type/size
def convert_bits(bits, M=2):

    num = int(log(M,2))

    converted_bits = []
    for index in range(len(bits[::num])):
        symbol = bits[index*num:index*num+num]
        # binary list to integer conversion\r\n
        result = int("".join(str(i) for i in symbol),2)
        # result
        converted_bits = converted_bits + [result]

    return converted_bits
