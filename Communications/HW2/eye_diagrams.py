import math
import numpy as np
from numpy.random import rand
import matplotlib.pyplot as plt
from srrc1 import srrc1
from MaryConst import MaryConst
from convert_bits import convert_bits
from NRZ import NRZ
from Manchester import Manchester

plt.close('all')

#Set the required variables for pulse formulation
alpha = 0.2     # excess bandwidth
N = 10          # samples per symbol
Lp = 200         # SRRC truncation length
Ts = 1          # symbol time
T = Ts/N        # sample time
Nsym = 100                                # number of symbols

plot_num = 1

#Create the pulse
#filter_shape,t = NRZ(N,Ts)
filter_shape,t = Manchester(N,Lp)
#filter_shape,t = srrc1(alpha,N,Lp,Ts)

#Plot the pulse in time
plt.figure(plot_num)
plot_num += 1
#plt.plot(t,filter_shape)
plt.step(t,filter_shape,where="post")


#Create the PAM signal
#Write a function for BPSK, and basis functions
A = 1                                     # PAM amplitude
M = 2
type = "B"
symSize = math.log(M,2)
Eavg,LUT = MaryConst(M,A,type)             # LUT and average energy
bits = (rand(int(Nsym*symSize)) > 0.5).astype(int)     # random bits

converted_bits = np.array(convert_bits(bits, M))

if type == "P":
    mappedbits = LUT[converted_bits]                    # run the bits through the LUT
elif type == "Q":
    phi_0 = [int(x%math.sqrt(M)) for x in converted_bits]
    phi_1 = [int(x/math.sqrt(M)) for x in converted_bits]
    # print("Phi_0 points: ", phi_0)
    # print("Phi_1 points: ", phi_1)
    mappedbits_0 = np.array([LUT[0][x] for x in phi_0])
    mappedbits_1 = np.array([LUT[1][x] for x in phi_1])
    mappedbits = mappedbits_0 + mappedbits_1
    plt.stem(mappedbits_0, linefmt='green', use_line_collection='True')
    plot_num += 1
elif type == "B":
    # phi_0 = [int(x%math.sqrt(M)) for x in converted_bits]
    # phi_1 = [int(x/math.sqrt(M)) for x in converted_bits]
    # # print("Phi_0 points: ", phi_0)
    # # print("Phi_1 points: ", phi_1)
    # mappedbits_0 = np.array([LUT[0][x] for x in phi_0])
    # mappedbits_1 = np.array([LUT[1][x] for x in phi_1])
    # mappedbits = mappedbits_0 + mappedbits_1
    mappedbits = LUT[converted_bits]

plt.figure(plot_num)
plot_num += 1
plt.stem(mappedbits, use_line_collection='True')
print("Mapped bits: ", mappedbits)

upsampled = np.zeros(N*Nsym)              # this will produce a column vector of zeros
upsampled[0::N] = mappedbits              # creating the upsampled signal

plt.figure(plot_num)
plot_num += 1
plt.stem(upsampled, use_line_collection='True')

s = np.convolve(upsampled.reshape((N*Nsym,)),filter_shape)        # pass the signal through the pulse filter

plt.figure(plot_num)
plot_num += 1
plt.plot(s)
#plt.step(np.linspace(0,len(s)-1,len(s)),s)

#Matched filter
x = np.convolve(s,filter_shape)
plt.figure(plot_num)
plot_num += 1
plt.plot(x)
#plt.step(np.linspace(0,len(x)-1,len(x)),x)

for i in range(int(len(bits)/symSize)):
    plt.plot(
        np.array([2*Lp + i*N,
                  2*Lp + i*N]),
        np.array([-A*M, A*M]),
        color='red',
        linewidth=0.25)
plt.show()
print('bits=',bits)

# first peak at 2*Lp, then every N samples after that
# sampling time for eye diagrams

#SRRC
#offset = (2*Lp - np.floor(N/2)).astype(int)
#Nsymtoss = 2*np.ceil(Lp/N)   # throw away symbols at the end
# nc = (np.floor((len(x) - offset - Nsymtoss*N)/N)).astype(int) # number of points of signal 
# xreshape = x[offset:offset + nc*N].reshape(nc,N)

offset = (2*N-1 - np.floor(N/2)).astype(int)
Nsymtoss = 2*np.ceil(N/N)
nc = (np.floor((len(x) - offset - Nsymtoss*(2*N))/N).astype(int))
xreshape = np.array(x[offset:offset + nc*N].reshape(nc,N))

back_end = np.roll(xreshape[:,-1],1)
front_end = np.roll(xreshape[:,0],-1)
new_reshape = np.c_[back_end, np.c_[xreshape,front_end]]
final_reshape = new_reshape[1:-1,:]

#NRZ
# plt.figure(plot_num); plt.clf()
# plot_num += 1
# plt.plot(np.arange(-np.floor(N/2)-1, np.floor(N/2)+2), final_reshape.T,color='b', linewidth=0.25)
# plt.show()

#Manchester
plt.figure(plot_num); plt.clf()
plot_num += 1
plt.plot(np.arange(-np.floor(N/2)-1, np.floor(N/2)+1), final_reshape.T,color='b', linewidth=0.25)
plt.xlim([-5,5])
plt.show()


#Phase trajectories

