import numpy as np
import math

def Manchester(N=11,Lp=60,Ts=2,type="NRZ",Nsym=100):

    t = np.linspace(0,2*N-1,2*N)
    print(t)
    p_s = np.zeros(2*N)
    p_s[0:int(N/2)] = math.sqrt(1/Ts)
    p_s[int(N/2):N] = -math.sqrt(1/Ts)
    return p_s,t
