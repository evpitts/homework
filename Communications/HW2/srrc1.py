#SRRC function
#Using the discrete-time srrc1 function

import numpy as np
import math

def srrc1(alpha=0.2,N=11,Lp=60,Ts=1):

    n = np.linspace(-Lp*N,Lp*N,(2*Lp+1)*N)
    p_s = np.zeros(len(n))

    #If the denominator equals zero, calculate the srrc using L'Hospital's rule
    for index in range(len(n)):
        denom = (np.pi*n[index])/N*(1-((4*alpha*n[index])/N)**2)
        if denom == 0:
            numerator = np.pi*(1-alpha)/N*np.cos(np.pi*(1-alpha)*n[index]/N)
            numerator -= 4*alpha*n[index]/N*np.pi*(1+alpha)/N*np.sin(np.pi*(1+alpha)*n[index]/N)
            numerator += 4*alpha/N*np.cos(np.pi*n[index]*(1+alpha)/N)
            
            p_s[index] = 1/np.sqrt(N) * numerator / (np.pi/N - 3*np.pi/N*(4*alpha*n[index]/N)**2)
            
        else:
            numerator = 1/np.sqrt(N)*(np.sin(np.pi*(1-alpha)*n[index]/N) + 4*alpha*n[index]/N*np.cos(np.pi*(1+alpha)*n[index]/N))
            p_s[index] = numerator / denom
            
    return p_s,n

def srrc1_Jason(alpha,N,LP,Ts): #square root raised cosine
    n = list(range(-LP*Ts,LP*Ts,1))
    scaler = 1/math.sqrt(N)
    srrcout = []
    for curn in n:
        if curn != 0:
            a = math.sin(math.pi*(1-alpha)*curn/N)
            b = (4*alpha*curn/N)*math.cos(math.pi*(1+alpha)*curn/N)
            c = (math.pi*curn/N)*(1-(4*alpha*curn/N)**2)

        else:  # use l'hopital's rule to define srrc at n = 0
            a  = math.pi*(1-alpha)/N
            b  = 4*alpha/N
            c  = math.pi/N
        
        srrcout.append(scaler*(a+b)/c)
    return srrcout

