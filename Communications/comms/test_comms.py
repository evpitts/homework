import unittest
import numpy as np
import comms

class TestEncodeMethods(unittest.TestCase):

    def test_bpsk(self):
        qpsk_bits = [1,3,0,2,3,0,3,3,2,1,0,3,1]
        qpsk_rule = {180 : 0b_00,
                     270 : 0b_01,
                     0   : 0b_10,
                     90  : 0b_11}
        qpsk_symbol_loc = {0b_00 : 45,
                           0b_01 : 135,
                           0b_11 : 225,
                           0b_10 : 315}
        ans = np.array(comms.diff_decode(qpsk_bits, qpsk_rule, qpsk_symbol_loc))
        ans_correct = np.array([3, 0, 1, 1, 0, 0, 2, 3, 0, 1, 0, 1])
        self.assertEqual(np.all(ans - ans_correct), 0)

    # def test_qpsk(self):
    #     self.assertTrue('FOO'.isupper())
    #     self.assertFalse('Foo'.isupper())

    # def test_rule(self):
    #     s = 'hello world'
    #     self.assertEqual(s.split(), ['hello', 'world'])
    #     # check that s.split fails when the separator is not a string
    #     with self.assertRaises(TypeError):
    #         s.split(2)
            
    # def test_symbol(self):

if __name__ == '__main__':
    unittest.main()
