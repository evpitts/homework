"""This is the comms module.

It contains functions that help build a communications system
step-by-step from start to finish. These steps include message
generation, differential encoding, modulation, sampling, PLL
implementation, DDS implementation, and others.

"""

import numpy as np
import matplotlib.pyplot as plt
from skimage.feature import match_template
import pdb

def diff_decode(bits, rule, symbol_loc):
    """Return a list of encoded symbols that will comprise the message to
    be modulated and transmitted.
    
    Parameters
    ----------
    bits : list

       Input array of integers

    rule : dictionary

       Keys must be phase differences in degrees. Values must be integers

    symbol_loc: dictionary

       Keys must be phase differences in degrees. Values must be integers
    
    >>> qpsk_bits = [1,3,0,2,3,0,3,3,2,1,0,3,1]
    >>> qpsk_rule = {180 : 0b_00,
    ...              270 : 0b_01,
    ...              0   : 0b_10,
    ...              90  : 0b_11}
    >>> qpsk_symbol_loc = {0b_00 : 45,
    ...                    0b_01 : 135,
    ...                    0b_11 : 225,
    ...                    0b_10 : 315}
    >>> diff_decode(qpsk_bits, qpsk_rule, qpsk_symbol_loc)
    [3, 0, 1, 1, 0, 0, 2, 3, 0, 1, 0, 1]

    >>> bpsk_bits = [0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 0, 1, 0]
    >>> rule = {0   : 0b_0,
    ...         180 : 0b_1}
    >>> symbol_loc = {0b_0 : 180,
    ...               0b_1 : 0}
    >>> diff_decode(bpsk_bits, rule, symbol_loc)
    [1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1]

    """
    
    message = []
    prev_angle = None
    #num_bits = int(np.log2(len(symbol_loc)))

    for symbol in bits:
        angle = symbol_loc[symbol]
        if prev_angle is not None:
            diff = (angle - prev_angle) % 360
            message_symbol = rule[diff]
            #print("angle:", angle, "diff:", diff, "message symbol:", message_symbol)
            message.append(message_symbol)
            #print("decoded bit:", format(message_symbol,'#04b'))
        prev_angle = angle

    return message

def diff_encode(bits, rule, symbol_loc):

    switch_rule = {0b_00 : 180,
                   0b_01 : 270,
                   0b_10 : 0,
                   0b_11 : 90
    }

    deg_2_loc = {45:0b_11,
                 135:0b_01,
                 225:0b_00,
                 315:0b_10}
    
    #Choose an arbitrary starting bit
    encoded_bits = [1]
    prev_angle = symbol_loc[encoded_bits[0]]

    for symbol in bits:
        shift = switch_rule[symbol]
        next_angle = (shift + prev_angle) % 360
        #print("shift:", shift, "next_angle: ", next_angle)
        message_symbol = deg_2_loc[next_angle]
        encoded_bits.append(message_symbol)
        #print("encoded bit:", format(message_symbol,'#04b')) 
        prev_angle = next_angle
    
    return encoded_bits

def compute_k(BnT,zeta,k0,kp):
    theta_n= BnT/(zeta + 1/(4*zeta))
    k1 = 4*zeta*theta_n / (1 + 2*zeta*theta_n + theta_n**2)*1/(k0*kp)
    k2 = 4*theta_n**2 / (1 + 2*zeta*theta_n + theta_n**2)*1/(k0*kp)
    return k1,k2

def get_close_point(const, point_x, point_y):
    diffs = []
    for symbol in const:
        diff_x = symbol[0] - point_x
        diff_y = symbol[1] - point_y
        diffs.append(diff_x**2 + diff_y**2)
    #pdb.set_trace()
    temp = np.argmin(diffs)
    a_h_0 = const[temp][0]
    a_h_1 = const[temp][1]
    if len(const) == 2:
        a_h_1 = 0
    return a_h_0,a_h_1

def delay_filter(L):

    hd = np.zeros(2*L+1)
    hd[L] = 1
    return hd

def differentiator_filter(f_s,L):

    T = 1/f_s
    n = np.array([x for x in range(-L,L+1)])
    hd = [0]*(2*L+1)

    for idx, val in enumerate(n):
        if val == 0:
            hd[idx] = 0
        else:
            hd[idx] = 1/T*(-1.0)**val/val
            
    hd = hd*np.blackman(2*L+1)

    return hd

def linear_interpolation(x,mu):
    """
    Requires 2 samples: x_nT, x_(n-1)T
    The linear coefficients are as follows:
    | i|b1_i|b0_i|
    |-1| 1  | 0  |
    | 0| -1 | 1  |
    """
    v_0 = x[0]
    v_1 = x[1] - x[0]

    x_mk_muk = v_1*mu + v_0

    return x_mk_muk

def cubic_interpolation(x,mu):
    """
DOUBLE CHECK THIS DOCUMENTATION (THE FRACTIONAL IDEX FORMATTING)
    x: array-like
       contains the 4 samples required to do cubic interpolation, arranged from most recent (x((n+2)T) or x(m)) to least recent (x((n-1)T) or x(m-2))
    num_points: integer
       Number of points to be interpolated. The fractional interval mu, used the calculate the interpolated points, is num_points evenly space points between the middle points (x((n+1)T) or x(m) and )
    Requires 4 samples: x_nT, x_(n-1)T, x_(n-2)T, x_(n-3)T
    The cubic coefficients are as follows:
    | i|b3_i|b2_i|b1_i|b0_i|
    |-2| 1/6|   0|-1/6|   0|
    |-1|-1/2| 1/2|   1|   0|
    | 0| 1/2|  -1|-1/2|   1|
    | 1|-1/6| 1/2|-1/3|   0|
    """
    v_0 = x[1]
    v_1 = (-1/6)*x[3] + (1)*x[2] + (-1/2)*x[1] + (-1/3)*x[0]
    v_2 = (0)*x[3] + (1/2)*x[2] + (-1)*x[1] + (1/2)*x[0]
    v_3 = (1/6)*x[3] + (-1/2)*x[2] + (1/2)*x[1] + (-1/6)*x[0]


    x_mk_muk = ((v_3*mu + v_2)*mu + v_1)*mu + v_0

    return x_mk_muk

def srrc1(alpha=0.2,N=11,Lp=60,Ts=1):

    n = np.linspace(-Lp*N,Lp*N,(2*Lp+1)*N)
    p_s = np.zeros(len(n))

    #If the denominator equals zero, calculate the srrc using L'Hospital's rule
    for index in range(len(n)):
        denom = (np.pi*n[index])/N*(1-((4*alpha*n[index])/N)**2)
        if denom == 0:
            numerator = np.pi*(1-alpha)/N*np.cos(np.pi*(1-alpha)*n[index]/N)
            numerator -= 4*alpha*n[index]/N*np.pi*(1+alpha)/N*np.sin(np.pi*(1+alpha)*n[index]/N)
            numerator += 4*alpha/N*np.cos(np.pi*n[index]*(1+alpha)/N)
            
            p_s[index] = 1/np.sqrt(N) * numerator / (np.pi/N - 3*np.pi/N*(4*alpha*n[index]/N)**2)
            
        else:
            numerator = 1/np.sqrt(N)*(np.sin(np.pi*(1-alpha)*n[index]/N) + 4*alpha*n[index]/N*np.cos(np.pi*(1+alpha)*n[index]/N))
            p_s[index] = numerator / denom
            
    return p_s,n

def uw_rotations(uwsym, num_rotations):
    """
    Returns an array of 2D arrays containing the x- and y-components of the unique word symbol locations at each desired rotation
    
    Parameters
    ----------
    uwsym : np.array

       Input array of symbol locations

    num_rotations : integer
    
       Number of rotations through which the uw should be shifted
    """
    angles = 2*np.pi*np.arange(0,num_rotations)/num_rotations
    uwrotsyms = np.zeros((len(uwsym),2,num_rotations));
    plt.figure(1)
    for i in range(angles.size):
        C = np.cos(angles[i])
        S = -np.sin(angles[i])
        G = np.array([[C, -S],[S, C]])
        uwrot = uwsym @ G;  # Rotate the UW symbols
        # Run them back through the look up table
        uwrotsyms[:,:,i] = uwrot; # Save the rotated version
        plt.scatter(uwrotsyms[:,1,i], uwrotsyms[:,0,i],c=next(colors))
    return uwrotsyms

def find_uw(uwrotsyms, output_symbols):
    """
    Returns a list of starting indices for occurances of the unique word, and the rotation to find it

    uwrotsyms : np.array

    output_symbols : list

       List of integers in which to find the unique word

    LUT : np.array

       2D array of x- and y-components of symbols locations, pairwise (i.e. LUT[0] = [x,y])
    """
    n,m = uwrotsyms.shape
    output_array = np.array([output_symbols]).T
    correlations = []
    for i in range(m):
        uw = np.reshape(uwrotsyms[:,i],(n,1))
        correlations.append(match_template(output_array,uw))
    return correlations
    
def convert_uws_l2s(uwrotlocs, LUT):
    """
    Returns an nxm array of unique word rotations in symbol numbers
    """
    num_rotations = uwrotlocs.shape[2]
    uwrotsyms = []
    for i in range(num_rotations):
        uwrotsyms.append([np.argmin(np.sum((LUT - temp)**2,axis=1)) for temp in uwrotlocs[:,:,i]])

    return np.array(uwrotsyms).T

def find_best_corr(correlations):
    """
    Of the four rotations, determined which rotation has the highest correlation with the output symbols
    
    """
    curr_len = -1
    curr_best = -1
    for idx,rotation in enumerate(correlations):
        fred=np.where(rotation > 0.75)[0]
        if len(fred) > curr_len: 
            curr_best = idx
            curr_len = len(fred)
            uw_instances = fred
    return curr_best, uw_instances

def plot_correlation(correlations,plt,plot_num):
    fig = plt.figure(plot_num)
    fig.suptitle("Correlations Of Each Rotation Of The UW With The Output Symbols")
    ax = fig.add_subplot(111, frameon=False)
    # hide tick and tick label of the big axes
    ax.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
    ax.grid(False)
    ax.set_xlabel("Symbol Index", fontsize=15)
    ax.set_ylabel("Correlation Value", fontsize=15, labelpad=20)
    ax0 = fig.add_subplot(221)
    ax0.plot(correlations[0], 'tab:blue')
    ax0.set_title("0$^{\circ}$ Phase Rotation")
    ax0.set_ylim([-1.0,1.0])
    ax1 = fig.add_subplot(222)
    ax1.plot(correlations[1], 'tab:orange')
    ax1.set_title("90$^{\circ}$ Phase Rotation")
    ax1.set_ylim([-1.0,1.0])
    ax2 = fig.add_subplot(223)
    ax2.plot(correlations[2], 'tab:green')
    ax2.set_title("180$^{\circ}$ Phase Rotation")
    ax2.set_ylim([-1.0,1.0])
    ax3 = fig.add_subplot(224)
    ax3.plot(correlations[3], 'tab:red')
    ax3.set_title("$270^{\circ}$ Phase Rotation")
    ax3.set_ylim([-1.0,1.0])    

def convert_l2s_complex(LUT, symbols_comp):
    """
    Returns the symbols as symbol numbers

    Parameters
    ----------
    symbols_comp : np.array
       Array of all symbols in complex format (a0 + ja1)
    """
    symbols_numb = np.zeros(len(symbols_comp))
    #pdb.set_trace()
    for idx in range(len(symbols_comp)):
        a0_hat, a1_hat = get_close_point(LUT, symbols_comp[idx].real, symbols_comp[idx].imag)
        symbols_numb[idx] = np.argmin(np.sum((LUT - [a0_hat,a1_hat])**2,axis=1))
    return symbols_numb

def convert_l2s(LUT_list, symbols):

    new_symbols = []
    for item in symbols:
        a0_hat = round(item[0],6)
        a1_hat = round(item[1],6)
        new_symbols.append(LUT_list.index([a0_hat,a1_hat]))
    return new_symbols

if __name__ == "__main__":
    import doctest
    doctest.testmod()
