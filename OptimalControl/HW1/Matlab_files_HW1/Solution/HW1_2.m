function HW1_2
    close all;
    %% Problem 2.1 - Linearize
    % Linearization produces following system matrices
    A = [0 0 0 1 0; 0 0 0 0 0; 0 0 0 0 1; 0 0 0 0 0; 0 0 0 0 0];
    B = [0 0; 0 0; 0 0; 1 0; 0 1];
    
    % Test rank of controllability matrix
    result_linearize_2_1 = runControllabilityTests(A, B)
    
    %% Problem 2.2 - Feedback linearized system
    % Feedback linearization results in the following system
    A = [zeros(2) eye(2); zeros(2, 4)];
    B = [zeros(2); eye(2)];
    
    % Test controllability
    result_2_2 = runControllabilityTests(A, B)
end

function plotResults(u_handle, t, x)
    % Loop through and calculate control
    u = zeros(size(t));
    for k = 1:length(u)
        u(k) = u_handle(x(:, k));
    end
    
    % Plot the states
    figure;
    subplot(1,2,1);
    plot(t, x(1, :));
    xlabel('Time(s)');
    hold on;
    plot(t, x(2, :), 'r');
    legend('x_1', 'x_2');
    
    % Plot the control
    subplot(1,2,2);
    plot(t, u);
    xlabel('Time(s)');
    ylabel('control (u)');
    
end

function result = runControllabilityTests(A, B)
    % Get eigenvalues
    lambda_vec = eig(A);
    pos_lambda = getNonNegativeEigenValues(A);
    result = '';
    
    % Get dimensions
    n = size(A, 1); % Number of states
    m = size(B, 2); % Number of inputs

    % Stabilizability
    unstabilizable_eigs = [];
    for k = 1:length(pos_lambda)
        lambda = pos_lambda(k);
        mat = [lambda .* eye(n) - A, B];
        if rank(mat) < n
            unstabilizable_eigs = [unstabilizable_eigs lambda];
        end
    end
    if isempty(unstabilizable_eigs) 
        result = [result 'Stabilizable' newline];
    else
        for k = 1:length(unstabilizable_eigs)
            result = [result 'Unstabilizable pole: ' num2str(unstabilizable_eigs(k)) newline]; 
        end
    end
    
    % Controllability Test 1
    uncontrollable_eigs = [];
    for k = 1:length(lambda_vec)
        lambda = lambda_vec(k);
        mat = [lambda .* eye(n) - A, B];
        if rank(mat) < n
            uncontrollable_eigs = [uncontrollable_eigs lambda];
        end
    end
    if isempty(uncontrollable_eigs) 
        result = [result 'Controllable (Test 1)' newline];
    else
        for k = 1:length(uncontrollable_eigs)
            result = [result 'Uncontrollable pole: ' num2str(uncontrollable_eigs(k)) newline]; 
        end
    end
    
    % Controllability Test 2
    ctrb_mat = ctrb(A, B);
    if rank(ctrb_mat) < n
       result = [result 'Uncontrollable (Test 2)'];
    else
        result = [result 'Controllable (Test 2)'];
    end    
end

function result = runObservabilityTests(A, C)
    % Get eigenvalues
    lambda_vec = eig(A);
    pos_lambda = getNonNegativeEigenValues(A);
    result = '';
    
    % Get dimensions
    n = size(A, 1); % Number of states
    p = size(C, 1); % Number of outputs

    % Detectable
    undetectable_eigs = [];
    for k = 1:length(pos_lambda)
        lambda = pos_lambda(k);
        mat = [lambda .* eye(n) - A; C];
        if rank(mat) < n
            undetectable_eigs = [undetectable_eigs lambda];
        end
    end
    if isempty(undetectable_eigs) 
        result = [result 'Detectable' newline];
    else
        for k = 1:length(undetectable_eigs)
            result = [result 'Undetectable pole: ' num2str(undetectable_eigs(k)) newline]; 
        end
    end
    
    % Detectability Test 1
    unobservable_eigs = [];
    for k = 1:length(lambda_vec)
        lambda = lambda_vec(k);
        mat = [lambda .* eye(n) - A; C];
        if rank(mat) < n
            unobservable_eigs = [unobservable_eigs lambda];
        end
    end
    if isempty(unobservable_eigs) 
        result = [result 'Observable (Test 1)' newline];
    else
        for k = 1:length(unobservable_eigs)
            result = [result 'Unobservable pole: ' num2str(unobservable_eigs(k)) newline]; 
        end
    end
    
    % Observability Test 2
    obs_mat = obsv(A, C);
    if rank(obs_mat) < n
       result = [result 'Unobservable (Test 2)'];
    else
        result = [result 'Observable (Test 2)'];
    end
end

function positive_eigenvalues = getNonNegativeEigenValues(A)
    % Get eigenvalues
    lambda_vec = eig(A);
    
    % Return positive eigenvalues
    positive_eigenvalues = lambda_vec(find(lambda_vec >= 0));    
end


