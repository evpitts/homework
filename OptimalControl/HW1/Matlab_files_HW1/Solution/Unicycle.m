classdef Unicycle < handle
    
    properties
        epsilon = 0.1
        yd
        
        % State indices
        ind_x1 = 1;
        ind_x2 = 2;
        ind_psi = 3;
        ind_v = 4;
        ind_omega = 5;
        
        % State feedback
        K % Feedback matrix
        kp = 1% Alternative proportional feedback
        kd = 1% Alternative derivative feedback
        
        % Plotting variables
        h_vehicle = [] % holds plot of the vehicle
        h_eps = [] % holds plot of the epsilon point
        r = 0.25 % size of vehicle
        c = 'b' % Color of the vehicle
        
    end
    
    methods
        function obj = Unicycle(yd)
            obj.yd = yd;
            
            % Create control
            A = [zeros(2) eye(2); zeros(2, 4)];
            B = [zeros(2); eye(2)];
            poles = [-1, -1.1, -1.2, -1.3];
            obj.K = place(A, B, poles);
        end
        
        function x_dot = dynamics(obj,t, x)
            % Calculate control inputs
            [a, alpha] = obj.calculateControl(t, x);
            
            % Extract needed states
            v = x(obj.ind_v);
            omega = x(obj.ind_omega);
            psi = x(obj.ind_psi);
            
            % Calculate dynamics
            x_dot = zeros(5,1);
            x_dot(1) = v * cos(psi);
            x_dot(2) = v * sin(psi);
            x_dot(3) = omega;
            x_dot(4) = a;
            x_dot(5) = alpha;            
        end
        
        function [a, alpha] = calculateControl(obj, t, x)
            % Calculate the control of the epsilon point
            u = obj.calculateEpsilonControl(t, x);
            
            % Get the inverse epsilon-rotation matrix
            R_eps_inv = obj.getRepsInv(x(obj.ind_psi));
            
            % Get the omega skew matrix
            omega_hat = obj.getOmegaHat(x(obj.ind_omega));
            
            % Calculate control
            v_bar = [x(obj.ind_v); x(obj.ind_omega)];
            a_bar = R_eps_inv * u - omega_hat*v_bar;
            
            % Return control
            a = a_bar(1);
            alpha = a_bar(2);
        end
        
        function u = calculateEpsilonControl(obj, t, x)
            % Get the desired reference trajectory info
            [yd, yd_dot, yd_ddot] = obj.yd(t);
            
            % Calculate the epsilon point information
            y_eps = obj.getEpsPoint(x);
            y_eps_dot = obj.getEpsPointDerivative(x);
            y = [y_eps; y_eps_dot];
            
            % Create error vector
            z = [y_eps - yd; y_eps_dot - yd_dot];
            
            % Calculate control
            u = yd_ddot - obj.K * z;
            %u = yd_ddot - obj.kp*(y_eps - yd) - obj.kd*(y_eps_dot - yd_dot);
        end
        
        function y_eps = getEpsPoint(obj, x)
            psi = x(obj.ind_psi);
            y_eps = x(1:2) + obj.epsilon * [cos(psi); sin(psi)];
        end
        
        function y_eps_dot = getEpsPointDerivative(obj, x)
            % Create velocity vector and epsilon-rotation matrix
            v_bar = [x(obj.ind_v); x(obj.ind_omega)];
            R_eps = obj.getReps(x(obj.ind_psi));
            
            % Calculate the time derivative of the epsilon point
            y_eps_dot = R_eps * v_bar;
        end
        
        function R_eps = getReps(obj, psi)
            % Calculate trig functions
            c = cos(psi);
            s = sin(psi);
            
            % Calculate epsilon rotation matrix
            R_eps = [c, -obj.epsilon*s; s, obj.epsilon*c];
        end
        
        function R_eps_inv = getRepsInv(obj, psi)
            % Calculate trig functions
            c = cos(psi);
            s = sin(psi);
            
            % Matrix 1
            m1 = [1, 0; 0, 1/obj.epsilon];
            
            % Inverse rotation element
            R_inv = [c, s; -s c];
            
            % Output inverse rotation matrix
            R_eps_inv = m1*R_inv;
        end
        
        function omega_hat = getOmegaHat(obj, omega)
            omega_hat = [0, obj.epsilon*omega; omega/obj.epsilon, 0];
        end
        
        function h = plotUnicycle(obj, x)
            % Rotation matrix (for finding points on triangle)
            psi = x(obj.ind_psi);
            q = [x(obj.ind_x1); x(obj.ind_x2)];
            R = [cos(psi) -sin(psi); sin(psi) cos(psi)];

            % Points on triangle (descriptions are for pi/2 rotation)
            p1 = R * [-obj.r; -obj.r] + q;      % right corner of triangle
            p2 = R * [0; 0] + q;       % tip of triangle
            p3 = R * [-obj.r; obj.r] + q;       % Left corner of triangle
            p4a = R * [-obj.r; 0] + q;
            p4 = (q + p4a) ./ 2;            % mid point of triangle
            P = [p1'; p2'; p3'; p4'];

            % Plot the triangle
            if isempty(obj.h_vehicle)
                % Plot the agent
                obj.h_vehicle = fill(P(:, 1), P(:, 2), obj.c);
            else
                % Plot the agent
                set(obj.h_vehicle, 'xdata', P(:, 1), 'ydata', P(:, 2));                
            end
            
            h = obj.h_vehicle;
        end
        
        function h = plotEpsilonPoint(obj, x)
            % Calculate epsilon point
            y_eps = obj.getEpsPoint(x);
            
            if isempty(obj.h_eps)
                obj.h_eps = plot(y_eps(1), y_eps(2), 'ko');
            else
                set(obj.h_eps, 'xdata', y_eps(1), 'ydata', y_eps(2));
            end
            
            h = obj.h_eps;
        end
    end
end

