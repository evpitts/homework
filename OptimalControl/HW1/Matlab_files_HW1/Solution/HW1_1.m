function HW1_1
    close all;
    
    %% Problem 1
    % Material review with the following matrices
    Aa = [1 1; 2 -1]; Ba = [0; 1];
    Ab = [1 1; 0 -1]; Bb = [1; 0];
    Ac = [1 0; 0 0]; Bc = [0; 1];

    %% Problem 1.1
    % Evaluate each system in terms of stability and controllability tests
    % given in class.  Non-negative eigenvalues:
    a_nonnegative_eig = getNonNegativeEigenValues(Aa)
    b_nonnegative_eig = getNonNegativeEigenValues(Ab)
    c_nonnegative_eig = getNonNegativeEigenValues(Ac)
    
    % Controllability tests
    a_result = runControllabilityTests(Aa, Ba)
    b_result = runControllabilityTests(Ab, Bb)
    c_result = runControllabilityTests(Ac, Bc)
    
    %% Problem 1.2
    C1 = [1 0]; C2 = [0 1]; C3 = [1 1];
    
    % Aa
    Aa_C1_result = runObservabilityTests(Aa, C1)
    Aa_C2_result = runObservabilityTests(Aa, C2)
    Aa_C3_result = runObservabilityTests(Aa, C3)
    
    % Ab
    Ab_C1_result = runObservabilityTests(Ab, C1)
    Ab_C2_result = runObservabilityTests(Ab, C2)
    Ab_C3_result = runObservabilityTests(Ab, C3)
    
    % Ac
    Ac_C1_result = runObservabilityTests(Ac, C1)
    Ac_C2_result = runObservabilityTests(Ac, C2)
    Ac_C3_result = runObservabilityTests(Ac, C3)
    
    %% Problem 1.4
    R = 1; L = 2; C = 3;
    A = [-R/L -1/L; 1/C 0]; B = [1/L; 0]; C = [0 1];
    
    %% Problem 1.4.1 Observable
    controllable_1_4 = runControllabilityTests(A, B)
    observable_1_4 = runObservabilityTests(A, C)
    
    %% Problem 1.4.2 Control
    K = place(A, B, [-2.0, -1.0]);
    
    % Define control
    uz = @(z) -K*z + 4;
    ux = @(x) uz(x - [0; 4]);
    
    % Define dynamics
    f = @(t, x) A*x + B*ux(x);
    
    % Simulate
    x0 = [0; 0];    
    
    % plot
    [tout xout] = ode45(f, [0 10], x0);
    plotResults(ux, tout, xout');
    
    %% Problem 1.4.3
    % Very large poles
    K = place(A, B, [-20, -21]);
    uz = @(z) -K*z + 4;
    ux = @(x) uz(x - [0; 4]);
    f = @(t, x) A*x + B*ux(x);
    
    % plot
    [tout xout] = ode45(f, [0 10], x0);
    plotResults(ux, tout, xout');
    
    % Very small poles
    K = place(A, B, [-.1, -.11]);
    uz = @(z) -K*z + 4;
    ux = @(x) uz(x - [0; 4]);
    f = @(t, x) A*x + B*ux(x);
    
    % plot
    [tout xout] = ode45(f, [0 10], x0);
    plotResults(ux, tout, xout'); 
end

function plotResults(u_handle, t, x)
    % Loop through and calculate control
    u = zeros(size(t));
    for k = 1:length(u)
        u(k) = u_handle(x(:, k));
    end
    
    % Plot the states
    figure;
    subplot(1,2,1);
    plot(t, x(1, :));
    xlabel('Time(s)');
    hold on;
    plot(t, x(2, :), 'r');
    legend('x_1', 'x_2');
    
    % Plot the control
    subplot(1,2,2);
    plot(t, u);
    xlabel('Time(s)');
    ylabel('control (u)');
    
end

function result = runControllabilityTests(A, B)
    % Get eigenvalues
    lambda_vec = eig(A);
    pos_lambda = getNonNegativeEigenValues(A);
    result = '';
    
    % Get dimensions
    n = size(A, 1); % Number of states
    m = size(B, 2); % Number of inputs

    % Stabilizability
    unstabilizable_eigs = [];
    for k = 1:length(pos_lambda)
        lambda = pos_lambda(k);
        mat = [lambda .* eye(n) - A, B];
        if rank(mat) < n
            unstabilizable_eigs = [unstabilizable_eigs lambda];
        end
    end
    if isempty(unstabilizable_eigs) 
        result = [result 'Stabilizable' newline];
    else
        for k = 1:length(unstabilizable_eigs)
            result = [result 'Unstabilizable pole: ' num2str(unstabilizable_eigs(k)) newline]; 
        end
    end
    
    % Controllability Test 1
    uncontrollable_eigs = [];
    for k = 1:length(lambda_vec)
        lambda = lambda_vec(k);
        mat = [lambda .* eye(n) - A, B];
        if rank(mat) < n
            uncontrollable_eigs = [uncontrollable_eigs lambda];
        end
    end
    if isempty(uncontrollable_eigs) 
        result = [result 'Controllable (Test 1)' newline];
    else
        for k = 1:length(uncontrollable_eigs)
            result = [result 'Uncontrollable pole: ' num2str(uncontrollable_eigs(k)) newline]; 
        end
    end
    
    % Controllability Test 2
    ctrb_mat = ctrb(A, B);
    if rank(ctrb_mat) < n
       result = [result 'Uncontrollable (Test 2)'];
    else
        result = [result 'Controllable (Test 2)'];
    end    
end

function result = runObservabilityTests(A, C)
    % Get eigenvalues
    lambda_vec = eig(A);
    pos_lambda = getNonNegativeEigenValues(A);
    result = '';
    
    % Get dimensions
    n = size(A, 1); % Number of states
    p = size(C, 1); % Number of outputs

    % Detectable
    undetectable_eigs = [];
    for k = 1:length(pos_lambda)
        lambda = pos_lambda(k);
        mat = [lambda .* eye(n) - A; C];
        if rank(mat) < n
            undetectable_eigs = [undetectable_eigs lambda];
        end
    end
    if isempty(undetectable_eigs) 
        result = [result 'Detectable' newline];
    else
        for k = 1:length(undetectable_eigs)
            result = [result 'Undetectable pole: ' num2str(undetectable_eigs(k)) newline]; 
        end
    end
    
    % Detectability Test 1
    unobservable_eigs = [];
    for k = 1:length(lambda_vec)
        lambda = lambda_vec(k);
        mat = [lambda .* eye(n) - A; C];
        if rank(mat) < n
            unobservable_eigs = [unobservable_eigs lambda];
        end
    end
    if isempty(unobservable_eigs) 
        result = [result 'Observable (Test 1)' newline];
    else
        for k = 1:length(unobservable_eigs)
            result = [result 'Unobservable pole: ' num2str(unobservable_eigs(k)) newline]; 
        end
    end
    
    % Observability Test 2
    obs_mat = obsv(A, C);
    if rank(obs_mat) < n
       result = [result 'Unobservable (Test 2)'];
    else
        result = [result 'Observable (Test 2)'];
    end
end

function positive_eigenvalues = getNonNegativeEigenValues(A)
    % Get eigenvalues
    lambda_vec = eig(A);
    
    % Return positive eigenvalues
    positive_eigenvalues = lambda_vec(find(lambda_vec >= 0));    
end


