classdef Segway < handle
    
    
    properties
        % State indices
        n = 7
        ind_x1 = 1;
        ind_x2 = 2;
        ind_psi = 3;
        ind_omega = 4;
        ind_v = 5;
        ind_phi = 6;
        ind_phidot = 7;
        
        % Feedback indices
        z_ind = 3:7; % Indices of z within x
        z_ind_psi = 1;
        z_ind_omega = 2;
        z_ind_v = 3;
        z_ind_phi = 4;
        z_ind_phidot = 5;
        
        % Linearized matrices (\dot{z} = Az + Bu)
        A % State matrix
        B % Input matrix
        K % Feedback matrix
        
        % Output matrix (y = [psi, v, phi])
        C = [1 0 0 0 0; 0 0 1 0 0; 0 0 0 1 0];
        
        % Control selection
        control_selection % available controllers: "stabilize_control", "velocity_control"
        velocity_control = false        
        
        % Velocity control variables
        v_d = 0
        omega_d = 0
    end
    
    methods
        function obj = Segway(poles, control_selection, varargin)
            % Store the selected control and associated variables
            obj.control_selection = control_selection;
            if strcmp(control_selection, 'velocity_control')
                obj.v_d = varargin{1};
                obj.omega_d = varargin{2};                 
            end
            
            % Initialize linearized matrices
            obj.A = AMat_zero();
            obj.B = BMat_zero();
            
            % Create feedback
            obj.K = place(obj.A, obj.B, poles);
        end
        
        function xdot = dynamics(obj,t, x, u1, u2)
            % Extract needed states
            psi = x(obj.ind_psi);
            omega = x(obj.ind_omega);
            v = x(obj.ind_v);
            phi = x(obj.ind_phi);
            phidot = x(obj.ind_phidot);
            
            % Get unicycle dynamics for updating state
            x1dot = v * cos(psi);
            x2dot = v * sin(psi);
                        
            % Calculate dynamics for \dot{omega}, \ddot{phi}, and \dot{v}
            omegadot = Omegadot(omega, phi, phidot, u2);
            phiddot = Phiddot(omega, phi, phidot, u1);
            vdot = Vdot(omega, phi, phidot, u1);
            
            % Output the aggregate dynamics
            xdot = zeros(obj.n, 1);
            xdot(obj.ind_x1) = x1dot;
            xdot(obj.ind_x2) = x2dot;
            xdot(obj.ind_psi) = omega;
            xdot(obj.ind_omega) = omegadot;
            xdot(obj.ind_v) = vdot;
            xdot(obj.ind_phi) = phidot;
            xdot(obj.ind_phidot) = phiddot;            
        end
        
        function xdot = dynamicsWithObserver(obj, t, x, observer)
            % Extract states
            x_state = x(1:obj.n);
            z_hat = x(obj.n+1:end);
            
            % Calculate control with z_hat
            z_err = obj.getZErr(z_hat);
            [u1, u2] = obj.stabilizingControl(z_err);
            
            % Calculate state dynamics
            x_dot_state = obj.dynamics(t, x_state, u1, u2);
            
            % Calculate z_hat dynamics
            y = obj.C * x_state(obj.z_ind); % i.e. C*z
            z_hat_dot = observer.dynamics(z_hat, [u1; u2], y);
            
            % Output the aggregate dynamics
            xdot = [x_dot_state; z_hat_dot];
        end
        
        function xdot = dynamicsWithoutObserver(obj, t, x)            
            % Calculate control
            z_err = obj.getZErr(x(obj.z_ind));
            [u1, u2] = obj.stabilizingControl(z_err);
            
            % Calculate system dynamics
            xdot = obj.dynamics(t, x, u1, u2);            
        end
        
        function z_err = getZErr(obj, z)
            z_err = z;
            z_err(obj.z_ind_v) = z_err(obj.z_ind_v) - obj.v_d;
            z_err(obj.z_ind_omega) = z_err(obj.z_ind_omega) - obj.omega_d;
            
            % Remove orientation error
            if strcmp(obj.control_selection, 'velocity_control')
                z_err(obj.z_ind_psi) = 0;
            end
        end
        
        function [u1, u2] = stabilizingControl(obj, z_err)            
            % Calculate control
            u = -obj.K * z_err;
            u1 = u(1);
            u2 = u(2);
        end
        
        function plotTiltAndVelocities(obj, t, x)
           % Plot phi
           subplot(1,3,1);
           plot(t, x(obj.ind_phi, :), 'linewidth', 2); hold on;
           plot([t(1), t(end)], [0 0], 'r:', 'linewidth', 2);
           xlabel('Time (s)');
           ylabel('Tilt angle (rad)');
           
           % Plot translational velocity
           subplot(1,3,2);
           plot(t, x(obj.ind_v, :), 'linewidth', 2); hold on;
           plot([t(1), t(end)], [obj.v_d obj.v_d], 'r:', 'linewidth', 2);
           xlabel('Time (s)');
           ylabel('Trans. Vel (m/s)');
           
           % Plot rotational velocity
           subplot(1,3,3);
           plot(t, x(obj.ind_omega, :), 'linewidth', 2); hold on;
           plot([t(1), t(end)], [obj.omega_d obj.omega_d], 'r:', 'linewidth', 2);
           xlabel('Time (s)');
           ylabel('Rot. Vel (rad/s)');
        end
    end
end

