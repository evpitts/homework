function A_mat = AMat_zero
%AMAT_ZERO
%    A_MAT = AMAT_ZERO

%    This function was generated by the Symbolic Math Toolbox version 8.6.
%    22-Jan-2021 13:25:33

A_mat = reshape([0.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,2.163943600450572,0.0,7.248584194881244e+1,0.0,0.0,0.0,1.0,0.0],[5,5]);
