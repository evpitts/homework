function HW1_3_ExtraCredit()
    close all;
    
    %Create a reference signal
    t = 1:.01:30;
    
    %Create a Bicycle object
    bicycle = Bicycle(@getReference);
    
    %Simulate forward in time
    %Create the initial state vector with non-zero elements
    x0 = [1; 1; 0; 0; 0];
    %Use ODE45 to simulate forward in time
    [tvec, xvec] = ode45(@(t,x)bicycle.dynamics(t,x), t, x0);
    
    %Plot
    plotSystemTrajectory(tvec', xvec', bicycle);
    simulateSystem(tvec', xvec', bicycle);
end

function [ydes, ydes_dot, ydes_ddot] = getReference(t)
    %Create y desired - make it differentiable
    ydes = [t; sin(t)];
    %Create y desired dot
    ydes_dot = [ones(1, length(t)); cos(t)];
    %Create y desired double dot
    ydes_ddot = [zeros(1, length(t)); -sin(t)];
end

function plotSystemTrajectory(t, x, bicycle)
    % Get dimensions of the state
    n = size(x, 2);
    
    % Calculate the y vector
    y_eps = zeros(2, n);
    for k = 1:n
        y_eps(:, k) = bicycle.getEpsPoint(x(:,k));
    end
    
    %% Plot reference and actual epsilon points
    [ydes, ydes_dot, ydes_ddot] = getReference(t);
    figure;
    
    % Plot y_1
    subplot(1,2,1)
    plot(t, ydes(1, :), 'r'); hold on;
    plot(t, y_eps(1,:));
    xlabel('Time (s)')
    ylabel('y_1');
    
    % Plot y_2
    subplot(1,2,2)
    plot(t, ydes(2,:), 'r'); hold on;
    plot(t, y_eps(2,:));
    xlabel('Time (s)')
    ylabel('y_2');
    legend('Desired', 'Epsilon');
    
end

function simulateSystem(t, x, bicycle)
    % Plot the reference trajectory
    figure;
    [ydes, ydes_dot, ydes_ddot] = getReference(t);
    plot(ydes(1,:), ydes(2,:), 'r', 'linewidth', 3); hold on;
    axis equal;
    pause();
    for k = 1:length(t)
        bicycle.plotUnicycle(x(:, k));
        bicycle.plotEpsilonPoint(x(:, k));
        pause(0.01);
    end    
end