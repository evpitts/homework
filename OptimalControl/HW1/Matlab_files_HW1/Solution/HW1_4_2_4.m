function HW1_4_2_4()
    clear;
    clc;
    close all;
    
    % Setup correct path
    addpath Segway
    
    % Create initial conditions
    v0 = 0.5;
    phi0 = pi/4;
    x0 = [0; 0; 0; 0; v0; phi0; 0];
    
    % Create the segway object
    poles = [-1; -1.1; -1.2; -1.3; -1.4];
    vd = 1.0;
    omegad = 0.25;
    seg = Segway(poles, 'velocity_control', vd, omegad);
    
    % Create the observer
    poles = 10 .* [poles];
    xhat = [0; 0; 0.25; pi/3; 0];
    %xhat = [0; 0; v0; phi0; 0];
    observer = SimpleObserver(poles, xhat, seg.A, seg.B, seg.C);
    
    % Simulate control
    t = 0:.01:20;
    options = odeset('RelTol',1e-3,'AbsTol',1e-4);
    [tvec, xvec] = ode45(@(t, x)seg.dynamicsWithObserver(t, x, observer), t, [x0;xhat], options);
    %[tvec, xvec] = EulerIntegration(0, .01, 20, [x0;xhat], @(t, x)seg.dynamicsWithObserver(t, x, observer));
    
    xvec = xvec'; % reshape to have each state be a column
    
    % Plot results
    figure;
    seg.plotTiltAndVelocities(tvec, xvec);
end

function [tvec, xvec] = EulerIntegration(t0, dt, tf, x0, f)
    tvec = t0:dt:tf;
    xvec = zeros(length(x0), length(tvec));
    
    % Inialize simulation variables
    t = t0;
    xvec(:, 1) = x0;
    x = x0;
    k = 1;
    
    % Loop through and simulate dynamics
    while t < tf
        % Simulate forward
        x = x + dt .* f(t, x)
        t = t + dt;
        
        % Debug
        %plotYvsX(t, x);
        
        % Update states
        k = k+1;
        xvec(:, k) = x;        
    end
    
    % Transpose xvec to get in the same shape as ode45
    xvec = xvec';
end

function plotYvsX(t, x)
    % Extract states
    x_state = x(1:7);
    z_hat = x(8:end);

    % Plot phi
    phi = x_state(6);
    phi_hat = z_hat(4);
    subplot(1,3,1)
    plot(t, phi, 'bo'); hold on;
    plot(t, phi_hat, 'ro');
    ylabel('phi');
    
    % Plot psi
    psi = x_state(3);
    psi_hat = z_hat(1);
    subplot(1,3,2)
    plot(t, psi, 'bo'); hold on;
    plot(t, psi_hat, 'ro');
    ylabel('psi');
    
    % Plot v
    v = x_state(5);
    v_hat = z_hat(3);
    subplot(1,3,3)
    plot(t, v, 'bo'); hold on;
    plot(t, v_hat, 'ro');
    ylabel('v');
    
    pause(0.0001);
end

