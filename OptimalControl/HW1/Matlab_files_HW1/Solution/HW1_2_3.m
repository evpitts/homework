function HW1_2_3()
    close all;
    
    % Plot reference signal
    t = 1:.01:30;   
    
    % Create Unicycle object
    unicycle = Unicycle(@getReference);
    
    % Simulate forward in time
    x0 = [1; 1; 0; 0; 0];
    [tvec xvec] = ode45(@(t,x)unicycle.dynamics(t,x), t, x0);
    
    % Visualize results
    plotSystemTrajectory(tvec', xvec', unicycle);
    simulateSystem(tvec', xvec', unicycle);
end

function [yd, yd_dot, yd_ddot] = getReference(t)
    yd = [t; cos(t)];
    yd_dot = [ones(1, length(t)); -sin(t)];
    yd_ddot = [zeros(1, length(t)); -cos(t)];
end

function plotSystemTrajectory(t, x, unicycle)
    % Get dimensions of the state
    n = size(x, 2);
    
    % Calculate the y vector
    y_eps = zeros(2, n);
    for k = 1:n
        y_eps(:, k) = unicycle.getEpsPoint(x(:,k));
    end
    
    %% Plot reference and actual epsilon points
    [yd, yd_dot, yd_ddot] = getReference(t);
    figure;
    
    % Plot y_1
    subplot(1,2,1)
    plot(t, yd(1, :), 'r'); hold on;
    plot(t, y_eps(1,:));
    xlabel('Time (s)')
    ylabel('y_1');
    
    % Plot y_2
    subplot(1,2,2)
    plot(t, yd(2,:), 'r'); hold on;
    plot(t, y_eps(2,:));
    xlabel('Time (s)')
    ylabel('y_2');
    legend('Desired', 'Epsilon');
    
end

function simulateSystem(t, x, unicycle)
    % Plot the reference trajectory
    figure;
    [yd, yd_dot, yd_ddot] = getReference(t);
    plot(yd(1,:), yd(2,:), 'r', 'linewidth', 3); hold on;
    axis equal;
    pause();
    for k = 1:length(t)
        unicycle.plotUnicycle(x(:, k));
        unicycle.plotEpsilonPoint(x(:, k));
        pause(0.01);
    end    
end

