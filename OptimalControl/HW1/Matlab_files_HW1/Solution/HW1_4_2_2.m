function HW1_4_2_2()
    clear;
    clc;
    close all;
    
    % Setup correct path
    addpath Segway
    
    % Create initial conditions
    v0 = 0.5;
    phi0 = pi/4;
    x0 = [0; 0; 0; 0; v0; phi0; 0];
    
    % Create the segway object
    poles = [-1; -1.1; -1.2; -1.3; -1.4];
    vd = 1.0;
    omegad = 0.25;
    seg = Segway(poles, 'velocity_control', vd, omegad);
    
    % Simulate control
    t = 0:.01:20;
    [tvec, xvec] = ode45(@(t, x)seg.dynamicsWithoutObserver(t, x), t, x0);
    xvec = xvec'; % reshape to have each state be a column
    
    % Plot results
    figure;
    seg.plotTiltAndVelocities(t, xvec);
end

