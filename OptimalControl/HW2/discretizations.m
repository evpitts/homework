close all;

%% Initialize all of the common variables
t0 = 0;     %Initial time
dt = 0.1;     %Time step
tf = 10.0;  %Final time/length of sim
num = 3;    %Problem number (1,2,3,4)

%Set the control input function
x0 = [2;2]; %Initial state vector
u = @(t,x) 0;
%% ODE45
tvec = t0:dt:tf;    %Time vector
xvec = zeros(size(x0,1), length(tvec));    %Vector to hold state values
xvec(:,1) = x0; %Initialize the first element of xvec

%Simulate forward in time using the tvec and a for loop
x = x0;
for index = 2:length(tvec)
    t = tvec(index-1);  %Get the time at k-1
    u_k_1 = u(t,x); %Get the control at k-1
    [~, xode] = ode45(@(t,x) dynamics(t,x,u_k_1,num), [t t+dt], x);    %Use ODE45
    x = xode(end,:)';   %Get the final state from ODE45
    xvec(:,index) = x;  %Add to the end of xvec
end

%Get all of the controls
uvec = zeros(2,size(tvec,2));
for k = 1:size(tvec,2)
    uvec(:,k) = u(tvec(k), xvec(:,k));
end

%Plot the results of ODE45
figure('units','normalized','outerposition',[0 0 1 1]);
plotResults(tvec, xvec, uvec, 'b');
%% Euler discretization
xvec_Euler = zeros(size(x0,1), length(tvec));
xvec_Euler(:,1) = x0;

%Simulate forward in time using the tvec and a for loop
x = x0;
for index = 2:length(tvec)
    t = tvec(index-1);  %Get the time at k-1
    x_dot = dynamics(t,x,u(t,x),num);    %Use dynamics function to find next step
    x = x + dt * x_dot;   %Get next step
    xvec_Euler(:,index) = x;  %Add to the end of xvec
end

%Get all of the controls
uvec = zeros(2,size(tvec,2));
for k = 1:size(tvec,2)
    uvec(:,k) = u(tvec(k), xvec_Euler(:,k));
end

%Plot the results of Euler discretization
plotResults(tvec, xvec_Euler, uvec, 'g');

% Plot the resulting error
plotError(tvec, xvec, xvec_Euler, 'g');
%% Exact discretization
xvec_Exact = zeros(size(x0,1), length(tvec));
xvec_Exact(:,1) = x0;

[A,B] = getSystem(num);

%Simulate forward in time using the tvec and a for loop
x = x0;
for index = 2:length(tvec)
    t = tvec(index-1);  %Get the time at k-1
    x_dot = dynamics(t,x,u(t,x),num);    %Use dynamics function to find next step
    A_d = expm(A*tvec(index-1));
    B_d = inv(A)*(A_d - eye(size(A)))*B;
    u_k_1 = u(t,x);
    if num == 4     %The shape of B_d is different in 4
        x = A_d*x + B_d*u_k_1;
    elseif num < 4
        x = A_d*x + B_d.*u_k_1;
    end
    xvec_Exact(:,index) = x;  %Add to the end of xvec
end

%Get all of the controls
uvec = zeros(2,size(tvec,2));
for k = 1:size(tvec,2)
    uvec(:,k) = u(tvec(k), xvec_Exact(:,k));
end

%Plot the results of Euler discretization
plotResults(tvec, xvec_Exact, uvec, 'r:');

% Plot the resulting error
plotError(tvec, xvec, xvec_Exact, 'r:');
%% RK4 discretization
xvec_RK4 = zeros(size(x0,1), length(tvec));
xvec_RK4(:,1) = x0;

%Simulate forward in time using the tvec and a for loop
x = x0;
[A,B] = getSystem(num);
for index = 2:length(tvec)
    t = tvec(index-1);  %Get the time at k-1
    u_k_1 = u(t,x);     %Get the input at k = 1
    %Perform RK4
    k1 = dynamics(t, x, u_k_1, num);
    k2 = dynamics(t+dt/2, x+(dt/2)*k1, u_k_1, num);
    k3 = dynamics(t+dt/2, x+(dt/2)*k2, u_k_1, num);
    k4 = dynamics(t+dt, x+dt*k3, u_k_1, num);
    x = x + (k1 + 2*k2 + 2*k3 + k4)/6;
    xvec_RK4(:,index) = x;  %Add to the end of xvec
end

%Get all of the controls
uvec = zeros(2,size(tvec,2));
for k = 1:size(tvec,2)
    uvec(:,k) = u(tvec(k), xvec_RK4(:,k));
end

%Plot the results of Euler discretization
plotResults(tvec, xvec_RK4, uvec, 'm--');

% Plot the resulting error
plotError(tvec, xvec, xvec_RK4, 'm--');
%% Dynamics
%Dynamics for all four systems
function xdot = dynamics(~, x, u, num)
    %Get state matrices
    [A,B] = getSystem(num);
    xdot = A*x+B.*u;
end

function [A,B] = getSystem(number)
    %Four different problems
    if number == 1
        A = [0 -2; 2 0];
        B = [0; 1];
    elseif number == 2
        A = [-1 0; 0 -1];
        B = [0; 1];
    elseif number == 3
        A = [0 -2; 1 0];
        B = [1; 0];
    elseif number == 4
        A = [-100 0; 0 -100];
        B = 0;
    end
end
%% Plotting
function plotResults(tvec,xvec,uvec,color)
    % Plot variables
    fontsize = 18;
    linewidth = 2;
    % Plot the resulting states
    subplot(2,2,1); hold on;
    plot(tvec, xvec(1,:), color, 'linewidth', linewidth);
    ylabel('q_1(t)', 'fontsize', fontsize);
    subplot(2,2,3); hold on;
    plot(tvec, xvec(2,:), color, 'linewidth', linewidth);
    ylabel('q_2(t)', 'fontsize', fontsize);
end

function plotError(tvec, xvec_truth, xvec, color)
    % Plot variables
    fontsize = 18;
    linewidth = 2;
    % Calculate the normalized difference
    x_err = abs(xvec_truth - xvec);
    % Plot the resulting states
    subplot(2,2,2); hold on;
    plot(tvec, x_err(1,:), color, 'linewidth', linewidth);
    ylabel('q_1(t)', 'fontsize', fontsize);
    subplot(2,2,4); hold on;
    plot(tvec, x_err(2,:), color, 'linewidth', linewidth);
    ylabel('q_2(t)', 'fontsize', fontsize);
end