function S01_L03_Unicycle_Discretization
close all;

    %% Initialize the simulation variables
    % Timing variables
    t0 = 0;     % Initial time
    dt = 1;   % Step size
    tf = 10.0;  % Final time
    number = 5; % Problem number
    
    % Initial state
    if number > 4
        x0 = [0;0;0];
        u = @(t, x, number) [1; 0.25];
    elseif number <= 4
        x0 = [0;0];
        u = @(t, x, number) [1; 0.25];
    end
    % Set the control input function
    
    %u = @(t, x) [sin(t); cos(t)];
    %u = @(t, x) [t^2*sin(10*t); 1+3*cos(4*t)];
    
    %% Simulate the system using ode45
    % Simulate the system
    [tvec, xvec_true] = matlabOde45Discrete(x0, t0, dt, tf, u, @unicycleDynamics, number);
    uvec = getControlVector(tvec, xvec_true, u);
    
    % Plot the resulting states
    figure('units','normalized','outerposition',[0 0 1 1]);
    plotResults(tvec, xvec_true, uvec, 'b', number);
    
    %% Simulate the system using an Euler discretization
    % Simulate the system
    [tvec, xvec_euler] = eulerIntegration(x0, t0, dt, tf, u, @unicycleDynamics, number);
    uvec = getControlVector(tvec, xvec_euler, u);
    
    % Plot the resulting states
    plotResults(tvec, xvec_euler, uvec, 'ro', number);
    
    % Plot the resulting error
    plotError(tvec, xvec_true, xvec_euler, 'r', number);
    
    %% Simulate the system using an Rk4 discretization
    % Simulate the system
    [tvec, xvec_rk4] = rk4Integration(x0, t0, dt, tf, u, @unicycleDynamics, number);
    uvec = getControlVector(tvec, xvec_rk4, u);
    
    % Plot the resulting states
    plotResults(tvec, xvec_rk4, uvec, 'go', number);
    
    % Plot the resulting error
    plotError(tvec, xvec_true, xvec_rk4, 'g', number);
    
    %% Simulate the system using an exact discretization
    % Simulate the system
    [tvec, xvec_exact] = exactIntegration(x0, t0, dt, tf, u, @discretizedUnicycleDynamics, number);
    uvec = getControlVector(tvec, xvec_exact, u);
    
    % Plot the resulting states
    plotResults(tvec, xvec_exact, uvec, 'ko', number);
    
    % Plot the resulting error
    plotError(tvec, xvec_true, xvec_exact, 'k', number);
        
    
end

function xdot = unicycleDynamics(~, x, u, number)
% unicycleDynamics Calculates the dynamics of the unicycle robot given the
% inputs
%
% Inputs:
%   t: Current time (not used)
%   x: Unicycle configuration ( (x,y) position and orientation)
%   u: Input to the unicycle
%       u(1): Translational velocity input
%       u(2): Rotational velocity input
%
% Outputs:
%   xdot: dynamics at time t
    g = 1;
    l = 2;
    b = 0.1;
    m = 1.5;
    if number == 1
        A = [0 -2; 2 0];
        B = [0; 1];
        xdot = A*x + B.*u;
    elseif number == 2
        A = [-1 0; 0 -1];
        B = [0; 1];
        xdot = A*x + B.*u;
    elseif number == 3
        A = [0 -2; 1 0];
        B = [1; 0];
        xdot = A*x + B.*u;
    elseif number == 4
        A = [-100 0; 0 -100];
        B = 0;
        xdot = A*x + B*u;
    elseif number == 5
        % Extract orientation
        th = x(3);

        % Extract inputs
        v = u(1);
        w = u(2);

        % Calculate the dynamics
        xdot = [ v * cos(th); ...
                 v * sin(th); ...
                 w];
    elseif number == 6
        xdot = [ x(2);
                 g/l*sin(x(1)) - b/(m*l^2)*x(2) + 1/(m*l^2)*u];
    end 
end

function xdot = discretizedUnicycleDynamics(~, x, u, dt, number)
% unicycleDynamics Calculates the dynamics of the unicycle robot given the
% inputs
%
% Inputs:
%   t: Current time (not used)
%   x: Unicycle configuration ( (x,y) position and orientation)
%   u: Input to the unicycle
%       u(1): Translational velocity input
%       u(2): Rotational velocity input
%
% Outputs:
%   xdot: dynamics at time t

    if number == 1
        A = [0 -2; 2 0];
        B = [0; 1];
        xdot = A*x + B.*u;
    elseif number == 2
        A = [-1 0; 0 -1];
        B = [0; 1];
        xdot = A*x + B.*u;
    elseif number == 3
        A = [0 -2; 1 0];
        B = [1; 0];
        xdot = A*x + B.*u;
    elseif number == 4
        A = [-100 0; 0 -100];
        B = 0;
        xdot = A*x + B*u;
    elseif number == 5
        % Extract orientation
        th = x(3);

        % Extract inputs
        v = u(1);
        w = u(2);
        
        if(w == 0)
            xdot = [v*dt*cos(th);
                    v*dt*sin(th);
                    w*dt];
        else
            xdot = [v/w*(sin(w*dt+th) - sin(th));
                    v/w*(cos(th) - cos(w*dt+th));
                    w*dt];
        end
    elseif number == 6
        % Extract states
        
        x2 = x(1);
    end
end

function [tvec, xvec] = matlabOde45Discrete(x0, t0, dt, tf, u, f, number)
    %MatlabOde45 uses ODE 45 to simulate the state starting at x0 from time
    % t0 to tf assuming discrete steps in the input (i.e., the input is
    % only updated every dt seconds)
    %
    % Inputs:
    %   x0: nx1 initial state
    %   t0: scalar - initial time
    %   dt: scalar - time increment
    %   tf: scalar - final time
    %   u: function handle that takes time and state as inputs and outputs
    %   the control input
    %   f: function handle that take in time, state, and input
    %
    % Outputs:
    %   tvec: 1xm vector of times associated with the states
    %   xvec: nxm matrix of states where each column is a state at the
    %   associated time in tvec
    
    % Initialize values
    tvec = t0:dt:tf;
    len = length(tvec);
    xvec = zeros(size(x0,1), len);
    xvec(:,1) = x0;
    
    % Simulate forward in time
    x = x0;
    for k = 2:len
        % Calculate the control input
        t = tvec(k-1);  % Time at step k-1
        u_km1 = u(t,x); % Input at step k-1
        
        % Simulate forward in time
        [~, xmat] = ode45(@(t,x) f(t, x, u_km1, number), [t t+dt], x);
        x = xmat(end,:)'; % Extract the final state from xmat
        xvec(:,k) = x;
    end
end

function [tvec, xvec] = eulerIntegration(x0, t0, dt, tf, u, f, number)
    %eulerIntegration uses eulerIntegration to simulate the state starting at x0 from time
    % t0 to tf
    %
    % Inputs:
    %   x0: nx1 initial state
    %   t0: scalar - initial time
    %   dt: scalar - time increment
    %   tf: scalar - final time
    %   u: function handle that takes time and state as inputs and outputs
    %   the control input
    %   f: function handle that take in time, state, and input
    %
    % Outputs:
    %   tvec: 1xm vector of times associated with the states
    %   xvec: nxm matrix of states where each column is a state at the
    %   associated time in tvec
    
    % Initialize values
    tvec = t0:dt:tf;
    len = length(tvec);
    xvec = zeros(size(x0,1), len);
    xvec(:,1) = x0;
    
    % Simulate forward in time
    x = x0;
    for k = 2:len
        % Calculate state dynamics
        t = tvec(k-1);
        xdot = f(t, x, u(t,x), number);
        
        % Simulate forward in time
        x = x + dt * xdot;
        xvec(:,k) = x;
    end
end

function [tvec, xvec] = rk4Integration(x0, t0, dt, tf, u, f, number)
    %rk4Integration uses a four step runge-kutta discretization of the system
    %
    % Inputs:
    %   x0: nx1 initial state
    %   t0: scalar - initial time
    %   dt: scalar - time increment
    %   tf: scalar - final time
    %   u: function handle that takes time and state as inputs and outputs
    %   the control input
    %   f: function handle that take in time, state, and input
    %
    % Outputs:
    %   tvec: 1xm vector of times associated with the states
    %   xvec: nxm matrix of states where each column is a state at the
    %   associated time in tvec
    
    % Initialize values
    tvec = t0:dt:tf;
    len = length(tvec);
    xvec = zeros(size(x0,1), len);
    xvec(:,1) = x0;
    
    % Simulate forward in time
    x = x0;
    for k = 2:len
        % Calculate state dynamics
        t = tvec(k-1);
        u_km1 = u(t,x); % Input at step k-1
        k1 = f(t, x, u_km1, number);
        k2 = f(t+dt/2, x+(dt/2)*k1, u_km1, number);
        k3 = f(t+dt/2, x+(dt/2)*k2, u_km1, number);% Use k3
        k4 = f(t+dt, x+dt*k2, u_km1, number);% Use definition of k4
        % Simulate forward in time
        x = x + (k1 + 2*k2 + 2*k3 + k4)/6; % Use definition of state transition
        xvec(:,k) = x;
    end
end

function [tvec, xvec] = exactIntegration(x0, t0, dt, tf, u, f, number)
    %exactIntegration uses an exact discretization of the system
    %
    % Inputs:
    %   x0: nx1 initial state
    %   t0: scalar - initial time
    %   dt: scalar - time increment
    %   tf: scalar - final time
    %   u: function handle that takes time and state as inputs and outputs
    %   the control input
    %   f: function handle that take in time, state, and input
    %
    % Outputs:
    %   tvec: 1xm vector of times associated with the states
    %   xvec: nxm matrix of states where each column is a state at the
    %   associated time in tvec
        % Initialize values
    tvec = t0:dt:tf;
    len = length(tvec);
    xvec = zeros(size(x0,1), len);
    xvec(:,1) = x0;
    
    % Simulate forward in time
    x = x0;
    for k = 2:len
        % Calculate state dynamics
        t = tvec(k-1);
        % Simulate forward in time
        xdot = f(t, x, u(t,x), dt, number);
        x = x+xdot;
        xvec(:,k) = x;
    end
end

function u_vec = getControlVector(tvec, xvec, u)
%getControlVector calculate the control vector over the specified time
%interval and state
%
% Inputs:
%   tvec: 1xm vector of time inputs
%   xvec: nxm matrix of states
%   u: function handle that takes time and state as inputs and outputs
%   the control input

    len = size(tvec, 2);
    u_vec = zeros(2, len);
    for k = 1:len
        u_vec(:,k) = u(tvec(k), xvec(:,k));
    end

end

function plotResults(tvec, xvec, uvec, color, number)

    % Plot variations
    if number <= 4
        plotnum = 2;
    else
        plotnum = 3;
    end
    
    % Plot variables
    fontsize = 18;
    linewidth = 2;
    
    % Plot the resulting states
    subplot(plotnum,2,1); hold on;
    plot(tvec, xvec(1,:), color, 'linewidth', linewidth);
    ylabel('q_1(t)', 'fontsize', fontsize);
    
    subplot(plotnum,2,3); hold on;
    plot(tvec, xvec(2,:), color, 'linewidth', linewidth);
    ylabel('q_2(t)', 'fontsize', fontsize);
    
    if plotnum == 3
        subplot(plotnum,2,5); hold on;
        plot(tvec, xvec(3,:), color, 'linewidth', linewidth);
        ylabel('q_3(t)', 'fontsize', fontsize);
    end
    
%     subplot(5,1,4); hold on;
%     plot(tvec, uvec(1,:), color, 'linewidth', linewidth);
%     ylabel('v(t)', 'fontsize', fontsize);
%     xlabel('time (s)', 'fontsize', fontsize);
%     
%     subplot(5, 1, 5); hold on;
%     plot(tvec, uvec(2,:), color, 'linewidth', linewidth);
%     ylabel('\omega(t)', 'fontsize', fontsize);
%     xlabel('time (s)', 'fontsize', fontsize);
end

function plotError(tvec, xvec_truth, xvec, color, number)
    
    % Plot variations
    if number <= 4
        plotnum = 2;
    else
        plotnum = 3;
    end
    
    % Plot variables
    fontsize = 18;
    linewidth = 2;
    
    % Calculate the normalized difference
    x_err = abs(xvec_truth - xvec);
    
    % Plot the resulting states
    subplot(plotnum,2,2); hold on;
    plot(tvec, x_err(1,:), color, 'linewidth', linewidth);
    ylabel('q_1(t)', 'fontsize', fontsize);
    
    subplot(plotnum,2,4); hold on;
    plot(tvec, x_err(2,:), color, 'linewidth', linewidth);
    ylabel('q_2(t)', 'fontsize', fontsize);
    
    if plotnum == 3
        subplot(plotnum,2,6); hold on;
        plot(tvec, x_err(3,:), color, 'linewidth', linewidth);
        ylabel('q_3(t)', 'fontsize', fontsize);
    end

end