function LinearDiscretization_solution
close all;

    %% Create the system matrices
%     % Part A
%     A = [0 -2; 2 0];
%     B = [0; 1];
    
    % Part B
    A = [-1 0; 0 -1];
    B = [0; 1];
    
%     % Part C
%     A = [0 -2; 1 0];
%     B = [1; 0];
    
%     % Part D
%     A = [-100 0; 0 -100];
%     B = [0;0];
    
    %% Create the discretization
    % Timing variables
    dt = 0.1; % Discretization time step
    t0 = 0; % Initial time
    tf = 10; % Final time
    
    % Euler discretization method
    [A_euler, B_euler] = eulerDiscretization(A, B, dt)
    
    % Exact discretization method
    [A_exact, B_exact] = calculateDiscreteTimeMatrices(A, B, dt)
    
    %% Simulate the system using ode45
    % Initialize the state and control
    x0 = [2; 2];
    u = @(t, x) 0;
    
    % Simulate the system
    [tvec, xvec_true] = matlabOde45Discrete(x0, t0, dt, tf, u, A, B);    
    
    % Plot the resulting states
    figure('units','normalized','outerposition',[0 0 1 1]);
    plotResults(tvec, xvec_true, 'b');
    
    %% Simulate the system using an Euler discretization
    % Simulate the system
    [tvec, xvec_euler] = discreteLinearIntegration(x0, t0, dt, tf, u, A_euler, B_euler);
        
    % Plot the resulting states
    plotResults(tvec, xvec_euler, 'ro');
    
    % Plot the resulting error
    plotError(tvec, xvec_true, xvec_euler, 'r');    
        
    %% Simulate the system using an exact discretization
    % Simulate the system
    [tvec, xvec_exact] = discreteLinearIntegration(x0, t0, dt, tf, u, A_exact, B_exact);
        
    % Plot the resulting states
    plotResults(tvec, xvec_exact, 'ko');
    
    % Plot the resulting error
    plotError(tvec, xvec_true, xvec_exact, 'k');
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%  Discretization functions   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [Abar, Bbar] = eulerDiscretization(A, B, dt)
% eulerDiscretization takes a continuous-time system defined by (A,B) as 
%       \dot{x} = Ax + Bu
% and produced the discrete time matrices
%       x_{k+1} = Abar x_k + Bbar u_k
% using an Euler discretization technique

    % Get the state size
    n = size(A, 1);
    
    % Calculate the discrete time state matrices
    Abar = eye(n) + dt.*A; 
    Bbar = dt.*B;
end

function [Abar, Bbar] = calculateDiscreteTimeMatrices(A, B, dt)
% calculateDiscreteTimeMatrices takes a continuous-time system defined by (A,B) as 
%       \dot{x} = Ax + Bu
% and produced the discrete time matrices
%       x_{k+1} = Abar x_k + Bbar u_k
% using exact discretization

    % Calculate the discrete time state matrix
    Abar = expm(A*dt);
    
    % Calculate the discrete time input matrix
    n = size(A, 1); % Number of states
    b0 = zeros(n*n, 1);
    [t, Bbar_mat] = ode45(@(t, x) bBarDynamics(t, x), [0, dt], b0);
    Bbar_mat = Bbar_mat';
    Bbar = reshape(Bbar_mat(:, end), n, n)*B;
    
    % Dynamics function for calculating Bbar
    function b_dot = bBarDynamics(tau, ~)
        b_dot = expm(A*(dt-tau)); % Calculate the time derivative at tau
        b_dot = reshape(b_dot, n*n, 1) % Reshape to a column vector
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%  Integration functions   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [tvec, xvec] = matlabOde45Discrete(x0, t0, dt, tf, u, A, B)
    %MatlabOde45 uses ODE 45 to simulate the state starting at x0 from time
    % t0 to tf assuming discrete steps in the input (i.e., the input is
    % only updated every dt seconds)
    %
    % Inputs:
    %   x0: nx1 initial state
    %   t0: scalar - initial time
    %   dt: scalar - time increment
    %   tf: scalar - final time
    %   u: function handle that takes time and state as inputs and outputs
    %   the control input
    %   (A,B): Continuous-time dynamic updates
    %
    % Outputs:
    %   tvec: 1xm vector of times associated with the states
    %   xvec: nxm matrix of states where each column is a state at the
    %   associated time in tvec
    
    % Initialize values
    tvec = t0:dt:tf;
    len = length(tvec);
    xvec = zeros(size(x0,1), len);
    xvec(:,1) = x0;
    
    % Simulate forward in time
    x = x0;
    for k = 2:len
        % Calculate the control input
        t = tvec(k-1);  % Time at step k-1
        u_km1 = u(t,x); % Input at step k-1
        
        % Simulate forward in time
        [~, xmat] = ode45(@(t,x) dynamicsLinear(t, x, u_km1, A, B), [t t+dt], x);
        x = xmat(end,:)'; % Extract the final state from xmat
        xvec(:,k) = x;
    end
    
    
end

function xdot = dynamicsLinear(~, x, u, A, B)
    %dynamicsLinear calculate the dynamics of a linear, time-invariant system
    % xdot = Ax + Bu
    %
    % Inputs:
    %   t: time value (not used)
    %   x: current state
    %   u: current input
    %   A: State matrix
    %   B: Input matrix
    xdot = A*x + B*u;
end

function [tvec, xvec] = discreteLinearIntegration(x0, t0, dt, tf, u, Abar, Bbar)
    %discreteLinearIntegration uses discrete time integration to integrate
    % the state starting at x0 from time t0 to tf
    %
    % Inputs:
    %   x0: nx1 initial state
    %   t0: scalar - initial time
    %   dt: scalar - time increment
    %   tf: scalar - final time
    %   u: function handle that takes time and state as inputs and outputs
    %   the control input
    %   (Abar, Bbar): Discrete-time equations (x_{k+1} = Abar x_k + Bbar u_k)
    %
    % Outputs:
    %   tvec: 1xm vector of times associated with the states
    %   xvec: nxm matrix of states where each column is a state at the
    %   associated time in tvec
    
    % Initialize values
    tvec = t0:dt:tf;
    len = length(tvec);
    xvec = zeros(size(x0,1), len);
    xvec(:,1) = x0;
    
    % Simulate forward in time
    x = x0;
    for k = 2:len
        % Calculate state dynamics
        t = tvec(k-1);
        x = Abar*x + Bbar*u(t,x);
        
        % Store x at the new iteration
        xvec(:,k) = x;
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%  Plotting functions   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function plotResults(tvec, xvec, color)

    % Plot variables
    fontsize = 18;
    linewidth = 2;
    
    % Plot the resulting states
    subplot(2,2,1); hold on;
    plot(tvec, xvec(1,:), color, 'linewidth', linewidth);
    ylabel('x_1(t)', 'fontsize', fontsize);
    
    subplot(2,2,3); hold on;
    plot(tvec, xvec(2,:), color, 'linewidth', linewidth);
    ylabel('x_2(t)', 'fontsize', fontsize);
end

function plotError(tvec, xvec_truth, xvec, color)
    % Plot variables
    fontsize = 18;
    linewidth = 2;
    
    % Calculate the normalized difference
    x_err = abs(xvec_truth - xvec);
    
    % Plot the resulting states
    subplot(2,2,2); hold on;
    plot(tvec, x_err(1,:), color, 'linewidth', linewidth);
    ylabel('e_1(t)', 'fontsize', fontsize);
    
    subplot(2,2,4); hold on;
    plot(tvec, x_err(2,:), color, 'linewidth', linewidth);
    ylabel('e_2(t)', 'fontsize', fontsize);
end
