classdef Branin < CostClass
    
    properties
       a = 5.1/(4*pi^2)
       b = 5/pi
       c = -6
       d = 10*(1-1/(8*pi))
       e = 10
    end
        
    methods
        function obj = Branin(x0)
            % Define limits
            x1 = [-5 15];
            x2 = [-5 15];
            z = [0 100];
            
            % Create the class
            obj = obj@CostClass(x1, x2, z,x0);
        end
        
        function val = cost(obj,x)
            x1 = x(1);
            x2 = x(2);
            val = (x2-obj.a.*x1.^2+obj.b*x1+obj.c).^2+obj.d.*cos(x1)+obj.e;
        end
        
        function dc_dx = partial(obj, x)
            x1 = x(1);
            x2 = x(2);
            v = (x2-obj.a.*x1.^2+obj.b*x1+obj.c);
            dc_dx = 2*v.*[-2*obj.a*x1+obj.b, 1] + [-obj.d*sin(x1), 0];
        end
        
        function step = optimal_step(obj, x)
            step = [0;0];
        end
    end
end
