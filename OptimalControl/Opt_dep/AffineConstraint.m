classdef AffineConstraint < ConstraintClass
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        % coefficients for cost
        a % scales x1
        b % scales x2
        c % scalar offset
    end
    
    methods
        function obj = AffineConstraint(x1lim, x2lim, is_equality, a, b, c)
            % Create the constraint class
            obj = obj@ConstraintClass(x1lim, x2lim, is_equality);
            
            % Store constraint variables
            obj.a = a;
            obj.b = b;
            obj.c = c;
        end
        
        function h = constraint(obj, x)
            h = obj.a * x(1) + obj.b*x(2) + obj.c;
        end
        
        function dh_dx = partial(obj, x)
            dh_dx = [obj.a obj.b];
        end
        
        function x1 = solve(obj, x)
            x1 = -(obj.b*x(2) + obj.c) / obj.a;
        end
    end
end

