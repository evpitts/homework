classdef Affine3DConstraint < ConstraintClass
    
    properties
        % coefficients for cost
        a % scales x1
        b % scales x2
        c % scales x3
        d % scalar offset
        
        % matrix for scaling x
        A 
    end
    
    methods
        function obj = Affine3DConstraint(x1lim, x2lim, a, b, c, d)
            % Create the constraint class
            obj = obj@ConstraintClass(x1lim, x2lim, true);
            
            % Store constraint variables
            obj.a = a;
            obj.b = b;
            obj.c = c;
            obj.d = d;
            
            % Store matrix for easy calculation of constraint
            obj.A = [obj.a, obj.b, obj.c];
        end
        
        function h = constraint(obj, x)
            h = obj.A + obj.d;
        end
        
        function dh_dx = partial(obj, x)
            dh_dx = [obj.a obj.b obj.c];
        end
        
        function x1_2 = solve(obj, x)
            % Given x3 solve for x1 and x2
            x1_2 = [obj.a obj.b]\(-obj.d - obj.c.*x(3));
        end
    end
end

