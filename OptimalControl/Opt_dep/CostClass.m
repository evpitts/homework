classdef (Abstract) CostClass < handle
    
    % Plotting properties
    properties
        x1lim % Limits for plotting along x1 axis
        x2lim % Limits for plotting along x2 axis
        zlim % Limits for plotting vertical axis
        dx = 0.1 
        n % number of optimization variables
        
        % Plotting handles
        cost_vs_iter_handle = [];  % Handle to cost vs iteration plot
        surf_contour_fig = [];  % Handle to the figure for surf and contour plots
    end
    
    % Gradient descent variables
    properties
        x0 % Initial state
        eps = 0.1; % Stopping criteria
        iter_num = 1 % stores the number of times the gradient has been called
        
        % Stepsizes
        alpha_const = .011; % Stepsize for constant steps
        alpha_dim = 0.5; % Stepsize for diminishing steps
        
        % Armijo variables
        alpha = 0.5;
        beta = 0.5;
        k = 1;
    end
    
    % Cost function methods
    methods(Abstract)
        c = cost(obj, x)
        dc_dx = partial(obj, x)
        step = optimal_step(obj, x)
    end
    
    methods
        function obj = CostClass(x1lim, x2lim, zlim, x0)
            % Read in properties
            obj.x1lim = x1lim;
            obj.x2lim = x2lim;
            obj.zlim = zlim;
            obj.x0 = x0;
            obj.n = length(x0);            
        end
        
        %%%%%%%%%  Evaluation functions %%%%%%%%%%%%
        function numericallyTestLocalSolution(obj, x, dx, max_dev)
        % This function will numerically test points around a solution
        % deviating around x by max_dev with a resolution of dx
        
            % Check for two dimensions
            if length(x) ~= 2
                return;
            end
            
            % Get data points to test 
            x1_vec = [x(1)-max_dev:dx:x(1)+max_dev];
            x2_vec = [x(2)-max_dev:dx:x(2)+max_dev];
            [X1, X2] = meshgrid(x1_vec, x2_vec);
            
            % Initialize minimum value as the input value
            x_min = x;  % Initialize the smallest value for x as the input
            c_min = obj.cost(x); % Initialize the smallest cost in terms of the input
            disp(['Input x = ', obj.formatArray(x), ' With cost = ', num2str(c_min)]);
            
            % Search for a smaller costing x
            found_smaller = false;
            for m = 1:size(X1, 1)
                for n = 1:size(X1, 1)
                    % Create the vector to evaluate
                    x_eval = [X1(m,n); X2(m,n)];
                    
                    % Check to see if the value satisifies the constraints
                    if ~obj.satisfiesConstraints(x_eval)
                        continue;
                    end
                    
                    % Evaluate the cost
                    c_eval = obj.cost(x_eval);
                    
                    % Compare the cost to the smallest value seen
                    if c_eval < c_min
                        % Store smaller values
                        found_smaller = true;
                        x_min = x_eval;
                        c_min = c_eval;
                    end
                end
            end
            
            % Output results
            if found_smaller
                disp(['Found a smaller result: ', obj.formatArray(x_min), ' With cost = ', num2str(c_min)]);
            else
                disp('No smaller cost found');
            end
        end
        
        function result = satisfiesConstraints(obj, x)
            % Returns true if the input satisfies constraints
            result = true; 
        end
        
        function text = formatArray(obj, x)
            text = ['[', num2str(x(1)), ', ' num2str(x(2)), ']'];
        end
        
        function [c, dc_dx] = objectiveAndGrad(obj, x)
            c = obj.cost(x);
            dc_dx = obj.partial(x);
        end
        
        %%%%%%%%%  Plotting functions %%%%%%%%%%%%
        function plotCost(obj, varargin)
            if obj.n ~= 2 % Only plot for 2D optimization
                return;
            end
            
            % Calculate values to be evaluated
            x1_vec = [obj.x1lim(1):obj.dx:obj.x1lim(2)];
            x2_vec = [obj.x2lim(1):obj.dx:obj.x2lim(2)];
            [X1, X2] = meshgrid(x1_vec, x2_vec);
            
            % Get cost function to be used
            if nargin < 2
                costfunction = @(x)obj.cost(x);
            else
                costfunction = @(x)varargin{1}(x);
            end
            
            % Loop through and evaluate each value
            Z = zeros(size(X1));
            for m = 1:size(X1,1)
                for n = 1:size(X1,2)
                    x = [X1(m,n); X2(m,n)];
                    Z(m,n) = min(obj.zlim(2), costfunction(x));
                end
            end
            
            % Plot the surf and contour plots
            if isempty(obj.surf_contour_fig)
                obj.surf_contour_fig = figure;
            end
            subplot(1,2,1);
            surf(X1,X2,Z)
            zlim(obj.zlim);            
            colormap(pink);
            shading interp
            
            subplot(1,2,2);
            contour(X1,X2, Z, 60);
            hold on;
            axis equal
        end  
        
        function plotState(obj, x, varargin)
            if obj.n ~= 2 % Only plot for 2D optimization
                return;
            end
            
            if nargin > 2
                c = [varargin{1} 'o'];
            else
                c = 'ko';                
            end
            
            set(0, 'currentfigure', obj.surf_contour_fig);
            subplot(1,2,2);
            plot(x(1), x(2), c, 'linewidth', 3); 
            xlim(obj.x1lim);
            ylim(obj.x2lim);
            axis equal
        end
        
        function plotCostVsIteration(obj, x)
            % Create the figure
            if isempty(obj.cost_vs_iter_handle)
                figure;
                
                % get data to plot
                i = 0;  % iteration
                c = obj.cost(x); % cost
                
                % plot data
                obj.cost_vs_iter_handle = plot(i, c, 'ro', 'linewidth', 3);
                xlabel('Iteration', 'fontsize', 18);
                ylabel('Cost', 'fontsize', 18);
            else
                % Extract data from plot
                iterations = get(obj.cost_vs_iter_handle, 'xdata');
                costs = get(obj.cost_vs_iter_handle, 'ydata');
                
                % Create new data
                i = iterations(end) + 1;
                c = obj.cost(x);
                
                % Set new data
                set(obj.cost_vs_iter_handle, 'xdata', [iterations i], 'ydata', [costs c]);                
            end
            
        end
        
        %%%%%%%%%  Gradient Step Calculations %%%%%%%%%%%%
        function bool = stop(obj, x)
            dc_dx = obj.partial(x);
            val = norm(dc_dx)
            if val < obj.eps
                bool = true;
            else
                bool = false;
            end
        end
        
        function step = constant(obj, x)
            dc_dx = obj.partial(x);
            step = -obj.alpha_const .* dc_dx';
        end
        
        function step = diminish(obj, x)
            dc_dx = obj.partial(x);
            step = -obj.alpha_dim / sqrt(obj.iter_num) .* dc_dx';
            obj.iter_num = obj.iter_num + 1;
        end
        
        function step = armijo_step(obj, x)
            dc_dx = obj.partial(x);
            
            % Perform an armijo search
            obj.k = obj.k  ;
            obj.armijo_search(x, dc_dx);
            
            % Output step
            step = -obj.beta^obj.k * dc_dx';
        end
        
        function armijo_search(obj, x, dc_dx)
            % Initialize variables
            dc_squared = dc_dx * dc_dx';
            c = obj.cost(x); % cost at x
            
            cond1 = false;
            cond2 = false;
            
            % Perform search            
            while ~cond1 || ~cond2
                xk = x - obj.beta^obj.k * dc_dx';       % x_k
                xk1 = x - obj.beta^(obj.k-1) * dc_dx';  % x_{k-1}
                ck = obj.cost(xk);                      % c(x_k)
                ck1 = obj.cost(xk1);                    % c(x_k-1)
                
                % Check condition 1
                if ck - c <= -obj.beta^obj.k * obj.alpha * dc_squared
                    cond1 = true;
                else
                    cond1 = false;
                end
                
                % Check condition 2
                if ck1 - c > -obj.beta^(obj.k-1) * obj.alpha * dc_squared
                    cond2 = true;
                else
                    cond2 = false;
                end
                
                % Update k
                if cond1 && ~cond2
                    obj.k = obj.k-1;
                elseif cond2 && ~cond1
                    obj.k = obj.k+1;                    
                end                
            end
        end
    end
end

