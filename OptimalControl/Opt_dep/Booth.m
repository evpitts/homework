classdef Booth < CostClass
        
    methods
        function obj = Booth(x0)
            % Define limits
            x1 = [-10 10];
            x2 = [-10 10];
            z = [0 3000];
            
            % Create the class
            obj = obj@CostClass(x1, x2, z,x0);
        end
        
        function val = cost(obj,x)
            x1 = x(1);
            x2 = x(2);
            val = (x1+2*x2-7)^2+(2*x1+x2-5)^2;
        end
        
        function dc_dx = partial(obj, x)
            x1 = x(1);
            x2 = x(2);
            dc_dx = [10*x1 - 34 + 8*x2, 10*x2 - 38 + 8*x1];
        end
        
        function step = optimal_step(obj, x)
            step = [0;0];
        end
    end
end
