classdef Quadratic < CostClass
        
    properties
        % Cost variables
        Q = [8 3; 3 5];
        b = [1; -3];
    end
    
    methods
        function obj = Quadratic(x0)
            % Define limits
            x1 = [-10 10];
            x2 = [-10 10];
            z = [-10 100];
            
            % Create the class
            obj = obj@CostClass(x1, x2, z, x0);
        end
        
        function val = cost(obj,x)
            val = 0.5* x'*obj.Q*x + obj.b'*x;
        end
        
        function dc_dx = partial(obj, x)
           dc_dx = 0.5*x' * (obj.Q + obj.Q') + obj.b';
        end
        
        function step = optimal_step(obj, x)
            dc_dx = obj.partial(x);
            
            % Calculate step size
            k2 = -(x'*obj.Q*dc_dx' + obj.b'*dc_dx');
            k1 = 0.5*dc_dx*obj.Q*dc_dx';
            a =  -k2/(2*k1); 
            
            % Calculate step
            step = -a * dc_dx';
        end
        
        function min = calculateMin(obj)
            M = 0.5* (obj.Q + obj.Q');
            min = -inv(M) * obj.b;
        end
    end
end

