classdef EllipseConstraint < ConstraintClass
    % Implements the constraint: x1^1/a^2 + x2^2/b^2 - 1 = 0
    
    properties
        % coefficients for cost
        a % scales x1^2
        b % scales x2^2   
        c % constant
    end
    
    methods
        function obj = EllipseConstraint(x1lim, x2lim, is_equality, a, b, c)
            % Create the constraint class
            obj = obj@ConstraintClass(x1lim, x2lim, is_equality);
            
            % Store constraint variables (force required positive and
            % negative values
            obj.a = abs(a);     % a must be positive
            obj.b = abs(b);     % b must be positive
            obj.c = abs(c);
        end
        
        function h = constraint(obj, x)
            h = x(1)^2 / obj.a^2 + x(2)^2/obj.b^2 - obj.c;
        end
        
        function dh_dx = partial(obj, x)
            dh_dx = [2*x(1)/obj.a^2, 2*x(2)/obj.b^2];
        end
        
        function x = solve(obj, x)
            % Note that the ellipse constraint puts an inherent upper
            % and lower bound on x2, with extremes at |x2| < b*sqrt(c)
            
            % Adjust x2
            extreme = obj.b*sqrt(obj.c);
            if abs(x(2)) > extreme 
                % Calculate angle of vector
                t = atan2(x(2), x(1));
                
                % Calculate position on ellipse
                x(1) = obj.a * cos(t);
                x(2) = obj.b * sin(t);
                return;
            end
            
            % Calculate the two solutions for x1
            x1_1 = sqrt(-obj.a^2*(x(2)^2/obj.b^2 - obj.c));
            x1_2 = -x1_1;
            
            % Aggregate the two solutions
            x_1 = [x1_1; x(2)];
            x_2 = [x1_2; x(2)];
            
            % Calculate the distance from the input solution
            e_1 = norm(x - x_1);
            e_2 = norm(x - x_2);
            
            % Choose the solutions that is closest to the original
            if e_1 < e_2
                x(1) = x1_1;
            else
                x(1) = x1_2;
            end            
        end
        
        
    end
end

