classdef Matyas < CostClass
    
    properties
       a = 5.1/(4*pi^2)
       b = 5/pi
       c = -6
       d = 10*(1-1/(8*pi))
       e = 10
    end
        
    methods
        function obj = Matyas(x0)
            % Define limits
            x1 = [-10 10];
            x2 = [-10 10];
            z = [0 100];
            
            % Create the class
            obj = obj@CostClass(x1, x2, z,x0);
        end
        
        function val = cost(obj,x)
            x1 = x(1);
            x2 = x(2);
            val = 0.26*(x1^2+x2^2)-0.48*x1*x2;
        end
        
        function dc_dx = partial(obj, x)
            x1 = x(1);
            x2 = x(2);
            dc_dx = [0.52*x1 - 0.48*x2, 0.52*x2 - 0.48*x1];
        end
        
        function step = optimal_step(obj, x)
            step = [0;0];
        end
    end
end
