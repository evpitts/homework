classdef ConstrainedCostClass < CostClass
    properties
        f_vec % vector of references to constraint classes
        cost_ref % reference to a cost class  
        lam % lagrange multipliers
        p % number of constraints
        
        % Separated constraints
        h_vec % vector of references to the equality constraints
        g_vec % vector of references to the inequality constraints
        n_eq % Number of equality constraints
        n_ineq % Number of inequality constraints 
        
        % Step size
        alpha_cons = 0.1;
    end
    
    methods
        function obj = ConstrainedCostClass(cost, f_vec)
            obj = obj@CostClass(cost.x1lim, cost.x2lim, cost.zlim, cost.x0);
            
            obj.f_vec = f_vec;
            obj.cost_ref = cost;
            obj.p = length(f_vec);
            obj.lam = zeros(obj.p, 1); 
            
            % Separate out the equality and inequality constraints
            obj.n_eq = 0; % Number of equality constraints
            obj.n_ineq = 0; % Number of inequality constraints
            for k = 1:length(obj.f_vec)
                if obj.f_vec(k).is_equality % Add the constraint to the equality list
                    obj.n_eq = obj.n_eq + 1;
                    obj.h_vec{obj.n_eq} = obj.f_vec(k);
                else % Add the constraint to the inquality list
                    obj.n_ineq = obj.n_ineq + 1;
                    obj.g_vec{obj.n_ineq} = obj.f_vec(k);
                end                
            end
        end
        
        function c = cost(obj, x)
            % This function uses the augmented penalty function to
            % calculate the cost in order to be able to use the
            % unconstrained gradient methods
            
            % Initialize the cost to the actual cost
            c = obj.actualCost(x);
            
            % Add the penalty terms for each constraint
            for i = 1:obj.p
                c = c + obj.f_vec(i).penaltyValue(x);
            end            
        end
        
        function [g, h, dg, dh] = constraintsAndPartials(obj, x)
            % Extract inequality constraint values
            [g, dg] = obj.constraintsAndPartialsFromConstraint(x, obj.g_vec, obj.n_ineq);
            dg = dg'; % fmincon wants column vectors for partials
            
            % Extract equality constraint values
            [h, dh] = obj.constraintsAndPartialsFromConstraint(x, obj.h_vec, obj.n_eq);            
            dh = dh'; % fmincon wants column vectors for partials
        end
        
        function [f, df] = constraintsAndPartialsFromConstraint(obj, x, f_obj, n_con)
            % Initialize the outputs
            if n_con == 0
                f = [];
                df = [];
            else
                f = zeros(n_con, 1);
                df = zeros(n_con, obj.cost_ref.n);
            end
            
            % Populate the outputs
            for k = 1:n_con
                [f(k), df(k, :)] = f_obj{k}.constraintAndGrad(x);
            end
        end
        
        function dLbar_dx = partial(obj, x)
            % This function uses the augmented penalty function to
            % calculate the gradient in order to be able to use the
            % unconstrained gradient methods
            
            % Inilize the gradient to the unconstrained gradient
            dLbar_dx = obj.cost_ref.partial(x);
            
            % Add each of the penalty contributions
            for i = 1:obj.p
                dLbar_dx = dLbar_dx + obj.f_vec(i).penaltyGradient(x);
            end
        end
        
        function c = actualCost(obj, x)
            c = obj.cost_ref.cost(x);
        end
        
        function dH_dx = lagrangianPartial(obj, x)
            % Calculate partial of cost
            dL_dx = obj.cost_ref.partial(x);
            
            % Calculate partial of constraints
            dh_dx = constraintPartial(obj, x);
            
            % Calcate partial of augmented function
            dH_dx = dL_dx + obj.lam' * dh_dx;
        end
        
        function dh_dx = constraintPartial(obj, x)
            % Calculate partial of constraints
            dh_dx = zeros(obj.p, obj.n);
            for i = 1:obj.p
                dh_dx(i, :) = obj.f_vec(i).partial(x);
            end
        end
        
        function plotCost(obj)
            plotCost@CostClass(obj, @(x)obj.actualCost(x));
            
            % Plot constraints
            if obj.n ~= 2 % Only plot for 2D optimization
                return;
            end
            subplot(1,2,2); hold on;
            for i = 1:obj.p
               obj.f_vec(i).plotConstraint();
            end
        end
        
        %%%%%%%%%%%% Gradient Step Calculations %%%%%%%%%%%%
        function step = optimal_step(obj, x)
            step = 0.0;
        end
       
        function step = constrainedStep(obj, x)
            % Return zero if over constrained
            % Overconstrained if there is more than one constraint
            %
            
            % Solve for constrained solution using input
            x = obj.adjustToConstraint(x);
            
            % Calculate partials
            dL_dx = obj.cost_ref.partial(x);
            dh_dx = obj.constraintPartial(x);
            
            % Extract state (z) and input (u)
            dL_dz = dL_dx(1:obj.p);
            dL_du = dL_dx(obj.p+1:obj.n);
            dh_dz = dh_dx(:, 1:obj.p);
            dh_du = dh_dx(:, obj.p+1:obj.n);
            
            % Calculate lambda
            if rank(dh_dz) ~= obj.p % we have found a singularity point(a correct solution would
                                    % be to reduce the number ofconstraints and reduce the
                                    % dimensionality of \lambda
                obj.lam = zeros(obj.p, 1);
            else
                obj.lam = (-dL_dz*(dh_dz^-1))';
            end
            
            % Calculate dH/du
            dH_du = dL_du + obj.lam'*dh_du;
            
            % Calculate step
            step = [zeros(obj.p,1); -obj.alpha_cons * dH_du'];            
        end
        
        function bool = stopConstrained(obj, x)
            dH_dx = obj.lagrangianPartial(x);
            val = norm(dH_dx);
            if val < obj.eps
                bool = true;
            else
                bool = false;
            end
        end
        
        function x = adjustToConstraint(obj, x)
            % This function solves for z \in \R^p given u \in \R^{n-p}
            %
            % Note: this function is simplified right now to solve for
            % only p = 1, n = 2 (i.e. a single constraint)
            
            if obj.p > 1
                return;
            end
            
            z = obj.f_vec(1).solve(x);
            u = x(obj.p+1:end);
            x = [z; u];
        end
        
        function result = satisfiesConstraints(obj, x)
            result = true; % Assume constraints satisified unless proven otherwise
            for k = 1:obj.p
                % Evaluate the kth constraint
                h = obj.f_vec(k).constraint(x);
                
                % If equality constraint, check to see that h = 0
                if obj.f_vec(k).is_equality()
                    if abs(h) > .00001
                        result = false;                       
                    end
                    
                % If inequality constraint, check to see that h < 0
                else
                    if h > .00001
                        result = false;
                    end
                end
            end
        end
        
        function plotCostVsIteration(obj, x)
            % Create the figure
            if isempty(obj.cost_vs_iter_handle)
                figure;
                
                % get data to plot
                i = 0;  % iteration
                c = obj.actualCost(x); % cost
                
                % plot data
                obj.cost_vs_iter_handle = plot(i, c, 'ro', 'linewidth', 3);
                xlabel('Iteration', 'fontsize', 18);
                ylabel('Cost', 'fontsize', 18);
            else
                % Extract data from plot
                iterations = get(obj.cost_vs_iter_handle, 'xdata');
                costs = get(obj.cost_vs_iter_handle, 'ydata');
                
                % Create new data
                i = iterations(end) + 1;
                c = obj.actualCost(x);
                
                % Set new data
                set(obj.cost_vs_iter_handle, 'xdata', [iterations i], 'ydata', [costs c]);                
            end
            
        end
    end
end

