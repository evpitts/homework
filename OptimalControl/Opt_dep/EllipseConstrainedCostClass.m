classdef EllipseConstrainedCostClass < ConstrainedCostClass
    properties
        h1 % An instance of the Ellipse class
    end
    
    methods
        function obj = EllipseConstrainedCostClass(cost, h1)
            obj = obj@ConstrainedCostClass(cost, h1);
            obj.h1 = h1;
        end
        
        function x = adjustToConstraint(obj, x)
            % This function solves for z \in \R^p given u \in \R^{n-p}
            %
            % Note: this function is simplified right now to solve for
            % only p = 1, n = 2 (i.e. a single constraint)
            
            x = obj.h1.solve(x);                        
        end
        
    end
end

