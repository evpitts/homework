% Timing variables
close all
t0 = 0;     % Initial time
dt = 0.1;   % Step size
tf = 5;  % Final time

x = linspace(t0,tf);
t = t0:dt:2*pi;
g = exp(t);

m = (tf-t0)/dt;
%What should this actually be??
n = 4;

%Initialize the variables
Q = zeros(n);
b = zeros(n,1);
c = 0;
h = 0;

%Probably need to reorder this
for jindex = 1:n
    for index = 1:n
        for kindex = 1:m
            Q(index,jindex) = x(kindex)^(index+jindex);
            b(jindex) = exp(x(kindex))*x(kindex)^jindex;
            c = c + exp(x(kindex))^2;
        end
    end
end
u = inv((Q + Q')')*2*b;
disp(Q)
disp(b)
disp(c)
for index = 1:n
    h = h + u(index)*x.^index;
end

figure;
plot(x,h,'ro');
hold on;
plot(t,g,'b');