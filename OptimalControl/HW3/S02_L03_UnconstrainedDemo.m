function S02_L03_UnconstrainedDemo
    close all;
    clc;
    
    % Add dependent folders
    addpath ../Opt_dep
    
    %% Create the cost
    %cost = Quadratic([10; 10]);
    %cost = Branin([0; 5]);
    cost = Booth([0; 5]);
    %cost = Matyas([0;5]);
    
    %% Visualize the cost
    cost.plotCost();
    
    %% Select the gradient descent method
    %step = @(x) cost.constant(x);
    %step = @(x) cost.diminish(x);
    %step = @(x) cost.optimal_step(x);
    step = @(x) cost.armijo_step(x);
    
    %% Perform gradient descent
    x = cost.x0;
    cost.plotState(x);
    pause();
    
    i = 1
    while ~cost.stop(x)
       x = x + step(x)
       i = i+1
       
       cost.plotState(x);
       pause(0.1);
    end    
    end_cost = cost.cost(x)
    
%     %% Solve using Optimization library
%     x0 = cost.x0; % Starting guess
%     options = optimoptions(@fmincon, 'Algorithm', 'sqp');
%     options = optimoptions(options, 'SpecifyObjectiveGradient', true);
%     options.Display = 'iter';
%     lb = []; ub = []; % No upper or lower bounds
%     
%     tic
%     % Matlab call          FUN                  X0, A,  B,  Aeq, Beq, lb, ub 
%     [x, cost_grad] = fmincon(@cost.objectiveAndGrad, x0, [], [], [],  [],  lb, ub, @nonlcon, options);
%     time_grad = toc
%     cost.plotState(x);
%     disp(['cost_grad = ' num2str(cost_grad)]);
%     
%     % Without gradient
%     options = optimoptions(options, 'SpecifyObjectiveGradient', false);
%         
%     tic
%     % Matlab call                     FUN               X0, A,  B,  Aeq, Beq, lb, ub 
%     [x, cost_no_grad] = fmincon(@cost.objectiveAndGrad, x0, [], [], [],  [],  lb, ub, @nonlcon, options);
%     time_no_grad = toc
%     cost.plotState(x);
%     disp(['cost_no_grad = ' num2str(cost_no_grad)]);   
    
end

function [c, ceq] = nonlcon(x)
    c = 0;
    ceq = 0;
end