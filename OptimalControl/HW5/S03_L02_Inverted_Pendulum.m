function S03_L02_Inverted_Pendulum
close all;
    %% Problem setup
    % Initialize problem parameters
    tf = 3.0; % Final time
    P.dt = 0.01; % Time step
    P.N = round(tf/P.dt); % Number of steps
    P.n_x = 2; % Number of states
    P.n_u = 1; % Number of control inputs
    P.n_ctrl = P.n_u*P.N; % Number of control inputs combined
    P.n_state = P.n_x*P.N; % Number of states combined
    P.x0 = [pi/3; 0]; % Initial state
    P.k_u = .01; % Penalty on control
    P.S = diag([100, 100]); % Penalty on terminal state
    P.x_d = [0; 0];
    
    % Initialize physical parameters
    P.g = 9.8; % Gravity constant
    P.l = 0.25; % Pendulum length
    P.m = 1/9.8; % Pendulum mass
    P.b = 1.0; % Friction coefficient
    P.umax = 1.0; % Maximum input
    
    % Initialize variables for optimization
    u0 = 0.01.*ones(P.n_ctrl, 1); % Note that a zero-input control corresponds 
                                  % to a local minimum that the optimization has a 
                                  % hard time moving out of
    
    % Create the initial state
    x0 = eulerSim(u0, P); % Simulate forward in time using the control laws
    
    % Optimize
    tic
    [x, u] = simultaneousOptimization(x0, u0, P); % Multiple shooting
    %[x, u] = sequentialOptimization(u0, P); % Single shooting
    time_opt = toc
    
    %% Plot the result
    % Reformat the state and control to have one column per time index
    x_mat = [P.x0 reshape(x, P.n_x, P.N)]; % Add in the initial state as well
    u_mat = reshape(u, P.n_u, P.N);
    
    % Plot the states
    figure;
    names = {'$\theta$', '$\dot{\theta}$'};
    for k = 1:P.n_x
        subplot(3, 1, k);
        plot([1 P.N], [P.x_d(k) P.x_d(k)], 'r:', 'linewidth', 3); hold on;
        plot(x_mat(k,:), 'b', 'linewidth', 2); 
        xlim([1 P.N+1]); % Ensures the plot has the same x-axis as the others
        ylabel(names{k},'interpreter','latex');
    end
    
    % Plot the control inputs
    names = {'Torque'};
    for k = 1:P.n_u
        subplot(3, 1, k+P.n_x);
        plot(u_mat(k,:), 'b', 'linewidth', 2);
        xlim([1 P.N+1]); % Ensures the plot has the same x-axis as the others
        ylabel(names{k},'interpreter','latex');
    end
end

%%%%  Optimization functions %%%%
function [x, u] = simultaneousOptimization(x0, u0, P)
% simultaneousOptimization simultaneously optimizes over both the state and
% the control, using an equality constraint to relate the two
%
% Inputs:
%   x0: Initial guess for the states (x_1 to x_N in a single column vector)
%   u0: Initial guess for the control (u_0 to u_N-1 in a single column vector)
%   P: Struct of parameters
%
% Outputs:
%   x: Optimized state (x_1 to x_N in a single column vector)
%   u: Optimized control (u_0 to u_N-1 in a single column vector)

    %% Initialize parameters
    % Initialize partials for constraint
    P.dh_dx = eye(P.n_state); % Initializes the partial of the constraint wrt the state for easy access
    P.dh_du = zeros(P.n_state, P.n_ctrl); % Initializes the partial of the constraint wrt the control
    
    % Create the aggregate state and initial cost
    y0 = [x0; u0]; % Combine the initial state and control into one vector
    c_init = costCombined(y0, P) % Display the initial cost
%     [~, h_init] = nonlinearConstraints(y0, P); % Can be used to see if the
                                                % constraint is satisfied (sanity check on model)
    
    % Initialize the solver variables
    options = optimoptions(@fmincon, 'Algorithm', 'sqp'); % choose sequential-quadratic-programming
    %options = optimoptions(@fmincon, 'Algorithm', 'interior-point'); % choose an the interior-point algorithm
    options = optimoptions(options, 'SpecifyObjectiveGradient', true, 'SpecifyConstraintGradient',true); % Indicate whether to use gradients
    options = optimoptions(options, 'OptimalityTolerance', 0.1); % Tolerance for optimization
    options.Display = 'iter'; % Have Matlab display the optimization information with each iteration
    
    % Define the linear inequality constraints (empty matrices because we
    % do not have any)
    A = [];
    B = [];
    
    % Define the linear equality constraints (empty matrices because we do
    % not have any)
    Aeq = []; % No equality constraints
    Beq = [];
    
    % Define the upper and lower bounds on the input
    lb_state = -inf.*ones(P.n_state, 1);  % Bounds on state
    ub_state = inf.*ones(P.n_state, 1);
    lb_ctrl = -P.umax.*ones(P.n_ctrl, 1); % Bounds on control
    ub_ctrl = P.umax.*ones(P.n_ctrl, 1);
    lb = [lb_state; lb_ctrl]; % Bounds on y 
    ub = [ub_state; ub_ctrl]; 
    
%     % Add a terminal constraint
%     lb(P.n_state-P.n_x+1:P.n_state) = P.x_d - .1;
%     ub(P.n_state-P.n_x+1:P.n_state) = P.x_d + .1;
    
    % Matlab call:
    optimize = true; % If optimize is true, then it will optimize. Otherwise it will load in the data
    [y, final_cost] = fmincon(@(y) costCombined(y, P), y0, A, B, Aeq, Beq, lb, ub, @(y) nonlinearConstraints(y, P), options);
    disp(['Final cost = ' num2str(final_cost)]);
    
    % Extract the state and control from the variable y
    [x, u] = extractStateAndControl(y, P);
end

function [x, u] = sequentialOptimization(u0, P)
% sequentialOptimization optimizes over the control. At each iteration the
% control is used to calculate the state which are both used to calculate
% the cost
%
% Inputs:
%   u0: Initial guess for the control (u_0 to u_N-1 in a single column vector)
%   P: Struct of parameters
%
% Outputs:
%   x: Optimized state (x_1 to x_N in a single column vector)
%   u: Optimized control (u_0 to u_N-1 in a single column vector)
    
    % Output the initial cost
    c_init = costSequential(u0, P)
    
    % Initialize the solve variables
    %options = optimoptions(@fmincon, 'Algorithm', 'sqp'); % choose sequential-quadratic-programming
    options = optimoptions(@fmincon, 'Algorithm', 'interior-point'); % choose an the interior-point algorithm
    options = optimoptions(options, 'SpecifyObjectiveGradient', true); % Indicate whether to use gradients (Note that there are no constraint gradients)
    options = optimoptions(options, 'OptimalityTolerance', 0.1); % Tolerance for optimization
    options.Display = 'iter'; % Have Matlab display the optimization information with each iteration
    
    % Define the linear inequality constraints (empty matrices because we
    % do not have any)
    A = [];
    B = [];
    
    % Define the linear equality constraints (empty matrices because we do
    % not have any)
    Aeq = []; % No equality constraints
    Beq = [];
    
    % Define the upper and lower bounds lb_ctrl = -P.umax.*ones(P.n_ctrl, 1); % Bounds on control
    lb = -P.umax.*ones(P.n_ctrl, 1);
    ub = P.umax.*ones(P.n_ctrl, 1);
    
    % Matlab call:
    optimize = true; % If optimize is true, then it will optimize. Otherwise it will load in the data
    if optimize
        [u, final_cost] = fmincon(@(u_in) costSequential(u_in, P), u0, A, B, Aeq, Beq, lb, ub, [], options);
        save('Data\s03_l02_sequential_data.mat');
    else
        data = load('Data\s03_l02_sequential_data.mat');
        u = data.u;
        final_cost = data.final_cost;
    end
    disp(['Final cost = ' num2str(final_cost)]);
    
    % Simulate the state forward in time to be able to output the result
    x = eulerSim(u, P);    
end

%%%%  Simulation functions %%%%%%
function x = eulerSim(u, P)
% eulerSim calculates the state given the control inputs using Euler
% Integration
%
% Inputs:
%   u: Control inputs (u_0 to u_N-1 in a single column vector)
%   P: Struct of parameters
%
% Outputs:
%   x: Calculated state (x_1 to x_N in a single column vector)

    % Initialize
    x_k = P.x0; % Set x_k to the initial state
    x = zeros(P.n_state, 1); % Initialize the output states
    
    % Calculate the simulation forward in time
    ind_state = 1:2; % Indices for the state
    ind_ctrl = 1; % Indices for the control
    for k = 1:P.N
        % Calculate the update given x_k
        f_k = rk4PendulumSim(x_k, u(ind_ctrl), P);
        x(ind_state) = f_k;
        
        % Update the indices
        ind_state = ind_state + P.n_x;
        ind_ctrl = ind_ctrl + P.n_u;
        x_k = f_k; % Update x_k for the next iteration
    end
end

function [x, u] = extractStateAndControl(y, P)
% sequentialOptimization extracts the state, x, and control, u, from the
% aggregate optimization variables y = [x; u]
%
% Inputs:
%   y: Column vector with both states and input (y = [x; u])
%   P: Struct of parameters
%
% Outputs:
%   x: Extracted state (x_1 to x_N in a single column vector)
%   u: Extracted control (u_0 to u_N-1 in a single column vector) 
%

    % Separate out the states
    x = y(1:P.n_state);
    u = y(P.n_state+1:end);
end

function [x_k1, k1, k2, k3, k4] = rk4PendulumSim(xk, uk, P)
% eulerPendulumSim Uses euler integration to calculate the solution
%
% Inputs:
%   xk: state [theta; (d/dt)theta] where theta is the angle of the pendulum
%   uk: Control torque at iteration k 
%   P: Parameter vector
%
% Outputs:
%   x_k1: Update (state at time k+1)
    
    %I'm actually using the standard rk4 method here. I just didn't change the name of
    %the functions
    k1 = pendulumDynamics(xk, uk, P);
    k2 = pendulumDynamics(xk + P.dt/2*k1, uk, P);
    k3 = pendulumDynamics(xk + P.dt/2*k2, uk, P);
    k4 = pendulumDynamics(xk + P.dt*k3, uk, P);

    x_k1 = xk + P.dt*(1/6*k1 + 1/3*k2 + 1/3*k3 + 1/6*k4); 
end

function xdot = pendulumDynamics(x, u, P)
% discreteDynamics Calculates the discrete unicycle dynamics of a given
% state and input
%
% Inputs:
%   x: State [theta; (d/dt)theta] where theta is the angle of the pendulum
%   u: Control torque
%
% Outputs:
%   fk: discrete dynamics (i.e. x_{k+1} = fk(xk, uk) )

    % Define parameters
    g = P.g;
    l = P.l;
    m = P.m;
    b = P.b;
    
    % Extract state
    theta = x(1);
    thetadot = x(2);
    
    % Calculate dynamics
    xdot = [ thetadot; ...
             (g/l)*sin(theta) - (b/(m*l^2))*thetadot + ((1/(m*l^2))*u)];
end

%%%%  Cost functions %%%%%%
function [c, dc_dy] = costCombined(y, P)
%costCombined calculates the cost and gradient of the cost with respect to
%the aggregate state, y = [x; u]
%
% Inputs:
%   y: Column vector with both states and input (y = [x; u])
%   P: Struct of parameters
%
% Outputs:
%   c: cost
%   dc_dy: partial derivative of the cost wrt y

    % Separate out the states
    [x, u] = extractStateAndControl(y, P);
    
    % Calculate the cost
    c = cost(x, u, P);
    
    % Calculate the partial
    if nargout > 1 % Only calculate the partial if the calling function wants it
        dc_dy = [calculatePartialWrtState(x,u,P), ... % dc/dx
                 calculatePartialWrtInput(x,u,P)];    % dc/du
    else
        dc_dy = []; % Output an empty value if not needed
    end
         
    %% Check partials (Comment this code when working)
    % Calculate individual partials
    dc_dx = calculatePartialWrtState(x,u,P);
    dc_du = calculatePartialWrtInput(x,u,P);
    
%     % Calculate numerical partials
%     dc_dx_num = jacobianest(@(x_in) cost(x_in, u, P), x);
%     dc_du_num = jacobianest(@(u_in) cost(x, u_in, P), u);
%     
%     % Calculate error
%     dc_dx_err = norm(dc_dx - dc_dx_num, 'fro')
%     dc_du_err = norm(dc_du - dc_du_num, 'fro')
    
end

function [c, dc_du] = costSequential(u, P)
%costSequential calculates the cost and gradient of the cost with respect to
%the input, u
%
% Inputs:
%   u: Input control (u_0 to u_N-1 in a single column vector)
%   P: Struct of parameters
%
% Outputs:
%   c: cost
%   dc_du: partial derivative of the cost wrt u

    % Calculate the state based upon the controls
    x = eulerSim(u, P);
    
    % Calcualte the cost
    c = cost(x, u, P);
    
    % Calculate the partial
    if nargout > 1
        dc_du = sequentialPartial(x, u, P);        
    else
        dc_du = [];
    end    
end

function c = cost(x, u, P)
%cost calculates the cost given the state and inputs
%
% Inputs: 
%   x: State (x_1 to x_N in a single column vector)
%   u: Control (u_0 to u_N-1 in a single column vector) 
%   P: Struct of parameters
%
% Outputs:
%   dc_du: Partial of cost wrt the input

    % Loop through the control inputs to calculate the cost
    c = 0; % Initialize the cost as zero
    ind_ctrl = 1:P.n_u; % Index of the control at iteration k
    for k = 0:(P.N-1)
        % Calculate cost
        u_k = u(ind_ctrl);
        c = c + P.k_u * (u_k'*u_k);
        
        % Update indices for the next iteration
        ind_ctrl = ind_ctrl + P.n_u;
    end
    
    % Calculate terminal cost
    x_e = (x(end-P.n_x+1:end) - P.x_d); % Error from the desired value
    c = c + x_e'*P.S*x_e; % Quadratic in the error
end

%%% Simultaneous optimization functions
function dc_dx = calculatePartialWrtState(x, u, P)
%calculatePartialWrtState calculates the partial of the cost wrt the state,
%x
%
% Inputs: 
%   x: State (x_1 to x_N in a single column vector)
%   u: Control (u_0 to u_N-1 in a single column vector) 
%   P: Struct of parameters
%
% Outputs:
%   dc_dx: Partial of cost wrt the state

    % Initialize the partial to the correct size
    dc_dx = zeros(1, P.n_state);
    
    % Calculate portion corresponding to x_N (note that this is the only
    % non-zero portion of this cost)
    ind_x_N = (P.n_state-P.n_x+1):P.n_state; % Indices of the final state
    dc_dx(ind_x_N) = 2.*(x(ind_x_N)-P.x_d)'*P.S; % Partial of the final state
end

function dc_du = calculatePartialWrtInput(x, u, P)
%calculatePartialWrtInput calculates the partial of the cost wrt the input,
% u
%
% Inputs: 
%   x: State (x_1 to x_N in a single column vector)
%   u: Control (u_0 to u_N-1 in a single column vector) 
%   P: Struct of parameters
%
% Outputs:
%   dc_du: Partial of cost wrt the input

    % Initialize the partial to the correct size
    dc_du = zeros(1, P.n_ctrl);
    
    % Calculate the portion corresponding to u_k for k = 0, ..., N-1
    ind_ctrl = 1:P.n_u; % Index of the control within u
    for k = 0:(P.N-1)
        uk = u(ind_ctrl); % Extract uk from the column vector
        dc_du(ind_ctrl) = (2*P.k_u*uk'); % Partial of c wrt uk
        
        % Update indices for next iteration
        ind_ctrl = ind_ctrl + P.n_u;
    end 
end

%%% Sequential optimization functions
function dc_du = sequentialPartial(x, u, P)
%sequentialPartial: Calculates the partial of the cost wrt the input u
% Note that the partial takes the form:
%       dc/duk = dL/duk + lam_{k+1}^T df/duk
% Inputs: 
%   x: State (x_1 to x_N in a single column vector)
%   u: Control (u_0 to u_N-1 in a single column vector) 
%   P: Struct of parameters
%
% Outputs:
%   dc_du: Partial of cost wrt the input

    % Initialize the partial
    dc_du = zeros(P.n_u, P.N); % We will use one column per gradient and then reshape to make a row vector at the end
    
    % Reshape vectors for easy access
    x_mat = [P.x0 reshape(x, P.n_x, P.N)]; % Also add in the initial state
    u_mat = reshape(u, P.n_u, P.N);
    
    % Initialize final lagrange multiplier (lam_N = dphi/dx(x_N)
    ind_x = size(x_mat, 2); % Final column index corresponds to the final state
    lam_kp1 = (2.*(x_mat(:,ind_x)-P.x_d)'*P.S)'; % dphi/dx(x_N) <-- This variable is used as lam_{k+1}
    lam_k = lam_kp1; % duplicate for initializaiton purposes (the loop moves backward 
                     % in time so at the beginning it changes iterations by
                     % setting lam_{k+1} = lam_k as k has been decremented
    
    % Simulate backward in time
    ind_u = size(u_mat, 2); % Index of the uk at iteration k (start with last column for k = P.N-1
    ind_x = ind_x - 1; % Index of xk at iteration k (start with second to last column for k = P.N-1)
    for k = (P.N-1):-1:0 % Simulate backwards in time
        % Extract the state and input
        uk = u_mat(:,ind_u); % Input at iteration k
        xk = x_mat(:, ind_x); % State at iteration k
        lam_kp1 = lam_k; % Update k index for the lagrange multipliers - \lambda at iteration k+1
        
        % Calculate partials needed for updates
        dLk_duk = 2*P.k_u*uk';
        dLk_dxk = zeros(1, P.n_x); % Instantaneous cost does not depend on xk
        dfk_duk = partialRK4WrtInput(xk, uk, P);
        dfk_dxk = partialRK4WrtState(xk, uk, P);
        
        % Calculate partial
        dc_du(:, ind_u) = (dLk_duk + lam_kp1'*dfk_duk)'; % Transposed to fit in temporary variable
        
        % Update Lagrange muliplier (moving backward in time)
        lam_k = dLk_dxk' + dfk_dxk'*lam_kp1;
        
        % Update the indices for the next iteration
        ind_u = ind_u - 1;
        ind_x = ind_x - 1;
    end
    
    % Reshape partial to be proper output
    dc_du = reshape(dc_du, 1, P.n_ctrl); % changes it from partial in columns for each iteration to one single row
end

%%% Constraint functions %%%%
function [g, h, dg_dy, dh_dy] = nonlinearConstraints(y, P)
%nonlinearConstraints calculates the nonlinear constraints given the
%aggregate state, y = [x; u] 
%
% Inputs:
%   y: Column vector with both states and input (y = [x; u])
%   P: Struct of parameters
%
% Outputs:
%   g: inequality constraints (empty for this problem)
%   h: equality constraints (used to represent the dynamics)
%   dg_dy: partial of inequality constraints wrt aggregate state
%   dh_dy: partial of equality constraints wrt aggregate state

    % Indicate that there are no inequality constraints
    g = [];
    dg_dy = [];

    % Separate out the states
    [x, u] = extractStateAndControl(y, P);

    % Calculate dynamic equality constraint
    h = calculateDynamicConstraint(x,u,P);
    
    % Caculate the partial of the equality constraint
    if nargout > 3
        dh_dy = [aggregatePartialDynamicWrtState(x, u, P), ... % dh/dx
                 aggregatePartialDynamicWrtInput(x, u, P)]';   % dh/du
                                                         % Transpose partial because matlab wants the 
                                                         % partial of each constraint to be a column vector
    else
        dh_dy = []; % Output empty vector
    end
                                                         
    %% Check the jacobian (comment out)
    dh_dx = aggregatePartialDynamicWrtState(x, u, P);
    dh_du = aggregatePartialDynamicWrtInput(x, u, P);
    
%     % Calculate numerical partials
%     dh_dx_num = jacobianest(@(x_in) calculateDynamicConstraint(x_in, u, P), x);
%     dh_du_num = jacobianest(@(u_in) calculateDynamicConstraint(x, u_in, P), u);
%     
%     % Calculate error
%     dh_dx_err = norm(dh_dx - dh_dx_num, 'fro')
%     dh_du_err = norm(dh_du - dh_du_num, 'fro')
%     if dh_du_err > .00001 || dh_dx_err > .00001
%         error('dh_du_err or dh_dx_err too large');
%     end
end

function h = calculateDynamicConstraint(x, u, P)
%calculateDynamicConstraint calculates the dynamic equality constraint wrt
%to the state and control 
%
% Inputs: 
%   x: State (x_1 to x_N in a single column vector)
%   u: Control (u_0 to u_N-1 in a single column vector) 
%   P: Struct of parameters
%
% Outputs:
%   h: dynamic constraint, block k corresponds to x_k+1 - fk(xk, uk) = 0

    % Initialize variables for looping through
    x_kp1 = P.x0;
    ind_state = 1:2; % Indices for the state
    ind_ctrl = 1; % Indices for the control
    h = zeros(P.n_state, 1); % Initialize h to be the correct size
    
    % Loop through the states to determine the constraint error
    for k = 0:(P.N-1)
        % Update the states
        x_k = x_kp1; % We have updated the index so we now update the state
        u_k = u(ind_ctrl); % input at iteration k
        x_kp1 = x(ind_state); % get x_{k+1}
        
        % Calculate what the dynamics should give for the state
        x_kp1_dyn = rk4PendulumSim(x_k, u_k, P);
        
        % Calculate the dynamic constraint
        h(ind_state) = x_kp1 - x_kp1_dyn;
        
        % Update the indices for the next iteration
        ind_state = ind_state + P.n_x;
        ind_ctrl = ind_ctrl + P.n_u;
    end
end

function dh_dx = aggregatePartialDynamicWrtState(x, u, P)
%aggregatePartialDynamicWrtState calculates the partial of the dynamic
%constraint wrt the state
%
% Inputs: 
%   x: State (x_1 to x_N in a single column vector)
%   u: Control (u_0 to u_N-1 in a single column vector) 
%   P: Struct of parameters
%
% Outputs:
%   dh_dx: Partial of constraint wrt the state

    % Initialize dh_dx (incorporates the identity term)
    dh_dx = P.dh_dx;
    
    % Calculate lower diagonal terms
    ind_r = (P.n_x+1):(P.n_x*2); % row indices (start on the second state
    ind_c = 1:P.n_x;
    ind_u = (P.n_u+1):(2*P.n_u);
    
    for k = 1:(P.N-1)
        % Extract state and input
        xk = x(ind_c);
        uk = u(ind_u);
        
        % Calculate partial
        dh_dx(ind_r, ind_c) = -partialRK4WrtState(xk, uk, P);
        
%         % Check partials numerically (comment out)
%         dh_dxk_num = -jacobianest(@(x_in) rk4PendulumSim(x_in, uk, P), xk);
%         dh_dxk_diff = dh_dx(ind_r, ind_c) - dh_dxk_num;
%         dh_dxk_err = norm(dh_dxk_diff);
%         if dh_dxk_err > .0001
%             error('Large error in dfk_dx');
%         end

        % Increment indices
        ind_r = ind_r + P.n_x;
        ind_c = ind_c + P.n_x;
        ind_u = ind_u + P.n_u;
    end
end

function dh_du = aggregatePartialDynamicWrtInput(x, u, P)
%aggregatePartialDynamicWrtInput calculates the partial of the dynamic
%constraint wrt the state
%
% Inputs: 
%   x: State (x_1 to x_N in a single column vector)
%   u: Control (u_0 to u_N-1 in a single column vector) 
%   P: Struct of parameters
%
% Outputs:
%   dh_du: Partial of constraint wrt the control

    % Initialize dh_du
    dh_du = P.dh_du;
    
    % Calculate first partial which uses the x0
    ind_r = 1:P.n_x; % Row indices (goes by state)
    ind_u = 1:P.n_u; % Column indices (goes by input)
    u0 = u(ind_u);
    dh_du(ind_r, ind_u) = -partialRK4WrtInput(P.x0, u0, P);
    
    % Loop through and calculate remaining partials
    ind_x = 1:P.n_x;
    ind_r = ind_r+P.n_x;
    ind_u = ind_u+P.n_u;
    for k = 1:(P.N-1)
        % Get state and input at k
        xk = x(ind_x);
        uk = u(ind_u);
        
        % Calculate partial
        dh_du(ind_r, ind_u) = -partialRK4WrtInput(xk, uk, P);
        
        % Check partials numerically (comment out)
%         dh_duk_num = -jacobianest(@(u_in) rk4PendulumSim(xk, u_in, P), uk);
%         dh_duk_diff = dh_du(ind_r, ind_u) - dh_duk_num;
%         dh_duk_err = norm(dh_duk_diff)
%         if dh_duk_err > .0001
%             error('Large error in dfk_du');
%         end

        % Update indices
        ind_x = ind_x+P.n_x;
        ind_r = ind_r+P.n_x;
        ind_u = ind_u+P.n_u;
    end
    
end

function dfk_dxk = partialDynamicWrtState(xk, uk, P)
%partialDynamicWrtState Partial of discrete dynamic update wrt the state at
%iteration k
%
% This assumes euler integration (i.e. fk = xk + dt*xdot)
%
% Inputs:
%   xk: State at iteration k (xk = [x; y; theta] where (x,y) is position and theta is orientation)
%   uk: Control at iteration k (uk = [v; w] where v is translational velocity and w is rotational velocity
%   P: Struct of parameters
%
% Outputs:
%   dfk_dxk: dfk/dxk 

    % Extract parameters
    g = P.g;
    l = P.l;
    m = P.m;
    b = P.b;

    % Extract state
    th = xk(1);
    th_dot = xk(2);    
    
    % Partial from continuous dynamics
    c_th = cos(th);
    dfk_dxk = [0, 1; ...
        g/l*c_th, -b/(m*l^2)];
end

function dfk_duk = partialDynamicWrtInput(xk, uk, P)
%partialDynamicWrtInput Partial of discrete dynamic update wrt the control at
%iteration k
%
% This assumes euler integration (i.e. fk = xk + dt*xdot)
%
% Inputs:
%   xk: State at iteration k (xk = [x; y; theta] where (x,y) is position and theta is orientation)
%   uk: Control at iteration k (uk = [v; w] where v is translational velocity and w is rotational velocity
%   P: Struct of parameters
%
% Outputs:
%   dfk_duk: dfk/duk 

    % Extract parameters
    l = P.l;
    m = P.m;
    
    % Partial from continuous dynamics
    dfk_duk = [0; ...
        1/(m*l^2)];
    
    %Don't need anything from the Euler update anymore, because I'm doing
    %RK4
end

function df_dx = partialRK4WrtState(xk,uk,P)
    %Using the partial derived in HW4
    [~, k1, k2, k3, k4] = rk4PendulumSim(xk, uk, P);
    
    dk1_dxk = partialDynamicWrtState(xk, uk, P);
    dk2_dxk = partialDynamicWrtState(xk+P.dt/2*k1, uk, P) * (eye(P.n_x) + P.dt/2*dk1_dxk);
    dk3_dxk = partialDynamicWrtState(xk+P.dt/2*k2, uk, P) * (eye(P.n_x) + P.dt/2*dk2_dxk);
    dk4_dxk = partialDynamicWrtState(xk+P.dt*k3, uk, P) * (eye(P.n_x) + P.dt*dk3_dxk);
    
    df_dx = eye(P.n_x) + P.dt*(1/6*dk1_dxk+1/3*dk2_dxk + 1/3*dk3_dxk + 1/6*dk4_dxk);
end

 function df_du = partialRK4WrtInput(xk,uk,P)
    %Using the partial derived in HW4 - note that each successive dfk_duk
    %requires a partial with respect to the state. This is because the
    %state depends on the inputs from the previous time step, so the chain
    %rule needs to be applied to the state as a function of the input
    
    [~, k1, k2, k3, k4] = rk4PendulumSim(xk, uk, P);
    
    %Take partials wrt state
    df_dl2 = partialDynamicWrtState(xk+P.dt/2*k1, uk, P);
    df_dl3 = partialDynamicWrtState(xk+P.dt/2*k2, uk, P);
    df_dl4 = partialDynamicWrtState(xk+P.dt*k3, uk, P);
    
    %Using the previously calculated partial wrt state
    dk1_duk = partialDynamicWrtInput(xk, uk, P);
    dk2_duk = partialDynamicWrtInput(xk+P.dt/2*k1, uk, P) + df_dl2*(P.dt/2*dk1_duk);
    dk3_duk = partialDynamicWrtInput(xk+P.dt/2*k2, uk, P) + df_dl3*(P.dt/2*dk2_duk);
    dk4_duk = partialDynamicWrtInput(xk+P.dt*k3, uk, P) + df_dl4*(P.dt*dk3_duk);
    
    df_du = P.dt*(1/6*dk1_duk+1/3*dk2_duk + 1/3*dk3_duk + 1/6*dk4_duk);
end