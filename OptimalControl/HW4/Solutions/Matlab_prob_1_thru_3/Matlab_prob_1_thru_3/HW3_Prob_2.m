function HW3_Prob_2
    close all;
    
    % Add dependent folders
    addpath Opt_dep
    
    %% Create the cost
    cost = FactoryProb_2_1(); step = @(x) cost.constrainedStep(x);
    %cost = FactoryProb_2_2(); step = @(x) cost.constrainedStep(x);
        
    % Set the stopping threshold very low to get closer to the solution
    cost.eps = 0.00001;
    
    %% Visualize the cost
    cost.plotCost();
    
    %% Perform gradient descent
    x = cost.x0;
    cost.plotState(x);
    cost.plotCostVsIteration(x);
    pause();
    
    i = 0
    while ~cost.stopConstrained(x)
        % Take step
       x = x + step(x)
       i = i+1
       
       % Adjust to constraint
       x = cost.adjustToConstraint(x);
       
       % Plot data
       cost.plotState(x);
       cost.plotCostVsIteration(x);
       pause(0.001);
    end
    
end

function cost = FactoryProb_2_1()
    %% Create the cost
    cost_unconstrained = HW3_Prob_2_1([5;5]);
    
    %% Create the constraints
    % Affine constraint a*x1 + b*x2 + c = 0
    is_equality = true;
    a = 1; b = 3; c = -1;
    h_vec(1) = AffineConstraint(cost_unconstrained.x1lim, ...
        cost_unconstrained.x2lim, is_equality, a, b, c);
    
    %% Create the constrained cost
    cost = ConstrainedCostClass(cost_unconstrained, h_vec);
end

function cost = FactoryProb_2_2()
    %% Create the cost
    cost_unconstrained = HW3_Prob_2_2([5;5;5]);
    
    %% Create the constraints
    % Affine constraint a*x1 + b*x2 + c*x3 + d = 0
    a = 1; b = 2; c = 3; d = -10;
    h1 = Affine3DConstraint(cost_unconstrained.x1lim, ...
        cost_unconstrained.x2lim, a, b, c, d);
    % Second affine constraint
    a = 1; b = -1; c = 2; d = -1;
    h2 = Affine3DConstraint(cost_unconstrained.x1lim, ...
        cost_unconstrained.x2lim, a, b, c, d);
    
    %% Create the constrained cost
    cost = AffineConstrainedCostClass(cost_unconstrained, h1, h2);
end