function HW4_Prob_2
    close all;
    
    % Add dependent folders
    addpath Opt_dep
    
    %% Create the unconstrained cost
    cost_unconstrained = HW4_Prob_2_cost([-5; -5]);
    
    %% Create the constraints
    % Affine constraint a*x1 + b*x2 + c = 0
    is_equality = false; % indicate that the constraint is equality (not inequality)
    a = -1; b = 0; c = 0;
    h_vec(1) = AffineConstraint(cost_unconstrained.x1lim, ...
        cost_unconstrained.x2lim, is_equality, a, b, c);
    h_vec(1).s = 1000;
    
    % Affine constraint a*x1 + b*x2 + c = 0
    is_equality = false; % indicate that the constraint is an inequality (not equality)
    a = 0; b = -1; c = 0;
    h_vec(2) = AffineConstraint(cost_unconstrained.x1lim, ...
        cost_unconstrained.x2lim, is_equality, a, b, c);
    h_vec(2).s = 1000;
    
    %% Create the constrained cost
    cost = ConstrainedCostClass(cost_unconstrained, h_vec); 
    
    %% Visualize the cost
    cost.plotCost();
    
    %% Perform gradient descent
    x = cost.x0;
    cost.plotState(x);
    cost.plotCostVsIteration(x);
    pause();
    
    % Perform descent
    performPenaltyMinimization(x, cost);
    
end

function performPenaltyMinimization(x, cost)
    %% Select the gradient descent method
    step = @(x) cost.armijo_step(x);
    
    %% Perform gradient descent
    i = 1
    while ~cost.stop(x)
       x = x + step(x)
       i = i+1
       
       cost.plotState(x);
       cost.plotCostVsIteration(x);
       pause(0.001);
    end
end

