function HW4_Prob_3_1_2
    close all;
    
    % Add dependent folders
    addpath Opt_dep
    
    %% Create the cost
    cost = Booth([-10;-10]);
    
    %% Visualize the cost
    cost.plotCost();
    
    % Define the linear inequality constraint ( 3 x1 + x2 \leq 6 )
    A = [3 1];
    B = 6;
    
    % Plot the linear inequality constraint
    lim = [-5 10];
    affine = AffineConstraint(lim, lim, false, A(1), A(2), -B);
    affine.plotConstraint();
    
    % Plot the circular constraint
    circle = EllipseConstraint(lim, lim, false, sqrt(5), sqrt(5));
    circle.plotConstraint();
    
    %% Perform gradient descent
    x = cost.x0;
    cost.plotState(x);
    
    %% Solve using Optimization library
    x0 = cost.x0; % Starting guess
    options = optimoptions(@fmincon, 'Algorithm', 'sqp');
    options = optimoptions(options, 'SpecifyObjectiveGradient', true, 'SpecifyConstraintGradient',true);
    options.Display = 'iter';
    lb = []; ub = []; % No upper or lower bounds
    
    % Matlab call          FUN                              X0, A, B,  Aeq, Beq, lb, ub 
    [x, cost_grad] = fmincon(@(x) cost.objectiveAndGrad(x), x0, A, B, [],  [],  lb, ub, @nonlinearConstraintsAndPartials, options);
    cost.plotState(x, 'r');
    disp(['cost_grad = ' num2str(cost_grad)]);  
    
    
    
 
end

function [g, h, dg_du, dh_du] = nonlinearConstraintsAndPartials(u)
% This implements a circular constraint (u1^2 + u2^2 -5 <= 0)
%
% Inputs:
%   u: Optimization variable
%
% Outputs:
%   g: Value of the inequality constraint function evaluated for u
%   h: Value of the equality constraint function evaluated for u
%   dg_du: Partial of inequality constraint with respect to u
%   dh_du: Partial of equality constraint with respect to u

    % Equality constraints (none)
    h = []; 
    dh_du = [];
    
    % Extract variables
    u1 = u(1);
    u2 = u(2);
        
    % Inequality constraints
    g = u1^2 + u2^2 - 5;
    dg_du = [2*u1; 2*u2];
end




