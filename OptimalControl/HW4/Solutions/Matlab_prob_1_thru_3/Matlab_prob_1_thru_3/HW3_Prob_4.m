function HW3_Prob_4
    close all;
    
    % Add dependent folders
    addpath Opt_dep
    
    %% Create the cost
    cost = FactoryProblem4_1();
    
    %% Visualize the cost
    cost.plotCost();
    
    %% Perform gradient descent
    x = cost.x0;
    cost.plotState(x);
    cost.plotCostVsIteration(x);
    pause();
    
    % Perform descent
    x = performPenaltyMinimization(x, cost);
    %x = performConstrainedMinimization(x, cost);
    
%     % Matlab call          FUN                  X0, A,  B,  Aeq, Beq, lb, ub 
%     options = optimoptions(@fmincon, 'Algorithm', 'sqp');
%     options = optimoptions(options, 'SpecifyObjectiveGradient', true, 'SpecifyConstraintGradient',true);
%     options.Display = 'iter';
%     
%     x0 = x;
%     [x, cost_grad] = fmincon(@(x) cost.cost_ref.objectiveAndGrad(x), x0, [], [], [],  [],  [], [], @(x)cost.constraintsAndPartials(x), options);
%     time_grad = toc
%     cost.plotState(x);
%     disp(['cost_grad = ' num2str(cost_grad)]);
    
    end_value = x
end

function x = performPenaltyMinimization(x, cost)
    %% Select the gradient descent method
    cost.alpha_const = 0.00001;
    %step = @(x) cost.constant(x);
    cost.alpha_dim = 0.005;
    step = @(x) cost.diminish(x);
    %step = @(x) cost.optimal_step(x);
    %step = @(x) cost.armijo_step(x);
    
    %% Perform gradient descent
    i = 1
    while ~cost.stop(x)
       x = x + step(x);
       i = i+1
       
       cost.plotState(x);
       cost.plotCostVsIteration(x);
       pause(0.001);
    end
end

function x = performConstrainedMinimization(x, cost)
    %% Select the gradient descent method
    step = @(x) cost.constrainedStep(x);

    %% Perform gradient descent
    i = 1
    while ~cost.stopConstrained(x)
       % Take step
       x = x + step(x);
       i = i+1
       
       % Adjust to constraint
       x = cost.adjustToConstraint(x);
       
       % Plot
       cost.plotState(x);
       cost.plotCostVsIteration(x);
       pause(0.001);
    end
end

function cost = FactoryProblem4_1()
    %% Create the cost
    cost = Booth([1; 1]);
end