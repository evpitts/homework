function HW3_Prob_1
% For problem 1.1 - Extra credit solution
% For problem 1.2 - Extra credit for numerical descent. Uncomment line 27 for the analytic solution 
    close all;
    
    % Add dependent folders
    addpath Opt_dep
    
    %% Create the cost
    cost = HW3_Prob_1_1([10; 10]);
    %cost = HW3_Prob_1_2(zeros(6, 1));
    
    % Set the stopping threshold very low to get closer to the solution
    cost.eps = 0.001;
    
    %% Visualize the cost
    cost.plotCost();
    
    %% Select the gradient descent method
    step = @(x) cost.constant(x);
    %step = @(x) cost.diminish(x);
    %step = @(x) cost.optimal_step(x);
    %step = @(x) cost.armijo_step(x);
    
    %% Perform gradient descent
    x = cost.x0;
    %x = cost.calculateMin();
    cost.plotState(x);
    cost.plotCostVsIteration(x);
    pause();
    
    i = 0
    while ~cost.stop(x)
       x = x + step(x)
       i = i+1
       
       % Plot data
       cost.plotState(x);
       cost.plotCostVsIteration(x);
       
       % Pause for plotting
       pause(0.001);
    end
    
end

