classdef AffineConstrainedCostClass < ConstrainedCostClass
    properties
        h_agg % The aggregate constraint class
    end
    
    methods
        function obj = AffineConstrainedCostClass(cost, h1, h2)
            h_vec(1) = h1;
            h_vec(2) = h2;
            obj = obj@ConstrainedCostClass(cost, h_vec);
            
            % Create the aggregate constraint class
            a = [h1.a; h2.a];
            b = [h1.b; h2.b];
            c = [h1.c; h2.c];
            d = [h1.d; h2.d];
            obj.h_agg = Affine3DConstraint(h1.x1lim, h2.x2lim, a, b, c, d);
        end
        
        function x = adjustToConstraint(obj, x)
            % This function solves for z \in \R^p given u \in \R^{n-p}
            %
            % Note: this function is simplified right now to solve for
            % only p = 2, n = 3 (i.e. a double constraint)
            
            if obj.p > 2
                return;
            end
            
            z = obj.h_agg.solve(x);
            u = x(obj.p+1:end);
            x = [z; u];
        end
        
    end
end

