classdef Booth < CostClass
    
    properties
       a = 5.1/(4*pi^2)
       b = 5/pi
       c = -6
       d = 10*(1-1/(8*pi))
       e = 10
    end
        
    methods
        function obj = Booth(x0)
            % Define limits
            x1 = [-5 15];
            x2 = [-5 15];
            z = [0 100];
            
            % Create the class
            obj = obj@CostClass(x1, x2, z,x0);
        end
        
        function val = cost(obj,x)
            x1 = x(1);
            x2 = x(2);
            val = (x1+2*x2-7)^2 + (2*x1+x2-5)^2;
        end
        
        function dc_dx = partial(obj, x)
            x1 = x(1);
            x2 = x(2);
            dc_dx = [10*x1 + 8*x2 - 34, 8*x1+10*x2-38];
        end
        
        function step = optimal_step(obj, x)
            step = [0;0];
        end
    end
end
