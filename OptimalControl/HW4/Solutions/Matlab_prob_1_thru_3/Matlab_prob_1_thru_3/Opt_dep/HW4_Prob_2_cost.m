classdef HW4_Prob_2_cost < CostClass
    
    properties
        
    end
    
    methods
        function obj = HW4_Prob_2_cost(x0)
            % Define limits
            x1 = [-10 10];
            x2 = [-10 10];
            z = [-10 20];
            
            % Create the class
            obj = obj@CostClass(x1, x2, z, x0);
        end
        
        function val = cost(obj,x)
            val = x(1)^2 - x(1) + x(2) + x(1)*x(2);
        end
        
        function dc_dx = partial(obj, x)
            x1 = x(1); x2 = x(2);
            dc_dx = [2*x1 - 1 + x2, 1 + x1];
        end
        
        function step = optimal_step(obj, x)
            step = [0;0];
        end
        
        
    end
end

