classdef ConstraintClass < handle
    
    properties
        x1lim % Limits for plotting along x1 axis
        x2lim % Limits for plotting along x2 axis
        dx = 0.1 % spacing for plotting x1 and x2
        is_equality % true => equality constraint, false => inequality constraint      
        
        % Penalty function variables
        k = 100 % scale on the penalty cost
        s = 1 % slope of sigmoid function
    end
    
    % Constraint class function methods
    methods(Abstract)
        dh_dx = partial(obj, x);
        h = constraint(obj, x);
        x1 = solve(obj, x);
    end
    
    methods
        function obj = ConstraintClass(x1lim, x2lim, is_equality)
            % Read in properties
            obj.x1lim = x1lim;
            obj.x2lim = x2lim;
            obj.is_equality = is_equality;
        end
        
        function plotConstraint(obj)
            [X1, X2] = meshgrid([obj.x1lim(1):obj.dx:obj.x1lim(2)], [obj.x2lim(1):obj.dx:obj.x2lim(2)]);
            
            % Loop through and evaluate each value
            H = zeros(size(X1));
            for m = 1:size(X1, 1)
                for n = 1:size(X2,2)
                    x = [X1(m,n); X2(m,n)];
                    H(m,n) = obj.constraint(x);
                end
            end
            
            % Create the contour plot
            contour(X1, X2, H, [0 0], 'r'); 
            if ~obj.is_equality % If inequality constraint then plot dotted line inside constraint
                contour(X1, X2, H, [-1 -1], ':r'); 
            end
        end
        
        function K = penaltyValue(obj, x)
            % Calculate the value of the constraint
            h = obj.constraint(x);
            
            % Calculate the penalty
            if obj.is_equality
                K = obj.squaredPenalty(h);
            else % => inequality constraint - use sigmoid
                K = obj.sigmoidPenalty(h);
            end
        end
        
        function dK_dx = penaltyGradient(obj, x)
            % Note that dK/dx = dK/dh * dh/dx
            
            % Calculate dK/dh
            h = obj.constraint(x);
            if obj.is_equality
                dK_dh = obj.squaredPartial(h);
            else
                dK_dh = obj.sigmoidPartial(h);
            end
            
            % Calculate dh/dx
            dh_dx = obj.partial(x);
            
            % Output full partial
            dK_dx = dK_dh * dh_dx;
        end       
        
    end
    
    methods(Access=protected)
        function K = sigmoidPenalty(obj, h)
            K = obj.k * h^2 / (1 + exp(-obj.s * h));                        
        end
        
        function K = squaredPenalty(obj, h)
            K = obj.k * h^2;
        end
        
        function dK_dh = sigmoidPartial(obj, h)
            dK_dh = 2*obj.k*h/(1+exp(-obj.s*h)) + ...
                obj.s*obj.k*h^2*exp(-obj.s*h) / (1+exp(-obj.s*h))^2;
        end
        
        function dK_dh = squaredPartial(obj, h)
            dK_dh = 2*obj.k * h;
        end
               
    end
end

