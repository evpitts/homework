classdef HW3_Prob_2_1 < CostClass
    
    properties
        
    end
    
    methods
        function obj = HW3_Prob_2_1(x0)
            % Define limits
            x1 = [-10 10];
            x2 = [-10 10];
            z = [-10 20];
            
            % Create the class
            obj = obj@CostClass(x1, x2, z, x0);
        end
        
        function val = cost(obj,x)
            x1 = x(1); x2 = x(2);
            val = 0.5* (x1^2/2 + x2^2/4);
        end
        
        function dc_dx = partial(obj, x)
           x1 = x(1); x2 = x(2);
           dc_dx = [x1/2, x2/4];
        end
        
        function step = optimal_step(obj, x)
            step = [0;0];
        end
        
        function min = calculateMin(obj)
            M = [0.5 0 1; 0 1/4 3; 1 3 0];
            min_agg = inv(M) * [0;0;1];
            min = min_agg(1:2);
        end
    end
end

