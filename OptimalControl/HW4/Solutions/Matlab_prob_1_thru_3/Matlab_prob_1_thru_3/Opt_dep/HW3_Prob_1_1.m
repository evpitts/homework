classdef HW3_Prob_1_1 < CostClass
        
    properties
        
    end
    
    methods
        function obj = HW3_Prob_1_1(x0)
            % Define limits
            x1 = [-10 10];
            x2 = [-10 10];
            z = [-10 20];
            
            % Create the class
            obj = obj@CostClass(x1, x2, z, x0);
        end
        
        function val = cost(obj,x)
            x1 = x(1); x2 = x(2);
            val = x1^2 - x1*x2 + x2^2 - 3 * x2;
        end
        
        function dc_dx = partial(obj, x)
           x1 = x(1); x2 = x(2);
           dc_dx = [2*x1-x2, 2*x2-x1-3];
        end
        
        function step = optimal_step(obj, x)
            step = [0;0];
        end
        
        function min = calculateMin(obj)
            min = [1; 2];
        end
    end
end

