classdef HW3_Prob_2_2 < CostClass
    
    properties
        
    end
    
    methods
        function obj = HW3_Prob_2_2(x0)
            % Define limits
            x1 = [-10 10];
            x2 = [-10 10];
            z = [-10 20];
            
            % Create the class
            obj = obj@CostClass(x1, x2, z, x0);
        end
        
        function val = cost(obj,x)
            val = x'*x;
        end
        
        function dc_dx = partial(obj, x)
           dc_dx = 2 .* x';
        end
        
        function step = optimal_step(obj, x)
            step = [0;0];
        end
        
        function min = calculateMin(obj)
            M = [2 0 0 1 1; 0 2 0 2 -1; 0 0 2 3 2; 1 2 3 0 0; 1 -1 2 0 0];
            min_agg = M\[0;0;0;10;1];
            min = min_agg(1:3);
        end
    end
end

