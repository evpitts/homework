classdef HW3_Prob_1_2 < CostClass
        
    properties
        % Dimensions
        %n = 8; % Number of optimization variables
        m = 20; % Number of sample points
        
        % Cost variables (x'Qx -2b'x + c
        Q % Quadratic term 
        b % Linear term
        c % Affine term
        
        % Data used to produce results
        input
        output
        
        % Handles used for plotting
        regression_handle = [];
    end
    
    methods
        function obj = HW3_Prob_1_2(x0)
            % Define limits
            x1 = [-10 10];
            x2 = [-10 10];
            z = [-10 100];
            
            % Create the class
            obj = obj@CostClass(x1, x2, z, x0);
            
            % Create the data and regression matrices
            obj.getData();
            obj.CalculateRegressionMatrices();
        end
        
        function val = cost(obj,x)
            val = x'*obj.Q*x - 2*obj.b'*x + obj.c;
        end
        
        
        function dc_dx = partial(obj, x)
           dc_dx = x' * (obj.Q + obj.Q') - 2 * obj.b';
        end
        
        function step = optimal_step(obj, x)
            step = [0;0];
        end
        
        function min = calculateMin(obj)
            M = (obj.Q + obj.Q');
            min = 2*inv(M)' * obj.b;
        end
        
        %%%  Approximation function values %%%
        function getData(obj, varargin)
            [obj.input, obj.output] = obj.functionBeingApproximated(true, obj.m);
        end
        
        function [inputs, outputs] = functionBeingApproximated(obj, use_noise, n_pnts)
            inputs = linspace(0,1, n_pnts)';
            outputs = cos(10.*inputs) + use_noise * 0.1 * randn(n_pnts, 1);
            %outputs = exp(inputs) + use_noise * 0.1 * randn(n_pnts, 1);
        end
        
        function CalculateRegressionMatrices(obj)
            % Calculate the Q matrix
            obj.Q = zeros(obj.n);
            for i = 1:obj.n
                for j = 1:obj.n
                    for k = 1:obj.m
                        % Minus two in the exponent to be zero indexed
                        obj.Q(i,j) = obj.Q(i,j) + obj.input(k)^(i+j-2);
                    end
                end
            end
            
            % Calculate the b matrix
            obj.b = zeros(obj.n, 1);
            for j = 1:obj.n
                for k = 1:obj.m
                    % Subtract 1 from the exponent due to zero indexing
                    obj.b(j) = obj.b(j) + obj.output(k) * obj.input(k)^(j-1);
                end
            end
            
            % Calculate the c value
            obj.c = 0;
            for k = 1:obj.m
                obj.c = obj.c + obj.output(k)^2;
            end
        end
        
        
        %%%  Functions implmented to compensate for > 2 dimension %%%
        function numericallyTestLocalSolution(obj, x, dx, max_dev)
        end
        
        function plotCost(obj, varargin)
            return;
        end
        
        function plotState(obj, x)
            % Calculate the input and output using the values x
            in_vec = obj.input;
            out_vec = zeros(1, length(in_vec));
            for k = 1:length(in_vec)
                out_vec(k) = obj.regression(x, in_vec(k));
            end
            
            % Plot the data
            if isempty(obj.regression_handle)
                % Plot the function we are trying to match
                [inputs, outputs] = obj.functionBeingApproximated(false, 1000);
                figure;
                plot(inputs, outputs, 'r:', 'linewidth', 3); hold on;
                
                % Plot the data used to create regression
                plot(obj.input, obj.output, 'ro');
                
                % Plot the regression data
                obj.regression_handle = plot(in_vec, out_vec, 'b', 'linewidth', 2);
            else
                % Replot the regression data
                set(obj.regression_handle, 'xdata', in_vec, 'ydata', out_vec);
            end
        end
        
        function h = regression(obj, x, in)
            h = 0;
            for k = 1:obj.n
                h = h + x(k)*in^(k-1);
            end
        end
        
        
%         %%% Garbage debug functions %%%
%         function Qk = calculateQk(obj, x)
%             Qk = [1 x x^2; x x^2 x^3; x^2 x^3 x^4];
%         end
%         
%         function bk = calculatebk(obj, x, g)
%            bk = [g; g*x; g*x^2]; 
%         end
%         
%         function ck = calculateck(obj, g)
%            ck = g^2; 
%         end
    end
end

