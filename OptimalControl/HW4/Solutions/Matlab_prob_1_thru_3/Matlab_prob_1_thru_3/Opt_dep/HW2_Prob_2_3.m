classdef HW2_Prob_2_3 < CostClass
    
    properties
        
    end
    
    methods
        function obj = HW2_Prob_2_3(x0)
            % Define limits
            x1 = [-10 10];
            x2 = [-10 10];
            z = [-10 20];
            
            % Create the class
            obj = obj@CostClass(x1, x2, z, x0);
        end
        
        function val = cost(obj,x)
            val = 4*(x(1) + x(2));
        end
        
        function dc_dx = partial(obj, x)
           dc_dx = [4, 4];
        end
        
        function step = optimal_step(obj, x)
            step = [0;0];
        end
        
        
    end
end

