function [c,ceq] = constraint(u)
c = u(1)^2 + u(2)^2 - 1;
ceq = [];