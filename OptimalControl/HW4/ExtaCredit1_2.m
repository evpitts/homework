options = optimoptions(@fmincon, 'Algorithm', 'sqp');
options = optimoptions(options, 'PlotFcn',{@optimplotstepsize,@optimplotx,@optimplotfval,@optimplotfirstorderopt});
options.Display = 'iter';

%% Problem 1
objective = @(u)(4*(u(1) + u(2)));

u0 = [2,2];

nonlcon = @constraint;
[u,fval,exitflag,output,history] = fmincon(objective, u0, A, b, [], [], [], [], nonlcon, options)

% %% Problem 2_1
% objective = @(u)-1*(u(1)^2-u(1)+u(2)+u(1)*u(2));
% 
% u0 = [10,10];
% 
% Aineq = [-1 0; 0 -1]; bineq = [0; 0];
% 
% [u,fval,exitflag,output,history] = fmincon(objective, u0, Aineq, bineq, [], [], [], [], [], options)