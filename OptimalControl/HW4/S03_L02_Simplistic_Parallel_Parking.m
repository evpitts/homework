function S03_L02_Simplistic_Parallel_Parking
close all;
    %% Check the model
    %checkModel(); % Check to make sure the discrete and continuous
                   %functions match up
    
    %% Problem setup
    % Initialize problem parameters
    P.N = 100; % Number of steps
    P.n_x = 3; % Number of states
    P.n_u = 2; % Number of control inputs
    P.n_ctrl = P.n_u*P.N; % Number of control inputs combined
    P.n_state = P.n_x*P.N; % Number of states combined
    P.dt = 0.1; % Time step
    P.x0 = [1; 0; pi/2]; % Initial state
    P.k_u = .01; % Penalty on control
    P.S = diag([100, 100, 10]); % Penalty on terminal state
    P.x_d = [0; 0; pi/2];
    
    % Initialize variables for optimization
    u0 = 0.01.*ones(P.n_ctrl, 1); % Note that a zero-input control corresponds 
                                  % to a local minimum that the optimization has a 
                                  % hard time moving out of
    
    % Create the initial state
    x0 = discreteSim(u0, P); % Simulate forward in time using the control laws
    
    % Optimize
    tic
    [x, u] = simultaneousOptimization(x0, u0, P);
    %[x, u] = sequentialOptimization(u0, P);
    time_opt = toc
    
    %% Plot the result
    % Reformat the state and control to have one column per time index
    x_mat = [P.x0 reshape(x, P.n_x, P.N)]; % Add in the initial state as well
    u_mat = reshape(u, P.n_u, P.N);
    
    % Plot the 2D results
    figure;
    plot2DResults(x0, P, 'r', 3); % Plot the initial solution
    plot2DResults(x, P, 'b', 3); % Plot the optimized solution
    
    % Plot the states
    figure;
    names = {'x', 'y', '\theta'};
    for k = 1:3
        subplot(5, 1, k);
        plot([1 P.N], [P.x_d(k) P.x_d(k)], 'r:', 'linewidth', 3); hold on;
        plot(x_mat(k,:), 'b', 'linewidth', 2); 
        xlim([0 P.N]); % Ensures the plot has the same x-axis as the others
        ylabel(names{k});
    end
    
    % Plot the control inputs
    names = {'u_v', 'u_w'};
    for k = 1:2
        subplot(5, 1, k+3);
        plot(u_mat(k,:), 'b', 'linewidth', 2);
        xlim([0 P.N]); % Ensures the plot has the same x-axis as the others
        ylabel(names{k});
    end
end

%%%%  Optimization functions %%%%
function [x, u] = simultaneousOptimization(x0, u0, P)
% simultaneousOptimization simultaneously optimizes over both the state and
% the control, using an equality constraint to relate the two
%
% Inputs:
%   x0: Initial guess for the states (x_1 to x_N in a single column vector)
%   u0: Initial guess for the control (u_0 to u_N-1 in a single column vector)
%   P: Struct of parameters
%
% Outputs:
%   x: Optimized state (x_1 to x_N in a single column vector)
%   u: Optimized control (u_0 to u_N-1 in a single column vector)

    %% Initialize parameters
    % Initialize partials for constraint
    P.dh_dx = eye(P.n_state); % Initializes the partial of the constraint wrt the state for easy access
    P.dh_du = zeros(P.n_state, P.n_ctrl); % Initializes the partial of the constraint wrt the control
    
    % Create the aggregate state and initial cost
    y0 = [x0; u0]; % Combine the initial state and control into one vector
    c_init = costCombined(y0, P) % Display the initial cost
    %[~, h_init] = nonlinearConstraints(y0, P); % Can be used to see if the
                                                % constraint is satisfied (sanity check on model)
    
    % Initialize the solver variables
    options = optimoptions(@fmincon, 'Algorithm', 'sqp'); % choose sequential-quadratic-programming
    %options = optimoptions(@fmincon, 'Algorithm', 'interior-point'); % choose an the interior-point algorithm
    options = optimoptions(options, 'SpecifyObjectiveGradient', true, 'SpecifyConstraintGradient',true); % Indicate whether to use gradients
    options = optimoptions(options, 'OptimalityTolerance', 0.1); % Tolerance for optimization
    options.Display = 'iter'; % Have Matlab display the optimization information with each iteration
    
    % Define the linear inequality constraints (empty matrices because we
    % do not have any)
    A = zeros(P.N*2,P.N*(P.n_x+P.n_u));
    y_idx = 1;
    for x_idx=2:3:P.N*P.n_x
        A(y_idx,x_idx) = 1;
        A(y_idx+1,x_idx) = -1;
        y_idx = y_idx + 2;
    end
    B = repmat([0.5; 0.5],P.N,1);
    
    % Define the linear equality constraints (empty matrices because we do
    % not have any)
    Aeq = []; % No equality constraints
    Beq = [];
    
    % Define the upper and lower bounds (empty matrices because we do not
    % have any)
    lb = [-1*Inf(P.n_x+P.n_u,P.N-1) [P.x_d; -inf; -inf]];
    ub = [Inf(P.n_x+P.n_u,P.N-1) [P.x_d; inf; inf]];
%     lb = []; % No upper or lower bounds
%     ub = []; 
    
    % Matlab call:
    optimize = true; % If optimize is true, then it will optimize. Otherwise it will load in the data
    if optimize
        [y, final_cost] = fmincon(@(y) costCombined(y, P), y0, A, B, Aeq, Beq, lb, ub, @(y) nonlinearConstraints(y, P), options);
        save('Data\s03_l02_data.mat');
    else
        data = load('Data\s03_l02_data.mat');
        y = data.y;
        final_cost = data.final_cost;
    end
    disp(['Final cost = ' num2str(final_cost)]);
    
    % Extract the state and control from the variable y
    [x, u] = extractStateAndControl(y, P);
end

function [x, u] = sequentialOptimization(u0, P)
% sequentialOptimization optimizes over the control. At each iteration the
% control is used to calculate the state which are both used to calculate
% the cost
%
% Inputs:
%   u0: Initial guess for the control (u_0 to u_N-1 in a single column vector)
%   P: Struct of parameters
%
% Outputs:
%   x: Optimized state (x_1 to x_N in a single column vector)
%   u: Optimized control (u_0 to u_N-1 in a single column vector)
    
    % Output the initial cost
    c_init = costSequential(u0, P)
    
    % Initialize the solve variables
    options = optimoptions(@fmincon, 'Algorithm', 'sqp'); % choose sequential-quadratic-programming
    %options = optimoptions(@fmincon, 'Algorithm', 'interior-point'); % choose an the interior-point algorithm
    options = optimoptions(options, 'SpecifyObjectiveGradient', true); % Indicate whether to use gradients (Note that there are no constraint gradients)
    options = optimoptions(options, 'OptimalityTolerance', 0.1); % Tolerance for optimization
    options.Display = 'iter'; % Have Matlab display the optimization information with each iteration
    
    % Define the linear inequality constraints (empty matrices because we
    % do not have any)
    A = [];
    B = [];

    % Define the linear equality constraints (empty matrices because we do
    % not have any)
    Aeq = []; % No equality constraints
    Beq = [];
    
    % Define the upper and lower bounds (empty matrices because we do not
    % have any)
    lb = []; % No upper or lower bounds
    ub = []; 
    
    % Matlab call:
    optimize = true; % If optimize is true, then it will optimize. Otherwise it will load in the data
    if optimize
        [u, final_cost] = fmincon(@(u_in) costSequential(u_in, P), u0, A, B, Aeq, Beq, lb, ub, [], options);
        save('Data\s03_l02_sequential_data.mat');
    else
        data = load('Data\s03_l02_sequential_data.mat');
        u = data.u;
        final_cost = data.final_cost;
    end
    disp(['Final cost = ' num2str(final_cost)]);
    
    % Simulate the state forward in time to be able to output the result
    x = discreteSim(u, P);    
end

%%%%  Simulation functions %%%%%%
function x = discreteSim(u, P)
% discreteSim calculates the state given the control inputs
%
% Inputs:
%   u: Control inputs (u_0 to u_N-1 in a single column vector)
%   P: Struct of parameters
%
% Outputs:
%   x: Calculated state (x_1 to x_N in a single column vector)

    % Initialize
    x_k = P.x0; % Set x_k to the initial state
    x = zeros(P.n_state, 1); % Initialize the output states
    
    % Calculate the simulation forward in time
    ind_state = 1:3; % Indices for the state
    ind_ctrl = 1:2; % Indices for the control
    for k = 1:P.N
        % Calculate the update given x_k
        f_k = discreteDynamics(P.dt, x_k, u(ind_ctrl));
        x(ind_state) = f_k;
        
        % Update the indices
        ind_state = ind_state + P.n_x;
        ind_ctrl = ind_ctrl + P.n_u;
        x_k = f_k; % Update x_k for the next iteration
    end
end

function [x, u] = extractStateAndControl(y, P)
% sequentialOptimization extracts the state, x, and control, u, from the
% aggregate optimization variables y = [x; u]
%
% Inputs:
%   y: Column vector with both states and input (y = [x; u])
%   P: Struct of parameters
%
% Outputs:
%   x: Extracted state (x_1 to x_N in a single column vector)
%   u: Extracted control (u_0 to u_N-1 in a single column vector) 
%

    % Separate out the states
    x = y(1:P.n_state);
    u = y(P.n_state+1:end);
end

function fk = discreteDynamics(dt, xk, uk)
% discreteDynamics Calculates the discrete unicycle dynamics of a given
% state and input
%
% Inputs:
%   dt: discretization period
%   xk: State at iteration k (xk = [x; y; theta] where (x,y) is position and theta is orientation)
%   uk: Control at iteration k (uk = [v; w] where v is translational velocity and w is rotational velocity
%
% Outputs:
%   fk: discrete dynamics (i.e. x_{k+1} = fk(xk, uk) )

    % Extract states and control inputs needed for dynamics
    theta = xk(3); % Orientation
    v = uk(1); % forward velocity
    w = uk(2); % rotational velocity
    
    % Calculate update
    if w == 0 % Special case for zero rotational velocity (to avoid dividing by zero)
        f1 = v*dt*cos(theta); % dynamics of x-position
        f2 = v*dt*sin(theta); % dynamics of y-position
    else % Case when w is not zero
        f1 = v/w*(sin(w*dt + theta) - sin(theta)); % dynamics of x-position
        f2 = v/w*(cos(theta) - cos(w*dt + theta)); % dynamics of y-position
    end 
    f3 = w*dt; % dynamics of theta
    
    % Calculate the new state
    fk = [f1; f2; f3] + xk; % State update equation
end

function xdot = continuousDynamics(t, x, u)
%continuousDynamics Implements the continuous dynamics for a unicycle robot
% 
% Inputs:
%   t: current time (not used but included for easy incorporation into
%   ode45)
%   x: current state (x = [x; y; theta] where (x,y) is position and theta is orientation)
%   u: current input (u = [v; w] where v is translational velocity and w is rotational velocity
%
% Output:
%   xdot: dx/dt - the time derivative of the state

    % Extract states and control inputs
    theta = x(3); % Orientation
    v = u(1); % forward velocity
    w = u(2); % rotational velocity
    
    % Calculate dynamics
    xdot = [v*cos(theta); v*sin(theta); w];
end

%%%%  Cost functions %%%%%%
function [c, dc_dy] = costCombined(y, P)
%costCombined calculates the cost and gradient of the cost with respect to
%the aggregate state, y = [x; u]
%
% Inputs:
%   y: Column vector with both states and input (y = [x; u])
%   P: Struct of parameters
%
% Outputs:
%   c: cost
%   dc_dy: partial derivative of the cost wrt y

    % Separate out the states
    [x, u] = extractStateAndControl(y, P);
    
    % Calculate the cost
    c = cost(x, u, P);
    
    % Calculate the partial
    if nargout > 1 % Only calculate the partial if the calling function wants it
        dc_dy = [calculatePartialWrtState(x,u,P), ... % dc/dx
                 calculatePartialWrtInput(x,u,P)];    % dc/du
    else
        dc_dy = []; % Output an empty value if not needed
    end
         
%     %% Check partials (Comment this code when working)
%     % Calculate individual partials
%     dc_dx = calculatePartialWrtState(x,u,P);
%     dc_du = calculatePartialWrtInput(x,u,P);
%     
%     % Calculate numerical partials
%     dc_dx_num = jacobianest(@(x_in) cost(x_in, u, P), x);
%     dc_du_num = jacobianest(@(u_in) cost(x, u_in, P), u);
%     
%     % Calculate error
%     dc_dx_err = norm(dc_dx - dc_dx_num, 'fro')
%     dc_du_err = norm(dc_du - dc_du_num, 'fro')
    
end

function [c, dc_du] = costSequential(u, P)
%costSequential calculates the cost and gradient of the cost with respect to
%the input, u
%
% Inputs:
%   u: Input control (u_0 to u_N-1 in a single column vector)
%   P: Struct of parameters
%
% Outputs:
%   c: cost
%   dc_du: partial derivative of the cost wrt u

    % Calculate the state based upon the controls
    x = discreteSim(u, P);
    
    % Calcualte the cost
    c = cost(x, u, P);
    
    % Calculate the partial
    if nargout > 1
        dc_du = sequentialPartial(x, u, P);        
    else
        dc_du = [];
    end
end

function c = cost(x, u, P)
%cost calculates the cost given the state and inputs
%
% Inputs: 
%   x: State (x_1 to x_N in a single column vector)
%   u: Control (u_0 to u_N-1 in a single column vector) 
%   P: Struct of parameters
%
% Outputs:
%   dc_du: Partial of cost wrt the input

    % Loop through the control inputs to calculate the cost
    c = 0; % Initialize the cost as zero
    ind_ctrl = 1:P.n_u; % Index of the control at iteration k
    for k = 0:(P.N-1)
        % Calculate cost
        u_k = u(ind_ctrl);
        c = c + P.k_u * (u_k'*u_k);
        
        % Update indices for the next iteration
        ind_ctrl = ind_ctrl + P.n_u;
    end
    
    % Calculate terminal cost
    x_e = (x(end-2:end) - P.x_d); % Error from the desired value
    c = c + x_e'*P.S*x_e; % Quadratic in the error
end

%%% Simultaneous optimization functions
function dc_dx = calculatePartialWrtState(x, u, P)
%calculatePartialWrtState calculates the partial of the cost wrt the state,
%x
%
% Inputs: 
%   x: State (x_1 to x_N in a single column vector)
%   u: Control (u_0 to u_N-1 in a single column vector) 
%   P: Struct of parameters
%
% Outputs:
%   dc_dx: Partial of cost wrt the state

    % Initialize the partial to the correct size
    dc_dx = zeros(1, P.n_state);
    
    % Calculate portion corresponding to x_N (note that this is the only
    % non-zero portion of this cost)
    ind_x_N = (P.n_state-P.n_x+1):P.n_state; % Indices of the final state
    dc_dx(ind_x_N) = 2.*(x(ind_x_N)-P.x_d)'*P.S; % Partial of the final state
end

function dc_du = calculatePartialWrtInput(x, u, P)
%calculatePartialWrtInput calculates the partial of the cost wrt the input,
% u
%
% Inputs: 
%   x: State (x_1 to x_N in a single column vector)
%   u: Control (u_0 to u_N-1 in a single column vector) 
%   P: Struct of parameters
%
% Outputs:
%   dc_du: Partial of cost wrt the input

    % Initialize the partial to the correct size
    dc_du = zeros(1, P.n_ctrl);
    
    % Calculate the portion corresponding to u_k for k = 0, ..., N-1
    ind_ctrl = 1:P.n_u; % Index of the control within u
    for k = 0:(P.N-1)
        uk = u(ind_ctrl); % Extract uk from the column vector
        dc_du(ind_ctrl) = (2*P.k_u*uk'); % Partial of c wrt uk
        
        % Update indices for next iteration
        ind_ctrl = ind_ctrl + P.n_u;
    end 
end

%%% Sequential optimization functions
function dc_du = sequentialPartial(x, u, P)
%sequentialPartial: Calculates the partial of the cost wrt the input u
% Note that the partial takes the form:
%       dc/duk = dL/duk + lam_{k+1}^T df/duk
% Inputs: 
%   x: State (x_1 to x_N in a single column vector)
%   u: Control (u_0 to u_N-1 in a single column vector) 
%   P: Struct of parameters
%
% Outputs:
%   dc_du: Partial of cost wrt the input

    % Initialize the partial
    dc_du = zeros(P.n_u, P.N); % We will use one column per gradient and then reshape to make a row vector at the end
    
    % Reshape vectors for easy access
    x_mat = [P.x0 reshape(x, P.n_x, P.N)]; % Also add in the initial state
    u_mat = reshape(u, P.n_u, P.N);
    
    % Initialize final lagrange multiplier (lam_N = dphi/dx(x_N)
    ind_x = size(x_mat, 2); % Final column index corresponds to the final state
    lam_kp1 = (2.*(x_mat(:,ind_x)-P.x_d)'*P.S)'; % dphi/dx(x_N) <-- This variable is used as lam_{k+1}
    lam_k = lam_kp1; % duplicate for initializaiton purposes (the loop moves backward 
                     % in time so at the beginning it changes iterations by
                     % setting lam_{k+1} = lam_k as k has been decremented
    
    % Simulate backward in time
    ind_u = size(u_mat, 2); % Index of the uk at iteration k (start with last column for k = P.N-1
    ind_x = ind_x - 1; % Index of xk at iteration k (start with second to last column for k = P.N-1)
    for k = (P.N-1):-1:0 % Simulate backwards in time
        % Extract the state and input
        uk = u_mat(:,ind_u); % Input at iteration k
        xk = x_mat(:, ind_x); % State at iteration k
        lam_kp1 = lam_k; % Update k index for the lagrange multipliers - \lambda at iteration k+1
        
        % Calculate partials needed for updates
        dLk_duk = 2*P.k_u*uk';
        dLk_dxk = zeros(1, P.n_x); % Instantaneous cost does not depend on xk
        dfk_duk = partialDynamicWrtInput(xk, uk, P);
        dfk_dxk = partialDynamicWrtState(xk, uk, P);
        
        % Calculate partial
        dc_du(:, ind_u) = (dLk_duk + lam_kp1'*dfk_duk)'; % Transposed to fit in temporary variable
        
        % Update Lagrange muliplier (moving backward in time)
        lam_k = dLk_dxk' + dfk_dxk'*lam_kp1;
        
        % Update the indices for the next iteration
        ind_u = ind_u - 1;
        ind_x = ind_x - 1;
    end
    
    % Reshape partial to be proper output
    dc_du = reshape(dc_du, 1, P.n_ctrl); % changes it from partial in columns for each iteration to one single row
end

%%% Constraint functions %%%%
function [g, h, dg_dy, dh_dy] = nonlinearConstraints(y, P)
%nonlinearConstraints calculates the nonlinear constraints given the
%aggregate state, y = [x; u] 
%
% Inputs:
%   y: Column vector with both states and input (y = [x; u])
%   P: Struct of parameters
%
% Outputs:
%   g: inequality constraints (empty for this problem)
%   h: equality constraints (used to represent the dynamics)
%   dg_dy: partial of inequality constraints wrt aggregate state
%   dh_dy: partial of equality constraints wrt aggregate state

    % Indicate that there are no inequality constraints
    g = [];
    dg_dy = [];

    % Separate out the states
    [x, u] = extractStateAndControl(y, P);

    % Calculate dynamic equality constraint
    h = calculateDynamicConstraint(x,u,P);
    
    % Caculate the partial of the equality constraint
    if nargout > 3
        dh_dy = [aggregatePartialDynamicWrtState(x, u, P), ... % dh/dx
                 aggregatePartialDynamicWrtInput(x, u, P)]';   % dh/du
                                                         % Transpose partial because matlab wants the 
                                                         % partial of each constraint to be a column vector
    else
        dh_dy = []; % Output empty vector
    end
                                                         
%     %% Check the jacobian (comment out)
%     dh_dx = aggregatePartialDynamicWrtState(x, u, P);
%     dh_du = aggregatePartialDynamicWrtInput(x, u, P);
%     
%     % Calculate numerical partials
%     dh_dx_num = jacobianest(@(x_in) calculateDynamicConstraint(x_in, u, P), x);
%     dh_du_num = jacobianest(@(u_in) calculateDynamicConstraint(x, u_in, P), u);
%     
%     % Calculate error
%     dh_dx_err = norm(dh_dx - dh_dx_num, 'fro');
%     dh_du_err = norm(dh_du - dh_du_num, 'fro');
%     if dh_du_err > .00001 || dh_dx_err > .00001
%         error('dh_du_err or dh_dx_err too large');
%     end
end

function h = calculateDynamicConstraint(x, u, P)
%calculateDynamicConstraint calculates the dynamic equality constraint wrt
%to the state and control 
%
% Inputs: 
%   x: State (x_1 to x_N in a single column vector)
%   u: Control (u_0 to u_N-1 in a single column vector) 
%   P: Struct of parameters
%
% Outputs:
%   h: dynamic constraint, block k corresponds to x_k+1 - fk(xk, uk) = 0

    % Initialize variables for looping through
    x_kp1 = P.x0;
    ind_state = 1:3; % Indices for the state
    ind_ctrl = 1:2; % Indices for the control
    h = zeros(P.n_state, 1); % Initialize h to be the correct size
    
    % Loop through the states to determine the constraint error
    for k = 0:(P.N-1)
        % Update the states
        x_k = x_kp1; % We have updated the index so we now update the state
        u_k = u(ind_ctrl); % input at iteration k
        x_kp1 = x(ind_state); % get x_{k+1}
        
        % Calculate what the dynamics should give for the state
        x_kp1_dyn = discreteDynamics(P.dt, x_k, u_k);
        
        % Calculate the dynamic constraint
        h(ind_state) = x_kp1 - x_kp1_dyn;
        
        % Update the indices for the next iteration
        ind_state = ind_state + P.n_x;
        ind_ctrl = ind_ctrl + P.n_u;
    end
end

function dh_dx = aggregatePartialDynamicWrtState(x, u, P)
%aggregatePartialDynamicWrtState calculates the partial of the dynamic
%constraint wrt the state
%
% Inputs: 
%   x: State (x_1 to x_N in a single column vector)
%   u: Control (u_0 to u_N-1 in a single column vector) 
%   P: Struct of parameters
%
% Outputs:
%   dh_dx: Partial of constraint wrt the state

    % Initialize dh_dx (incorporates the identity term)
    dh_dx = P.dh_dx;
    
    % Calculate lower diagonal terms
    ind_r = (P.n_x+1):(P.n_x*2); % row indices (start on the second state
    ind_c = 1:P.n_x;
    ind_u = (P.n_u+1):(2*P.n_u);
    
    for k = 1:(P.N-1)
        % Extract state and input
        xk = x(ind_c);
        uk = u(ind_u);
        
        % Calculate partial
        dh_dx(ind_r, ind_c) = -partialDynamicWrtState(xk, uk, P);
        
%         % Check partials numerically (comment out)
%         dh_dxk_num = -jacobianest(@(x_in) discreteDynamics(P.dt, x_in, uk), xk);
%         dh_dxk_err = norm(dh_dx(ind_r, ind_c) - dh_dxk_num);
%         if dh_dxk_err > .0001
%             error('Large error in dfk_dx');
%         end
        
        % Increment indices
        ind_r = ind_r + P.n_x;
        ind_c = ind_c + P.n_x;
        ind_u = ind_u + P.n_u;
    end
end

function dh_du = aggregatePartialDynamicWrtInput(x, u, P)
%aggregatePartialDynamicWrtInput calculates the partial of the dynamic
%constraint wrt the state
%
% Inputs: 
%   x: State (x_1 to x_N in a single column vector)
%   u: Control (u_0 to u_N-1 in a single column vector) 
%   P: Struct of parameters
%
% Outputs:
%   dh_du: Partial of constraint wrt the control

    % Initialize dh_du
    dh_du = P.dh_du;
    
    % Calculate first partial which uses the x0
    ind_r = 1:P.n_x; % Row indices (goes by state)
    ind_u = 1:P.n_u; % Column indices (goes by input)
    u0 = u(ind_u);
    dh_du(ind_r, ind_u) = -partialDynamicWrtInput(P.x0, u0, P);
    
    % Loop through and calculate remaining partials
    ind_x = 1:P.n_x;
    ind_r = ind_r+P.n_x;
    ind_u = ind_u+P.n_u;
    for k = 1:(P.N-1)
        % Get state and input at k
        xk = x(ind_x);
        uk = u(ind_u);
        
        % Calculate partial
        dh_du(ind_r, ind_u) = -partialDynamicWrtInput(xk, uk, P);
        
        % Update indices
        ind_x = ind_x+P.n_x;
        ind_r = ind_r+P.n_x;
        ind_u = ind_u+P.n_u;
    end
    
end

function dfk_dxk = partialDynamicWrtState(xk, uk, P)
%partialDynamicWrtState Partial of discrete dynamic update wrt the state at
%iteration k
%
% Inputs:
%   xk: State at iteration k (xk = [x; y; theta] where (x,y) is position and theta is orientation)
%   uk: Control at iteration k (uk = [v; w] where v is translational velocity and w is rotational velocity
%   P: Struct of parameters
%
% Outputs:
%   dfk_dxk: dfk/dxk 

    % Extract inputs and needed state
    v = uk(1);
    w = uk(2);
    th = xk(3);
    th_kp1 = w*P.dt + th; % Theta at time kp1 calculated from the inputs and current state
    
    % Pre-calculate trig functions
    s_th = sin(th);
    c_th = cos(th);
        
    % Calculate partial
    dfk_dxk = eye(P.n_x); % Initialize diagonal of the partial
    if w ~= 0
        % dfk,1/dtheta
        dfk_dxk(1,3) = v/w*(cos(th_kp1) - c_th);
        
        % dfk,2/dtheta
        dfk_dxk(2,3) = v/w*(sin(th_kp1) - s_th);
    else
        % dfk,1/dtheta
        dfk_dxk(1,3) = -v*P.dt*s_th;
        
        % dfk,2/dtheta
        dfk_dxk(2,3) = v*P.dt*c_th;
    end
end

function dfk_duk = partialDynamicWrtInput(xk, uk, P)
%partialDynamicWrtInput Partial of discrete dynamic update wrt the control at
%iteration k
%
% Inputs:
%   xk: State at iteration k (xk = [x; y; theta] where (x,y) is position and theta is orientation)
%   uk: Control at iteration k (uk = [v; w] where v is translational velocity and w is rotational velocity
%   P: Struct of parameters
%
% Outputs:
%   dfk_duk: dfk/duk 

    % Extract inputs and needed state
    v = uk(1);
    w = uk(2);
    th = xk(3);
    
    % Pre-calculate trig functions
    s_th = sin(th);
    c_th = cos(th);
    
    % Calculate partial
    if w ~= 0
        % Additional sine and cosine terms
        th_kp1 = w*P.dt + th; % Theta at time kp1 calculated from the inputs and current state
        s_th_kp1 = sin(th_kp1); % sine and cosine of theta(k+1)
        c_th_kp1 = cos(th_kp1);

        % Calculate scale factors
        w_inv = 1/w;
        v_d_w = v/w;
        v_d_w2 = v_d_w*w_inv;
        
        % Calculate the partials
        dfk_duk = [w_inv*(s_th_kp1-s_th), v_d_w*P.dt*c_th_kp1 - v_d_w2*(s_th_kp1 - s_th); ...
                   w_inv*(c_th - c_th_kp1), v_d_w*P.dt*s_th_kp1 - v_d_w2*(c_th-c_th_kp1); ...
                   0, P.dt];
    else
        dfk_duk = [P.dt*c_th, 0; ...
                   P.dt*s_th, 0; ...
                   0, P.dt];
    end
    
end

%%%%  Debug and visualization functions
function plot2DResults(x, P, c, linewidth)
    % Extract data
    %[x, ~] = extractStateAndControl(y, P);
    x_mat = [P.x0 reshape(x, P.n_x, P.N)];
    
    plot(x_mat(1,:), x_mat(2,:), c, 'linewidth', linewidth); hold on;
    plot(P.x_d(1), P.x_d(2), 'ro', 'linewidth', 2);
end

function checkModel()
    %% Plot continuity of discrete dynamics
    % Initialize state variables
    dt = 0.1;
    w = -.01:.00001:.01;
    len = length(w);
    v = ones(1,len);
    u_k = [v; w];
    
    % Calculate the discrete dynamics
    f_k_mat = zeros(3, len);
    x_k = rand(3,1);
    for k = 1:len
        f_k_mat(:, k) = discreteDynamics(dt, x_k, u_k(:,k));
    end
    
    % Plot the discrete dynamics
    figure;
    subplot(2,1,1);
    plot(w, f_k_mat(1,:), 'linewidth', 2);
    ylabel('f_1');
    
    subplot(2,1,2);
    plot(w, f_k_mat(2,:), 'linewidth', 2);
    ylabel('f_2');
    
    %% Look at continuous vs discrete dynamics
    dt = rand + 0.01; % Add 0.01 to avoid zero
    u = rand(2,1);
    %u = [1; 0];
    %x_k = [.16; .11; .4983];
    
    % Perform discrete update
    x_kp1 = discreteDynamics(dt, x_k, u)
    
    % Perform continuous update
    [tmat, xmat] = ode45(@(t,x) continuousDynamics(t, x, u), [0:.001:dt], x_k);
    xmat = xmat';
    x_dt = xmat(:,end)
    
    % Plot the continous time solution
    figure;
    plot(xmat(1,:), xmat(2,:), 'b', 'linewidth', 2); hold on;
    
    % Plot the discrete time solution
    plot([x_k(1) x_kp1(1)], [x_k(2) x_kp1(2)], 'ro', 'linewidth', 2);
    axis equal
end
