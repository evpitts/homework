function S02_L06_EqualityConstrainedDemo
    close all;
    
    % Add dependent folders
    addpath ../Opt_dep
    addpath ../HW3
    
    %% Create the cost
    %cost = FactoryConstrainedQuadratic();
    %cost = FactoryConstrainedBranin();
    cost = FactoryConstrainedBooth();
    
    %% Visualize the cost
    cost.plotCost();
    
    %% Perform gradient descent
    x = cost.x0;
    cost.plotState(x);
    pause();
    
    % Perform descent
    %x = performPenaltyMinimization(x, cost);
    x = performConstrainedMinimization(x, cost);
    cost.plotState(x, 'r');
    
    % Output the resulting cost
    cost_final = cost.cost_ref.cost(x)
    
      %% Solve using Optimization library
%     x0 = cost.x0; % Starting guess
%     options = optimoptions(@fmincon, 'Algorithm', 'sqp');
%     options = optimoptions(options, 'SpecifyObjectiveGradient', true, 'SpecifyConstraintGradient',true);
%     options.Display = 'iter';
%     lb = []; ub = []; % No upper or lower bounds
%     
%     tic
%     % Matlab call          FUN                  X0, A,  B,  Aeq, Beq, lb, ub 
%     [x, cost_grad] = fmincon(@(x) cost.cost_ref.objectiveAndGrad(x), x0, [], [], [],  [],  lb, ub, @(x)cost.constraintsAndPartials(x), options);
%     time_grad = toc
%     cost.plotState(x, 'r');
%     disp(['cost_grad = ' num2str(cost_grad)]);
%     
%     % Without gradient
%     options = optimoptions(options, 'SpecifyObjectiveGradient', false, 'SpecifyConstraintGradient',false);
%         
%     tic
%     % Matlab call                     FUN               X0, A,  B,  Aeq, Beq, lb, ub 
%     [x, cost_no_grad] = fmincon(@(x) cost.cost_ref.objectiveAndGrad(x), x0, [], [], [],  [],  lb, ub, @(x)cost.constraintsAndPartials(x), options);
%     time_no_grad = toc
%     cost.plotState(x, 'r');
%     disp(['cost_no_grad = ' num2str(cost_no_grad)]);   
end



function x = performPenaltyMinimization(x, cost)
    %% Select the gradient descent method
    cost.alpha_const = 0.00001;
    %step = @(x) cost.constant(x);
    cost.alpha_dim = 0.005;
    %step = @(x) cost.diminish(x);
    %step = @(x) cost.optimal_step(x);
    step = @(x) cost.armijo_step(x);
    
    %% Perform gradient descent
    i = 1
    while ~cost.stop(x)
       x = x + step(x);
       i = i+1
       
       cost.plotState(x);
       pause(0.001);
    end
end

function x = performConstrainedMinimization(x, cost)
    %% Select the gradient descent method
    step = @(x) cost.constrainedStep(x);

    %% Perform gradient descent
    i = 1
    while ~cost.stopConstrained(x)
       % Take step
       x = x + step(x);
       i = i+1
       
       % Adjust to constraint
       x = cost.adjustToConstraint(x);
       
       % Plot
       cost.plotState(x);
       pause(0.001);
    end
end

function cost = FactoryConstrainedQuadratic()
    %% Create the cost
    cost_unconstrained = Quadratic([0; 6]);
    
    %% Create constraints
    % Affine constraint a*x1 + b*x2 + c = 0
    is_equality = true; % indicate that the constraint is equality (not inequality)
    a = 3; b = 1; c = -6;
    f_vec(1) = AffineConstraint(cost_unconstrained.x1lim, ...
        cost_unconstrained.x2lim, is_equality, a, b, c);
    
    %% Create the constrained cost
    cost = ConstrainedCostClass(cost_unconstrained, f_vec);    
end

function cost = FactoryConstrainedBranin()
    %% Create the cost
    cost_unconstrained = Branin([10; 10]);
    
    %% Create constraints
    % Affine constraint a*x1 + b*x2 + c = 0
    is_equality = true; % indicate that the constraint is equality (not inequality)
    a = 3; b = 1; c = -6;
    f_vec(1) = AffineConstraint(cost_unconstrained.x1lim, ...
        cost_unconstrained.x2lim, is_equality, a, b, c);
    
    %% Create the constrained cost
    cost = ConstrainedCostClass(cost_unconstrained, f_vec); 
end

function cost = FactoryConstrainedBooth()
    %% Create the cost
    cost_unconstrained = Booth([4.5; 4]);
    
    %% Create constraints
    % Affine constraint a*x1 + b*x2 + c = 0
    is_equality = true; % indicate that the constraint is equality (not inequality)
    a = 2; b = 3; c = -5;
    f_vec(1) = AffineConstraint(cost_unconstrained.x1lim, ...
        cost_unconstrained.x2lim, is_equality, a, b, c);
    
    %% Create the constrained cost
    cost = ConstrainedCostClass(cost_unconstrained, f_vec); 
end